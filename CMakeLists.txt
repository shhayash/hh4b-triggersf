cmake_minimum_required(VERSION 3.11)
project(triggersf)
option(ON_GRID "Running on grid")
set(RDF_UTILS "rdf-utils")
include(${RDF_UTILS}/include.cmake)

include_directories(utils)

root_generate_dictionary(G__dictionary
                         utils/select-triggers-utils.h
                         LINKDEF
                         LinkDef.h)
# Place dictionary in a shared library
add_library(dictionary SHARED
                       G__dictionary.cxx)

set(select-triggers_SRC select-triggers.cpp)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ftemplate-depth=2048")

# Tell CMake to create the executable
add_executable(select-triggers
               ${select-triggers_SRC})
target_link_libraries(select-triggers PUBLIC rdf-utils dictionary stdc++fs)

get_property(TRIGGERSF_INCLUDE_DIRS TARGET select-triggers PROPERTY INCLUDE_DIRECTORIES)
string(REPLACE ";" ":" TRIGGERSF_INCLUDE_PATH "${TRIGGERSF_INCLUDE_DIRS}")
if(ON_GRID)
    set(HH4BDIR "\${TestArea}/build")
    string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}" "\${TestArea}" HH4BINCPATH "${TRIGGERSF_INCLUDE_PATH}")
else()
    set(HH4BDIR "${CMAKE_CURRENT_BINARY_DIR}")
    set(HH4BINCPATH "${TRIGGERSF_INCLUDE_PATH}")
endif()

configure_file(tools/setup.sh.in setup.sh @ONLY)
configure_file(tools/mergeTrigSF.zsh merge-nnt.zsh COPYONLY)
configure_file(trigger-list-SF.dat trigger-list-SF.dat COPYONLY)
