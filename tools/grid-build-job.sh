#!/usr/bin/env bash

if [ "$(lsb_release -sr | cut -d"." -f1)" = "6" ]; then
    echo "Running on SLC 6"
    source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96b x86_64-slc6-gcc8-opt
elif [ "$(lsb_release -sr | cut -d"." -f1)" = "7" ]; then
    echo "Running on CentOS 7"
	source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96b x86_64-centos7-gcc8-opt
else
    echo "Not using SLC 6 or CentOS 7. I hope you have all the dependencies installed."
fi
rm -rf build || echo "No build dir -- Good"
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DON_GRID=ON ..
make
