#!/bin/zsh
export PROMPT='$ '
export RPROMPT=
export LC_CTYPE=C
TIMESTAMP=`date -u +%Y-%^b-%dT%H%MZ`
TAG=`git describe --tag`
autoload colors
colors
function print_color() {
    echo -en "\e[$color[$1];1m"
    echo -n $2
    echo -en "\e[0m"
}

alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS -q && (voms-proxy-info -e || voms-proxy-init -voms atlas -valid 96:00:00) && lsetup panda

MC=""
SYSTEMATICS=
TTBARSELECTION=
while getopts ":sti:f:" opt; do
    case ${opt} in
        s)
            SYSTEMATICS="-s"
            ;;
        t)
            TTBARSELECTION='-t'
            ;;
        i)
            INDSLST="$OPTARG"
            ;;
        \?)
            print_color red "Option $OPTARG not recognized\n" 1>&2
            exit 1
            ;;
        :)
            print_color red "$OPTARG requires an argument\n" 1>&2
            exit 1
            ;;
    esac
done

if [ -z "$INDSLST" ]; then
    print_color red "No input dataset list\n" 1>&2
    exit 1
elif [[ ! -f "$INDSLST" ]]; then
    print_color red "$INDS is not a list of datasets\n" 1>&2
    exit 1
fi

OUTDS=
# Grid submission checklist
alias fail_checklist="print_color red \"CHECKLIST FAILED -- QUITTING\n\"; exit 1"

print_color blue "GRID SUBMISSION CHECKLIST\n"
print_color blue "-------------------------\n"
echo -n "1. Are there uncommitted changes? "
output=$(git status --untracked-files=no --porcelain)
if [[ -n "$output" ]]; then
    print_color red "YES\n"
    fail_checklist
else
    print_color green "No\n"
fi
echo -n "2. Are we on the master branch? "
output=$(git rev-parse --abbrev-ref HEAD)
if [[ $output != "master" ]]; then
    print_color red "NO\n"
    option_in=
    vared -p "   Override [y/N]? " option_in
    if [[ $option_in =~ '^[Yy].*$' ]]; then
        print_color green "   OK, continuing.\n"
    else
        fail_checklist
    fi
else
    print_color green "Yes\n"
fi

echo -n "3. Checking datasets... "
MC=0 # 0 for unset, empty for no, 1 for yes
while read -r INDS ; do
    if [[ "$INDS" =~ 'user\.[a-z]+\.HH4B\.([[:alnum:]]+)\.([-_[:alnum:]]+)\.([-_[:alnum:]]*)\.(AB21\.2\.[-_[:alnum:]]+)\.([-_[:alpha:]]+)_MiniNTuple.root/' ]]; then
        # Captured groups are DSID SAMPLE_TYPE MC_Tag MiniNTuple_Tag contents
        MC_Tag=$match[3]
        MNT_Tag=$match[4]
        SAMPLE_TYPE=$match[2]
        DSID=$match[1]
        CONTENTS=$match[5]
        if [[ -n "$SYSTEMATICS" ]]; then
            if [[ ! ("$CONTENTS" == *sys*) ]]; then
                print_color red "\nRUNNING SYSTEMATICS ON A SAMPLE WITHOUT SYSTEMATICS\n"
                fail_checklist
            fi
        fi

        # Set based on first dataset in list
        if [[ "$MC" = "0" ]]; then
            if [[ "$MC_Tag" == *MC* ]]; then
                MC=1
            else
                MC=
            fi
        elif [[ "$MC" = "1" ]]; then
            if [[ ! ("$MC_Tag" == *MC*) ]]; then
                print_color red "\nCannot mix MC and data in the same submission\n"
                fail_checklist
            fi
        elif [[ -z "$MC" ]]; then
            if [[ "$MC_Tag" == *MC* ]]; then
                print_color red "\nCannot mix data and MC in the same submission\n"
                fail_checklist
            fi
        fi
    else
        print_color red "\nNOT A VALID SAMPLE: $INDS\n"
        fail_checklist
    fi
done < $INDSLST

print_color green "All Good\n"
echo -n "4. Are we on a tag? "
output=$(git describe --exact-match HEAD 2>/dev/null)
if [[ -z "$output" ]]; then
    print_color red "NO\n"
    print_color yellow "Continuing in test mode\n"
    print_color yellow "Output dataset name will contain timestamp\n"
    OUTDS="user.$RUCIO_ACCOUNT.HH4B.NNT.$TIMESTAMP"
else
    print_color green "Yes\n"
    OUTDS="user.$RUCIO_ACCOUNT.HH4B.NNT.$TAG"
fi
print_color green "Checklist succeeded\n"
print_color red "BUILD DIRECTORY WILL BE DELETED!\n"
option_in=
vared -p "  OK [y/N]? " option_in
if [[ $option_in =~ '^[Yy].*$' ]]; then
    rm -rf build
else
    print_color yellow "Not deleting build directory. Ignore errors about large files being skipped."
fi

#check if the code.tgz from a previous submission is lurking, delete if exists
if [ -f code.tgz ]; then
    rm code.tgz 2>&1 > /dev/null
fi

if [ -z "$RUCIO_ACCOUNT" ]; then
    # make sure RUCIO_ACCOUNT is set
    RUCIO_ACCOUNT=$USER
fi

TARBALL="--outTarBall"

while read -r INDS; do
    THISDS=
    if [[ "$INDS" =~ 'user\.[a-z]+\.HH4B\.([[:alnum:]]+)\.([-_[:alnum:]]+)\.([-_[:alnum:]]*)\.(AB21\.2\.[-_[:alnum:]]+)\.([-_[:alpha:]]+)_MiniNTuple.root/' ]]; then
        # Always passes
        MC_Tag=$match[3]
        MNT_Tag=$match[4]
        SAMPLE_TYPE=$match[2] # or data year
        DSID=$match[1] # or period for data
        CONTENTS=$match[5]
        SYST=""
        if [[ -n $SYSTEMATICS ]]; then
            SYST="SYST."
        fi
        if [[ -z $MC ]]; then
            if [[ -n $TTBARSELECTION ]]; then
                THISDS="$OUTDS.$MNT_Tag.$SAMPLE_TYPE.$DSID.MNT_${CONTENTS}.${SYST}TOP"
            else
                THISDS="$OUTDS.$MNT_Tag.$SAMPLE_TYPE.$DSID.MNT_${CONTENTS}.${SYST}NOM"
            fi
        else
            if [[ -n $TTBARSELECTION ]]; then
                THISDS="$OUTDS.$MNT_Tag.$MC_Tag.$DSID.MNT_${CONTENTS}.${SYST}TOP"
            else
                THISDS="$OUTDS.$MNT_Tag.$MC_Tag.$DSID.MNT_${CONTENTS}.${SYST}NOM"
            fi
        fi
    fi

    if [ -z "$MC" ]; then
        # Not MC, so we can split jobs
        echo "Submitting task -- $INDS"
        if [[ "$INDS" == *data15* ]]; then
            YEAR=2015
        elif [[ "$INDS" == *data16* ]]; then
            YEAR=2016
        elif [[ "$INDS" == *data17* ]]; then
            YEAR=2017
        elif [[ "$INDS" == *data18* ]]; then
            YEAR=2018
        fi
        prun --exec "./tools/select-triggers-job.sh -y $YEAR ${SYSTEMATICS} $TTBARSELECTION -i %IN" \
            --bexec "./tools/grid-build-job.sh" \
            --extFile "rdf-utils/mc-config/xsec.tsv" \
            --excludeFile ".git" --forceStaged \
            --outDS "$THISDS" --inDS $INDS --outputs "NNT:nominal.root,LOG:*.log" \
            --cmtConfig "x86_64-centos7-gcc8-opt" \
            --nGBPerJob 4 \
            $TARBALL code.tgz
    else
        # MC, split anyway, we save the normalization now
        echo "Submitting task -- $INDS"
            if [[ "$INDS" == *2015-2016* ]]; then
                YEAR=2016
                LUMI=27.8
            elif [[ "$INDS" == *2017* ]]; then
                YEAR=2017
                LUMI=43.65
            elif [[ "$INDS" == *2018* ]]; then
                YEAR=2018
                LUMI=58.45
            fi
        prun --exec "./tools/select-triggers-job.sh -m -l $LUMI -y $YEAR ${SYSTEMATICS} $TTBARSELECTION -i %IN" \
            --bexec "./tools/grid-build-job.sh" \
            --extFile "rdf-utils/mc-config/xsec.tsv" \
            --excludeFile ".git" --forceStaged \
            --outDS "${THISDS}_Y${YEAR}" --inDS $INDS --outputs "NNT:*.root,LOG:*.log" \
            --cmtConfig "x86_64-centos7-gcc8-opt" \
            --nGBPerJob 5 \
            $TARBALL code.tgz

            if [[ "$INDS" == *2015-2016* ]]; then
                YEAR=2015
                prun --exec "./tools/select-triggers-job.sh -m -l $LUMI -y $YEAR ${SYSTEMATICS} $TTBARSELECTION -i %IN" \
                    --bexec "./tools/grid-build-job.sh" \
                    --extFile "rdf-utils/mc-config/xsec.tsv" \
                    --excludeFile ".git" --forceStaged \
                    --outDS "${THISDS}_Y${YEAR}" --inDS $INDS --outputs "NNT:*.root,LOG:*.log" \
                    --cmtConfig "x86_64-centos7-gcc8-opt" \
                    --nGBPerJob 5 \
                    $TARBALL code.tgz
            fi
    fi

    # After first task, switch to inTarBall
    TARBALL="--inTarBall"
done < $INDSLST
rm code.tgz 2>&1 > /dev/null

