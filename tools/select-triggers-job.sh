#!/usr/bin/env bash
exec 2>&1
echo WELCOME TO SELECT-TRIGGERS AT RESOLVED RECON
MC=""
LUMI=
YEAR=
PTCUT=
NBJETS=
TTBARSELECTION=
while getopts ":ml:y:sti:f:" opt; do
	case ${opt} in
	m)
		MC="--mc"
		;;
        l)
                LUMI="--lumi $OPTARG"
		;;
        y)
                YEAR="--year $OPTARG"
		;;
        s)
                SYSTEMATICS=1
		;;
        t)
                TTBARSELECTION="--ttbarSelection"
		;;
	i)
		INPUT="$(echo $OPTARG | sed 's/,/ /g')"
		;;
	\?)
		echo "Option $OPTARG not recognized" 1>&2
		exit 1
		;;
	:)
		echo "$OPTARG requires an argument" 1>&2
		exit 1
		;;
	esac
done

PTCUT="--pTcut 35"
NBJETS="--nbjets 2"
TRGMAP="--trigger-map"

if [ -z "$INPUT" ]; then
	echo "No input files" 1>&2
	exit 2
fi

source ${TestArea}/build/setup.sh

export RES_RECO_ON_GRID=1

echo ======== RESOLVED-RECON OUTPUT ========
echo PATH=$PATH
echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
echo ----- ls build -----
ls build
echo ----- EXEC -----
if [ ! -z "$SYSTEMATICS" ]; then
        if [ -z "$MC" ]; then
                echo "ERROR: Systematics requested on data"
                exit 1
        fi
        SYSTS_JET=("JET_BJES_Response"
                "JET_EffectiveNP_Detector1"
                "JET_EffectiveNP_Detector2"
                "JET_EffectiveNP_Mixed1"
                "JET_EffectiveNP_Mixed2"
                "JET_EffectiveNP_Mixed3"
                "JET_EffectiveNP_Modelling1"
                "JET_EffectiveNP_Modelling2"
                "JET_EffectiveNP_Modelling3"
                "JET_EffectiveNP_Modelling4"
                "JET_EffectiveNP_Statistical1"
                "JET_EffectiveNP_Statistical2"
                "JET_EffectiveNP_Statistical3"
                "JET_EffectiveNP_Statistical4"
                "JET_EffectiveNP_Statistical5"
                "JET_EffectiveNP_Statistical6"
                "JET_EtaIntercalibration_Modelling"
                "JET_EtaIntercalibration_NonClosure_highE"
                "JET_EtaIntercalibration_NonClosure_negEta"
                "JET_EtaIntercalibration_NonClosure_posEta"
                "JET_EtaIntercalibration_TotalStat"
                "JET_Flavor_Composition"
                "JET_Flavor_Response"
                "JET_JER_DataVsMC"
                "JET_JER_EffectiveNP_1"
                "JET_JER_EffectiveNP_10"
                "JET_JER_EffectiveNP_11"
                "JET_JER_EffectiveNP_12restTerm"
                "JET_JER_EffectiveNP_2"
                "JET_JER_EffectiveNP_3"
                "JET_JER_EffectiveNP_4"
                "JET_JER_EffectiveNP_5"
                "JET_JER_EffectiveNP_6"
                "JET_JER_EffectiveNP_7"
                "JET_JER_EffectiveNP_8"
                "JET_JER_EffectiveNP_9"
                "JET_Pileup_OffsetMu"
                "JET_Pileup_OffsetNPV"
                "JET_Pileup_PtTerm"
                "JET_Pileup_RhoTopology"
                "JET_PunchThrough_MC16"
                "JET_SingleParticle_HighPt")
        echo "NOMINAL"
        select-triggers ${MC} ${LUMI} ${YEAR} ${PTCUT} ${NBJETS} ${TTBARSELECTION} ${TRIGMAP} -o nominal.root ${INPUT}
        err=$?
        if (( $err != 0 )); then
                echo "--- NOMINAL FAILED WITH ERROR $err ---"
                echo "--- START LOG OUTPUT ---"
                cat select-triggers.log
                echo "--- END LOG OUTPUT ---"
                exit $err
        fi
        mv select-triggers.log nominal.log
        echo RUNNING JET SYSTEMATICS
        for syst in "${SYSTS_JET[@]}"; do
                echo $syst
                select-triggers ${MC} ${LUMI} ${YEAR} --tree "XhhMiniNtupleResolved_${syst}__1up" ${PTCUT} ${NBJETS} ${TTBARSELECTION} ${TRIGMAP} -o ${syst}-UP.root ${INPUT}
                err=$?
                if (( $err != 0 )); then
                        echo "--- ${syst}-UP FAILED WITH ERROR $err ---"
                        echo "--- START LOG OUTPUT ---"
                        cat select-triggers.log
                        echo "--- END LOG OUTPUT ---"
                        exit $err
                fi
                mv select-triggers.log ${syst}-UP.log
                select-triggers ${MC} ${LUMI} ${YEAR} --tree "XhhMiniNtupleResolved_${syst}__1down" ${PTCUT} ${NBJETS} ${TTBARSELECTION} ${TRIGMAP} -o ${syst}-DOWN.root ${INPUT}
                err=$?
                if (( $err != 0 )); then
                        echo "--- ${syst}-DOWN FAILED WITH ERROR $err ---"
                        echo "--- START LOG OUTPUT ---"
                        cat select-triggers.log
                        echo "--- END LOG OUTPUT ---"
                        exit $err
                fi
                mv select-triggers.log ${syst}-DOWN.log
        done        
else
        select-triggers ${MC} ${LUMI} ${YEAR} ${PTCUT} ${NBJETS} ${TTBARSELECTION} ${TRIGMAP} -o nominal.root ${INPUT}
        err=$?
        if (( $err != 0 )); then
        	echo "--- NANONTUPLE (NOMINAL) FAILED WITH ERROR $err ---"
        	echo "--- START LOG OUTPUT ---"
        	cat select-triggers.log
        	echo "--- END LOG OUTPUT ---"
        	exit $err
        fi
        mv select-triggers.log nominal.log
fi
