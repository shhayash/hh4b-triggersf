#include <iostream>
#include <string>
#include <vector>

#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TObject.h>
#include <TParameter.h>
#include <TTree.h>

// This is a macro, why not?
using namespace std;

class ChangeFile : public TObject {
    ClassDef(ChangeFile, 1)
    ChangeFile(TChain& chain, double total);
    double GetVal();

  public:
    Bool_t Notify();

  private:
    const TChain& chain;
    double normalization;
    double total;
};

ClassImp(ChangeFile)
ChangeFile::ChangeFile(TChain& chain, double total)
    : chain(chain), normalization(0.), total(total) {
    chain.SetNotify(static_cast<TObject*>(this));
}

Bool_t ChangeFile::Notify() {
    TFile* file = chain.GetFile();
    auto* isMC = static_cast<TParameter<bool>*>(file->Get("isMC"));
    if (!isMC->GetVal()) {
        normalization = 1.;
        return true;
    }
    else {
        auto* file_norm = static_cast<TParameter<double>*>(file->Get("normalization"));
        normalization = file_norm->GetVal() / total;
    }
    return true;
}

double ChangeFile::GetVal() { return normalization; }

void mergeTrigSF(vector<string> files, string output) {
    if (files.size() < 1) {
        cout << "ERROR: No input files\n";
        return;
    }
    TFile* out = TFile::Open(output.c_str(), "RECREATE");
    double total = 0.;
    for (const auto& file : files) {
        TFile* f = TFile::Open(file.c_str());
        auto* isMC = static_cast<TParameter<bool>*>(f->Get("isMC"));
        if (!isMC->GetVal()) {
            cout << "ERROR: This script is for MC only\n";
            return;
        }
        else {
            auto* file_norm = static_cast<TParameter<double>*>(f->Get("normalization"));
            total += file_norm->GetVal();
        }
        f->Close();
    }

    cout << "Total: " << total << "\n";

    for (const char* tree_name : {"baseline"}) {
        cout << "Processing tree " << tree_name << "\n";
        bool ok = false;
        TChain chain{tree_name, tree_name};
        for (const auto& file : files) {
            ok = chain.Add(file.c_str(), -1) || ok;
        }

        if (!ok) {
            continue;
        }

        // Normalization gets updated when file changes
        auto norm = ChangeFile(chain, total);

        double mc_sf = 0.;
        chain.SetBranchAddress("mc_sf", &mc_sf);
        out->cd();
        TTree* out_tree = chain.CloneTree(0);
        size_t num_entries = chain.GetEntries();
        for (size_t i = 0; i < num_entries; ++i) {
            chain.GetEntry(i);
            mc_sf = mc_sf * norm.GetVal();
            out_tree->Fill();
        }
        out_tree->Write();
    }

    auto* mc_param = new TParameter<bool>("isMC", true);
    mc_param->Write();
    auto* norm_param = new TParameter<double>("normalization", total);
    norm_param->Write();
}

