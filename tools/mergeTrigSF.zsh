#!/bin/zsh -f

cp $HH4BDIR/../tools/mergeTrigSF.C ./

autoload colors && colors
function print_color() {
    echo -en "\e[$color[$1];0m"
    echo -n $2
    echo -en "\e[0m"
}

function print_color_bold() {
    echo -en "\e[$color[$1];1m"
    echo -n $2
    echo -en "\e[0m"
}

print_color_bold blue "Unpacking Nano NTuples..."
for part in *.root.tgz; do
    name=${part:r:r}
    mkdir -p $name
    cd $name
    tar axf ../$part
    cd ..
done
print_color_bold green "  DONE\n"

print_color_bold blue "Merging files...\n"
for f in $(tar tf *.root.tgz([1])); do
    echo -n $f'... '
    if [[ $f == *Mini* ]];
    then
      echo "skipping\n"
    else
      root -b -q './mergeTrigSF.C+O('"$(echo */$f | sed 's/^/{"/; s/$/"}/; s/ /", "/g')"', "'$f'")' > /dev/null 2> /dev/null
      print_color green "done\n"
    fi
done
print_color_bold green "DONE\n"

rm -r mergeTrigSF* *(/)
