/// \file utils.h
/// General utilities
///

#pragma once
#include <array>
#include <cmath>
#include <limits>
#include <random>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <fmt/format.h>
#include <boost/operators.hpp>
#include <range/v3/all.hpp>

#include <TFile.h>
#include <TLorentzVector.h>
#include <TROOT.h>
#include <ROOT/RDataFrame.hxx>

#define XXH_INLINE_ALL
#include "xxhash.h"

using ranges::to;
constexpr double GeV = 1; ///< Set to 1 -- energies and momenta in GeV


/// Lepton
class Lepton {
  public:
    TLorentzVector p4; ///< 4-momentum
    bool iso;
};


/// LVL1Jet
class LVL1Jet : boost::less_than_comparable<LVL1Jet>, boost::equality_comparable<LVL1Jet> {
  public:
    TLorentzVector p4;          ///< 4-momentum
    // Emulated L1 trigger decision
    bool isL1Emulated_b2j2_High, isL1Emulated_b2j2_Low, isL1Emulated_b2j1_High, isL1Emulated_b2j1_Low, isL1Emulated_b2j0_Base, isL1Emulated_b1j0_Base;


    LVL1Jet() : p4(), isL1Emulated_b2j2_High(false), isL1Emulated_b2j2_Low(false), isL1Emulated_b2j1_High(false), isL1Emulated_b2j1_Low(false), 
                      isL1Emulated_b2j0_Base(false), isL1Emulated_b1j0_Base(false) {}
    LVL1Jet(float E, float pT, float eta, float phi)
        : p4(), isL1Emulated_b2j2_High(false), isL1Emulated_b2j2_Low(false), 
                isL1Emulated_b2j1_High(false), isL1Emulated_b2j1_Low(false),
                isL1Emulated_b2j0_Base(false), isL1Emulated_b1j0_Base(false) {
        p4.SetPtEtaPhiE(pT, eta, phi, E);
    }
};

/// Wrapper around LVL1Jet constructor
inline LVL1Jet make_LVL1jet(float E, float pT, float eta, float phi) {
    // Because a constructor can't be used as a Callable
    return LVL1Jet(E, pT, eta, phi);
}

inline bool operator==(const LVL1Jet& lhs, const LVL1Jet& rhs) {
    const float ZERO_LIMIT = 1.e-5;
    return (std::abs(lhs.p4.Pt()-rhs.p4.Pt())<ZERO_LIMIT) 
             && (std::abs(lhs.p4.Eta()-rhs.p4.Eta())<ZERO_LIMIT) && (std::abs(lhs.p4.Phi()-rhs.p4.Phi())<ZERO_LIMIT);
}


/// HLTJet
class HLTJet : boost::less_than_comparable<HLTJet>, boost::equality_comparable<HLTJet> {
  public:
    TLorentzVector p4;         ///< 4-momentum
    bool isHLTEmulated;        ///< passed or failed on single Jet trigger in the emulation method?

    HLTJet() : p4(), isHLTEmulated(false) {}
    HLTJet(float E, float pT, float eta, float phi)
        : p4(), isHLTEmulated(false) {
        p4.SetPtEtaPhiE(pT, eta, phi, E);
    }
};

/// Wrapper around HLTJet constructor
inline HLTJet make_HLTjet(float E, float pT, float eta, float phi) {
    // Because a constructor can't be used as a Callable
    return HLTJet(E, pT, eta, phi);
}

inline bool operator==(const HLTJet& lhs, const HLTJet& rhs) {
    const float ZERO_LIMIT = 1.e-5;
    return (std::abs(lhs.p4.Pt()-rhs.p4.Pt())<ZERO_LIMIT) 
             && (std::abs(lhs.p4.Eta()-rhs.p4.Eta())<ZERO_LIMIT) && (std::abs(lhs.p4.Phi()-rhs.p4.Phi())<ZERO_LIMIT);
}


/// Jet
class Jet : boost::less_than_comparable<Jet>, boost::equality_comparable<Jet> {
  public:
    TLorentzVector p4; ///< 4-momentum
    bool tagged;       ///< Is jet B-tagged?

    int  isL1Matched;
    int  b2j2_low_isHLTMatched;
    bool b2j2_low_isHLTEmulated,  b2j2_low_isL1Emulated;  
    int  b2j2_high_isHLTMatched;
    bool b2j2_high_isHLTEmulated, b2j2_high_isL1Emulated;  
    int  b2j1_low_isHLTMatched;
    bool b2j1_low_isHLTEmulated,  b2j1_low_isL1Emulated;  
    int  b2j1_high_isHLTMatched;
    bool b2j1_high_isHLTEmulated, b2j1_high_isL1Emulated;  
    int  b2j0_base_isHLTMatched;
    bool b2j0_base_isHLTEmulated, b2j0_base_isL1Emulated;  
    int  b1j0_base_isHLTMatched;
    bool b1j0_base_isHLTEmulated, b1j0_base_isL1Emulated;  

    Jet() : p4(), tagged(false), isL1Matched(0), 
            b2j2_low_isHLTMatched(0),  b2j2_low_isHLTEmulated(false),  b2j2_low_isL1Emulated(false),  
            b2j2_high_isHLTMatched(0), b2j2_high_isHLTEmulated(false), b2j2_high_isL1Emulated(false), 
            b2j1_low_isHLTMatched(0),  b2j1_low_isHLTEmulated(false),  b2j1_low_isL1Emulated(false),  
            b2j1_high_isHLTMatched(0), b2j1_high_isHLTEmulated(false), b2j1_high_isL1Emulated(false), 
            b2j0_base_isHLTMatched(0), b2j0_base_isHLTEmulated(false), b2j0_base_isL1Emulated(false), 
            b1j0_base_isHLTMatched(0), b1j0_base_isHLTEmulated(false), b1j0_base_isL1Emulated(false) {}

    Jet(float E, float pT, float eta, float phi, bool tagged)
        : p4(), tagged(tagged), isL1Matched(0), 
          b2j2_low_isHLTMatched(0),  b2j2_low_isHLTEmulated(false),  b2j2_low_isL1Emulated(false),  
          b2j2_high_isHLTMatched(0), b2j2_high_isHLTEmulated(false), b2j2_high_isL1Emulated(false), 
          b2j1_low_isHLTMatched(0),  b2j1_low_isHLTEmulated(false),  b2j1_low_isL1Emulated(false),  
          b2j1_high_isHLTMatched(0), b2j1_high_isHLTEmulated(false), b2j1_high_isL1Emulated(false), 
          b2j0_base_isHLTMatched(0), b2j0_base_isHLTEmulated(false), b2j0_base_isL1Emulated(false), 
          b1j0_base_isHLTMatched(0), b1j0_base_isHLTEmulated(false), b1j0_base_isL1Emulated(false) {
        p4.SetPtEtaPhiE(pT, eta, phi, E);
    }
};

/// Wrapper around Jet constructor
inline Jet make_jet(float E, float pT, float eta, float phi, bool tagged) {
    // Because a constructor can't be used as a Callable
    return Jet(E, pT, eta, phi, tagged);
}

/// Compare jets by b-tag then p<SUB>T</SUB>
inline bool operator<(const Jet& lhs, const Jet& rhs) {
    if (lhs.tagged == rhs.tagged) {
        return lhs.p4.Pt() < rhs.p4.Pt();
    }
    else {
        return lhs.tagged < rhs.tagged;
    }
}

inline bool operator==(const Jet& lhs, const Jet& rhs) {
    return (lhs.tagged == rhs.tagged) && (lhs.p4.Pt() == rhs.p4.Pt());
}


class JetSummaryInfo {
  public:
    int nLVL1Jet;
    int nEFJet, nSplitJet, nGSCJet;

    int lvl1_1st, lvl1_2nd, lvl1_3rd, lvl1_4th;
    int b2j2_high_1st, b2j2_high_2nd, b2j2_high_3rd, b2j2_high_4th;
    int b2j2_low_3rd,  b2j2_low_4th;
    int b2j1_high_1st, b2j1_low_2nd,  b2j1_low_3rd;
    int b2j0_base_1st, b2j0_base_2nd;
    int b1j0_base_1st;

    JetSummaryInfo () : nLVL1Jet(0), nEFJet(0), nSplitJet(0), nGSCJet(0),
                        lvl1_1st(-1), lvl1_2nd(-1), lvl1_3rd(-1), lvl1_4th(-1),
                        b2j2_high_1st(-1), b2j2_high_2nd(-1), b2j2_high_3rd(-1), b2j2_high_4th(-1),
                        b2j2_low_3rd(-1),  b2j2_low_4th(-1),
                        b2j1_high_1st(-1), b2j1_low_2nd(-1),  b2j1_low_3rd(-1),
                        b2j0_base_1st(-1), b2j0_base_2nd(-1), b1j0_base_1st(-1) {}
};


/*******   Trigger configuration   *******/

std::vector<std::string> trigListSplit(std::string str, std::string separator) {
    if (separator == "") return {str};
    std::vector<std::string> result;
    auto separator_length = separator.length();
    auto offset = std::string::size_type(0);
    while (1) {
        auto pos = str.find(separator, offset);
        if (pos == std::string::npos) {
            result.push_back(str.substr(offset));
            break;
        }
        result.push_back(str.substr(offset, pos - offset));
        offset = pos + separator_length;
    }
  
    if (result.size()!=2) return result;
  
    result[0].pop_back(); // HLT
    result[1] = separator + result[1]; // L1
    return result;
}


std::string suptrigJet(std::string suptrig) {
    if (suptrig == "") return suptrig;
    std::string jet = "EFJet";
    if (suptrig.find("gsc")!=std::string::npos) jet = "GSCJet";
    else if (suptrig.find("split")!=std::string::npos) jet = "SplitJet";
    return jet;
}



/// Main Trigger 
std::string b2j2trig(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_2j35_btight_2j35_L13J25.0ETA23"},
        {"2016", "HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25"},
        {"2017", "HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25"},
        {"2018", "HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25"}
    };
    return trigger[year];
};

std::string b2j1trig(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j100_2j55_bmedium"},
        {"2016", "HLT_j100_2j55_bmv2c2060_split"},
        {"2017", "HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30"},
        {"2018", "HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30"}
    };
    return trigger[year];
};

std::string b2j0trig(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", ""},
        {"2016", ""},
        {"2017", "HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21"},
        {"2018", "HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21"}
    };
    return trigger[year];
};

std::string b1j0trig(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j225_bloose"},
        {"2016", "HLT_j225_bmv2c2060_split"},
        {"2017", "HLT_j225_gsc300_bmv2c1070_split"},
        {"2018", "HLT_j225_gsc300_bmv2c1070_split"}
    };
    return trigger[year];
};
 
// Supporting Trigger for Jet Level Trigger Efficienncy
std::string suptrig_b2j2_high(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j35_L1_J25.0ETA23"},  
        {"2016", "HLT_j35_boffperf_split_L1_J15.0ETA25"},  
        {"2017", "HLT_j15_gsc35_boffperf_split_L1_J15.0ETA25"}, 
        {"2018", "HLT_j35_boffperf_split_L1_J15.0ETA25"}
    };
    return trigger[year];
};
std::string suptrig_b2j2_low(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j35_L1_NO"}, 
        {"2016", "HLT_j35_L1_J15.0ETA25"},
        {"2017", "HLT_j15_gsc35_boffperf_split_L1_J15.0ETA25"},
        {"2018", "HLT_j35_L1_J15.0ETA25"}
    };
    return trigger[year];
};

std::string suptrig_b2j1_high(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j100_L1_J75"}, 
        {"2016", "HLT_j100_L1_J75"}, 
        {"2017", "HLT_j110_gsc150_boffperf_split_L1_J85"},
        {"2018", "HLT_j110_gsc150_boffperf_split_L1_J85"}
    };
    return trigger[year];
};
std::string suptrig_b2j1_low(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j55_L1_J20"},  
        {"2016", "HLT_j55_boffperf_split_L1_J20"},  
        {"2017", "HLT_j35_gsc55_boffperf_split_L1_J30"},
        {"2018", "HLT_j45_gsc55_boffperf_split_L1_J30"}
    };
    return trigger[year];
};
 
std::string suptrig_b2j0_base(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j100_L1_J75"}, // fake 
        {"2016", "HLT_j100_L1_J75"}, // fake  
        {"2017", "HLT_j35_gsc55_boffperf_split_L1_NO"},
        {"2018", "HLT_j45_gsc55_boffperf_split_L1_NO"}
    };
    return trigger[year];
};

std::string suptrig_b1j0_base(std::string year) {
    std::map<std::string, std::string> trigger = {
        {"2015", "HLT_j225_L1_J100"}, 
        {"2016", "HLT_j225_boffperf_split_L1_J100"}, 
        {"2017", "HLT_j225_gsc300_boffperf_split_L1_J100"},
        {"2018", "HLT_j225_gsc300_boffperf_split_L1_J100"}
    };
    return trigger[year];
};

float getL1EtThr(std::string trigger) {
    std::string L1trig = trigListSplit(trigger, "L1")[1];
    std::map<std::string, float> L1thrMap{
        {"L1_NO",          0.00}, 
        {"L1_J15.ETA21",  15.01}, 
        {"L1_J15.0ETA25", 15.01}, 
        {"L1_J15",        15.01},
        {"L1_J20",        20.01},
        {"L1_J25.0ETA23", 25.01}, 
        {"L1_J30",        30.01},
        {"L1_J75",        75.01},
        {"L1_J85",        85.01},
        {"L1_J100",      100.01}
    };
    if (L1thrMap.count(L1trig) == 0) return 0;
    return L1thrMap[L1trig]; 
}

float getL1EtaThr(std::string trigger) {
    std::string L1trig = trigListSplit(trigger, "L1")[1];
    std::map<std::string, float> L1thrMap{
        {"L1_NO",       999.9}, 
        {"L1_J15.ETA21",  2.1},
        {"L1_J15.0ETA25", 2.5},
        {"L1_J15",      999.9},
        {"L1_J20",      999.9},
        {"L1_J25.0ETA23", 2.3},
        {"L1_J30",      999.9},
        {"L1_J75",      999.9},
        {"L1_J85",      999.9},
        {"L1_J100",     999.9}
    };
    if (L1thrMap.count(L1trig) == 0) return 999.9;
    return L1thrMap[L1trig]; 
}

float getHLTthr(std::string hltchain) {
    std::string hltName = trigListSplit(hltchain, "L1")[0];
    std::map<std::string, float> HLTthrMap{
        {"HLT_j15_gsc35_boffperf_split",    35},
        {"HLT_j35",                         35},
        {"HLT_j35_bperf",                   35},
        {"HLT_j35_boffperf",                35},
        {"HLT_j35_boffperf_split",          35},
        {"HLT_j35_gsc55_boffperf_split",    55},
        {"HLT_j45_gsc55_boffperf_split",    55},
        {"HLT_j55",                         55},
        {"HLT_j55_bperf",                   55},
        {"HLT_j55_boffperf_split",          55},
        {"HLT_j100",                       100},
        {"HLT_j100_bperf",                 100},
        {"HLT_j100_boffperf",              100},
        {"HLT_j110_gsc150_boffperf_split", 150},
        {"HLT_j225",                       225},
        {"HLT_j225_bperf",                 225},
        {"HLT_j225_boffperf_split",        225},
        {"HLT_j225_gsc300_boffperf_split", 300}
    };
    if (HLTthrMap.count(hltName) == 0) return 0;
    return HLTthrMap[hltName]; 
}

// Macro for template arg list
#define write_tree2                                                                    \
    Book<int, long long, double, float, double,                                        \
         bool, bool, bool, bool, bool, bool,                                           \
         std::vector<Jet>, JetSummaryInfo, std::vector<Lepton>, std::vector<Lepton>,   \
         float, float, float>

//  Hide from ROOTCLING
#if !defined(__ROOTCLING__)
#include "OutputTree.h"
/// Template OutputTree
// Ignore unused parameters in this section
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

// use macro to ensure argument lists are always the same
#define BRANCH_ARGS2                                                                  \
    int run_number, long long event_number, double mc_sf,                             \
    float met, double Mtw, bool pass_mutrig, bool pass_eltrig,                        \
    bool pass_b2j2, bool pass_b2j1, bool pass_b2j0, bool pass_b1j0,                   \
    const std::vector<Jet> all_jets, const JetSummaryInfo jetInfo,                    \
    const std::vector<Lepton> muons, const std::vector<Lepton> els,                   \
    float L1_HT, float HLT_HT, float Off_HT

OutputTree output_format2{
      "template", nullptr, 1,
      // Event level
      std::tuple{"run_number", [](BRANCH_ARGS2) { return run_number; }},
      std::tuple{"event_number", [](BRANCH_ARGS2) { return event_number; }},
      std::tuple{"mc_sf", [](BRANCH_ARGS2) { return mc_sf; }},
      std::tuple{"ntag", [](BRANCH_ARGS2) { 
                             int ntag = ranges::count_if(all_jets, [](auto&& jet) { return jet.tagged; });
                             return ntag; }},
      std::tuple{"njets", [](BRANCH_ARGS2) { 
                             int njets = all_jets.size();
                             return njets; }},
      std::tuple{"nmuons", [](BRANCH_ARGS2) { 
                             int nmuons = ranges::count_if(muons, [](auto&& lepton) {
                                              return (lepton.p4.Pt() > 25 * GeV) and lepton.iso; }); 
                             return nmuons; }},
      std::tuple{"nels", [](BRANCH_ARGS2) { 
                             int nels   = ranges::count_if(els, [](auto&& lepton) {
                                              return (lepton.p4.Pt() > 25 * GeV) and lepton.iso; });
                             return nels; }},
      std::tuple{"met", [](BRANCH_ARGS2) { return met; }},
      std::tuple{"mtw", [](BRANCH_ARGS2) { return Mtw; }},

      // HLT decision
      std::tuple{"pass_mutrig", [](BRANCH_ARGS2) { return pass_mutrig; }},
      std::tuple{"pass_eltrig", [](BRANCH_ARGS2) { return pass_eltrig; }},
      std::tuple{"pass_b2j2", [](BRANCH_ARGS2) { return pass_b2j2; }},
      std::tuple{"pass_b2j1", [](BRANCH_ARGS2) { return pass_b2j1; }},
      std::tuple{"pass_b2j0", [](BRANCH_ARGS2) { return pass_b2j0; }},
      std::tuple{"pass_b1j0", [](BRANCH_ARGS2) { return pass_b1j0; }},

      std::tuple{"nLVL1Jet", [](BRANCH_ARGS2) { return jetInfo.nLVL1Jet; }},
      std::tuple{"nEFJet", [](BRANCH_ARGS2) { return jetInfo.nEFJet; }},
      std::tuple{"nSplitJet", [](BRANCH_ARGS2) { return jetInfo.nSplitJet; }},
      std::tuple{"nGSCJet", [](BRANCH_ARGS2) { return jetInfo.nGSCJet; }},

      std::tuple{"HT_L1", [](BRANCH_ARGS2) { return L1_HT; }},
      std::tuple{"HT_HLT", [](BRANCH_ARGS2) { return HLT_HT; }},
      std::tuple{"HT_Off", [](BRANCH_ARGS2) { return Off_HT; }},
 
      // Jet level on offline pT order
      std::tuple{"pT_j1",  [](BRANCH_ARGS2) 
                { if (all_jets.size()>0) return all_jets[0].p4.Pt();  return -999.9; }},
      std::tuple{"eta_j1", [](BRANCH_ARGS2) 
                { if (all_jets.size()>0) return all_jets[0].p4.Eta(); return -999.9; }},
      std::tuple{"phi_j1", [](BRANCH_ARGS2) 
                { if (all_jets.size()>0) return all_jets[0].p4.Phi(); return -999.9; }},
      std::tuple{"tagged_j1", [](BRANCH_ARGS2) 
                { if (all_jets.size()>0) return all_jets[0].tagged;   return false; }},
      std::tuple{"pT_j2",  [](BRANCH_ARGS2) 
                { if (all_jets.size()>1) return all_jets[1].p4.Pt();  return -999.9; }},
      std::tuple{"eta_j2", [](BRANCH_ARGS2) 
                { if (all_jets.size()>1) return all_jets[1].p4.Eta(); return -999.9; }},
      std::tuple{"phi_j2", [](BRANCH_ARGS2) 
                { if (all_jets.size()>1) return all_jets[1].p4.Phi(); return -999.9; }},
      std::tuple{"tagged_j2", [](BRANCH_ARGS2) 
                { if (all_jets.size()>1) return all_jets[1].tagged;   return false; }},
      std::tuple{"pT_j3",  [](BRANCH_ARGS2) 
                { if (all_jets.size()>2) return all_jets[2].p4.Pt();  return -999.9; }},
      std::tuple{"eta_j3", [](BRANCH_ARGS2) 
                { if (all_jets.size()>2) return all_jets[2].p4.Eta(); return -999.9; }},
      std::tuple{"phi_j3", [](BRANCH_ARGS2) 
                { if (all_jets.size()>2) return all_jets[2].p4.Phi(); return -999.9; }},
      std::tuple{"tagged_j3", [](BRANCH_ARGS2) 
                { if (all_jets.size()>2) return all_jets[2].tagged;   return false; }},
      std::tuple{"pT_j4",  [](BRANCH_ARGS2) 
                { if (all_jets.size()>3) return all_jets[3].p4.Pt();  return -999.9; }},
      std::tuple{"eta_j4", [](BRANCH_ARGS2) 
                { if (all_jets.size()>3) return all_jets[3].p4.Eta(); return -999.9; }},
      std::tuple{"phi_j4", [](BRANCH_ARGS2) 
                { if (all_jets.size()>3) return all_jets[3].p4.Phi(); return -999.9; }},
      std::tuple{"tagged_j4", [](BRANCH_ARGS2) 
                { if (all_jets.size()>3) return all_jets[3].tagged;   return false; }},


      // Jet level on L1 pT order
      std::tuple{"L1trig_hasL1Jet_j1", [](BRANCH_ARGS2) { if (jetInfo.lvl1_1st != -1) { return true; } return false; }},
      std::tuple{"L1trig_hasL1Jet_j2", [](BRANCH_ARGS2) { if (jetInfo.lvl1_2nd != -1) { return true; } return false; }},
      std::tuple{"L1trig_hasL1Jet_j3", [](BRANCH_ARGS2) { if (jetInfo.lvl1_3rd != -1) { return true; } return false; }},
      std::tuple{"L1trig_hasL1Jet_j4", [](BRANCH_ARGS2) { if (jetInfo.lvl1_4th != -1) { return true; } return false; }},
      std::tuple{"L1trig_hasL1Jet_HT", [](BRANCH_ARGS2) { if (jetInfo.nLVL1Jet > 0) return true; return false;}},
 
      std::tuple{"L1trig_nth_j1", [](BRANCH_ARGS2) { return jetInfo.lvl1_1st+1; }},
      std::tuple{"L1trig_pT_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].p4.Pt(); } return -999.9; }},
      std::tuple{"L1trig_eta_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].p4.Eta(); } return -999.9; }},
      std::tuple{"L1trig_phi_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].p4.Phi(); } return -999.9; }},
      std::tuple{"L1trig_tagged_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].tagged; } return false; }},

      std::tuple{"L1trig_nth_j2", [](BRANCH_ARGS2) { return jetInfo.lvl1_2nd+1; }},
      std::tuple{"L1trig_pT_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].p4.Pt(); } return -999.9; }},
      std::tuple{"L1trig_eta_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].p4.Eta(); } return -999.9; }},
      std::tuple{"L1trig_phi_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].p4.Phi(); } return -999.9; }},
      std::tuple{"L1trig_tagged_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].tagged; } return false; }},

      std::tuple{"L1trig_nth_j3", [](BRANCH_ARGS2) { return jetInfo.lvl1_3rd+1; }},
      std::tuple{"L1trig_pT_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].p4.Pt(); } return -999.9; }},
      std::tuple{"L1trig_eta_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].p4.Eta(); } return -999.9; }},
      std::tuple{"L1trig_phi_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].p4.Phi(); } return -999.9; }},
      std::tuple{"L1trig_tagged_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].tagged; } return false; }},

      std::tuple{"L1trig_nth_j4", [](BRANCH_ARGS2) { return jetInfo.lvl1_4th+1; }},
      std::tuple{"L1trig_pT_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_4th != -1) { return all_jets[jetInfo.lvl1_4th].p4.Pt(); } return -999.9; }},
      std::tuple{"L1trig_eta_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_4th != -1) { return all_jets[jetInfo.lvl1_4th].p4.Eta(); } return -999.9; }},
      std::tuple{"L1trig_phi_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_4th != -1) { return all_jets[jetInfo.lvl1_4th].p4.Phi(); } return -999.9; }},
      std::tuple{"L1trig_tagged_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_4th != -1) { return all_jets[jetInfo.lvl1_4th].tagged; } return false; }},

      // b2j2 support trigger
      std::tuple{"L1trig_isL1Emulated_b2j2_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j2_high_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j2_high_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j2_high_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_4th != -1) { return all_jets[jetInfo.lvl1_4th].b2j2_high_isL1Emulated; } return false; }},

      std::tuple{"L1trig_isL1Emulated_b2j2_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].b2j2_low_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j2_low_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_4th != -1) { return all_jets[jetInfo.lvl1_4th].b2j2_low_isL1Emulated; } return false; }},

      // b2j1 support trigger
      std::tuple{"L1trig_isL1Emulated_b2j1_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].b2j1_high_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j1_low_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].b2j1_low_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j1_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_3rd != -1) { return all_jets[jetInfo.lvl1_3rd].b2j1_low_isL1Emulated; } return false; }},

      // b2j0 support trigger
      std::tuple{"L1trig_isL1Emulated_b2j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].b2j0_base_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j0_base_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_2nd != -1) { return all_jets[jetInfo.lvl1_2nd].b2j0_base_isL1Emulated; } return false; }},
      std::tuple{"L1trig_isL1Emulated_b2j0_HT",  [](BRANCH_ARGS2) 
                {bool result = false; if (L1_HT > 190.01) result = true; return result;}},

      // b1j0 base support trigger
      std::tuple{"L1trig_isL1Emulated_b1j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.lvl1_1st != -1) { return all_jets[jetInfo.lvl1_1st].b1j0_base_isL1Emulated; } return false; }},


      // Jet level on HLT pT order
      // b2j2 support trigger
      std::tuple{"HLTrig_nth_b2j2_high_j1", [](BRANCH_ARGS2)
                    { return jetInfo.b2j2_high_1st+1; }},
      std::tuple{"HLTrig_pT_b2j2_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j2_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j2_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j2_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j2_high_j1",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j2_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j2_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j2_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_1st != -1) { return all_jets[jetInfo.b2j2_high_1st].b2j2_high_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j2_high_j2", [](BRANCH_ARGS2)
                    { return jetInfo.b2j2_high_2nd+1; }},
      std::tuple{"HLTrig_pT_b2j2_high_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j2_high_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j2_high_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j2_high_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j2_high_j2",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j2_high_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j2_high_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j2_high_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_2nd != -1) { return all_jets[jetInfo.b2j2_high_2nd].b2j2_high_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j2_high_j3", [](BRANCH_ARGS2)
                    { return jetInfo.b2j2_high_3rd+1; }},
      std::tuple{"HLTrig_pT_b2j2_high_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j2_high_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j2_high_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j2_high_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j2_high_j3",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j2_high_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j2_high_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j2_high_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_3rd != -1) { return all_jets[jetInfo.b2j2_high_3rd].b2j2_high_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j2_high_j4", [](BRANCH_ARGS2)
                    { return jetInfo.b2j2_high_4th+1; }},
      std::tuple{"HLTrig_pT_b2j2_high_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j2_high_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j2_high_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j2_high_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j2_high_j4",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j2_high_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].b2j2_high_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j2_high_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j2_high_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_high_4th != -1) { return all_jets[jetInfo.b2j2_high_4th].b2j2_high_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j2_low_j3", [](BRANCH_ARGS2)
                    { return jetInfo.b2j2_low_3rd+1; }},
      std::tuple{"HLTrig_pT_b2j2_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j2_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j2_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j2_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j2_low_j3",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j2_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].b2j2_low_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j2_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j2_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_3rd != -1) { return all_jets[jetInfo.b2j2_low_3rd].b2j2_low_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j2_low_j4", [](BRANCH_ARGS2)
                    { return jetInfo.b2j2_low_4th+1; }},
      std::tuple{"HLTrig_pT_b2j2_low_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j2_low_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j2_low_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j2_low_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j2_low_j4",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j2_low_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].b2j2_low_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j2_low_j4",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j2_low_j4", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j2_low_4th != -1) { return all_jets[jetInfo.b2j2_low_4th].b2j2_low_isHLTEmulated; } return false; }},


      // b2j1 support trigger
      std::tuple{"HLTrig_nth_b2j1_high_j1", [](BRANCH_ARGS2)
                    { return jetInfo.b2j1_high_1st+1; }},
      std::tuple{"HLTrig_pT_b2j1_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j1_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j1_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j1_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j1_high_j1",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j1_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].b2j1_high_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j1_high_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j1_high_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_high_1st != -1) { return all_jets[jetInfo.b2j1_high_1st].b2j1_high_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j1_low_j2", [](BRANCH_ARGS2)
                    { return jetInfo.b2j1_low_2nd+1; }},
      std::tuple{"HLTrig_pT_b2j1_low_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j1_low_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j1_low_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j1_low_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j1_low_j2",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j1_low_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].b2j1_low_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j1_low_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j1_low_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_2nd != -1) { return all_jets[jetInfo.b2j1_low_2nd].b2j1_low_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j1_low_j3", [](BRANCH_ARGS2)
                    { return jetInfo.b2j1_low_3rd+1; }},
      std::tuple{"HLTrig_pT_b2j1_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j1_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j1_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j1_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j1_low_j3",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j1_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].b2j1_low_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j1_low_j3",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j1_low_j3", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j1_low_3rd != -1) { return all_jets[jetInfo.b2j1_low_3rd].b2j1_low_isHLTEmulated; } return false; }},


      // b2j0 base support trigger
      std::tuple{"HLTrig_nth_b2j0_base_j1", [](BRANCH_ARGS2)
                    { return jetInfo.b2j0_base_1st+1; }},
      std::tuple{"HLTrig_pT_b2j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j0_base_j1",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].b2j0_base_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_1st != -1) { return all_jets[jetInfo.b2j0_base_1st].b2j0_base_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_nth_b2j0_base_j2", [](BRANCH_ARGS2)
                    { return jetInfo.b2j0_base_2nd+1; }},
      std::tuple{"HLTrig_pT_b2j0_base_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b2j0_base_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b2j0_base_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b2j0_base_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b2j0_base_j2",[](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j0_base_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].b2j0_base_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b2j0_base_j2",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j0_base_j2", [](BRANCH_ARGS2) 
                    { if (jetInfo.b2j0_base_2nd != -1) { return all_jets[jetInfo.b2j0_base_2nd].b2j0_base_isHLTEmulated; } return false; }},

      std::tuple{"HLTrig_hasL1Jet_b2j0_HT",  [](BRANCH_ARGS2) 
                    { if (jetInfo.nLVL1Jet > 0) return true; return false; }},
      std::tuple{"HLTrig_isL1Emulated_b2j0_HT",  [](BRANCH_ARGS2) 
                    { bool result = false; if (L1_HT > 190.01) result = true; return result; }},
      std::tuple{"HLTrig_hasHLTJet_b2j0_HT",  [](BRANCH_ARGS2) 
                    { if (jetInfo.nEFJet > 0) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j0_HT",  [](BRANCH_ARGS2) 
                    { bool result = false; if (HLT_HT > 300) result = true; return result; }},
      std::tuple{"HLTrig_isHLTEmulated_b2j0_HT1000",  [](BRANCH_ARGS2) 
                    { bool result = false; if (HLT_HT > 1000) result = true; return result; }},


      // b1j0 base support trigger
      std::tuple{"HLTrig_nth_b1j0_base_j1", [](BRANCH_ARGS2)
                    { return jetInfo.b1j0_base_1st+1; }},
      std::tuple{"HLTrig_pT_b1j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].p4.Pt(); } return -999.9; }},
      std::tuple{"HLTrig_eta_b1j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].p4.Eta(); } return -999.9; }},
      std::tuple{"HLTrig_phi_b1j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].p4.Phi(); } return -999.9; }},
      std::tuple{"HLTrig_tagged_b1j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].tagged; } return false; }},
      std::tuple{"HLTrig_hasL1Jet_b1j0_base_j1",[](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].isL1Matched >= 1;} return false; }},
      std::tuple{"HLTrig_isL1Emulated_b1j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].b1j0_base_isL1Emulated; } return false; }},
      std::tuple{"HLTrig_hasHLTJet_b1j0_base_j1",  [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) return true; return false; }},
      std::tuple{"HLTrig_isHLTEmulated_b1j0_base_j1", [](BRANCH_ARGS2) 
                    { if (jetInfo.b1j0_base_1st != -1) { return all_jets[jetInfo.b1j0_base_1st].b1j0_base_isHLTEmulated; } return false; }}

      };
// warn about unused parameters again
#pragma GCC diagnostic pop
#endif
