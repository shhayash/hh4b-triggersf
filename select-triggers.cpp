/// \file resolved-recon.cpp
/// \brief Main implementation file
///
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <fcntl.h>
#include <sys/stat.h>
#include <sysexits.h>

#include <fmt/format.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>
#include <cxxopts.hpp>
#include <range/v3/all.hpp>

#include <TH1I.h>
#include <TParameter.h>
#include <TROOT.h>
#include <TTree.h>
#include <ROOT/RDataFrame.hxx>

#include "MCConfig.h"
#include "TriggerTest.h"
#include "select-triggers-utils.h"

using ROOT::RDataFrame;
namespace view = ranges::views;
namespace action = ranges::actions;
namespace fs = std::filesystem;

std::string tagger = "DL1r";                    ///< B-tagging algorithm
std::string working_point = "FixedCutBEff_77";  ///< B-tagging working point
std::string year = "2015";                      ///< Year
float pTcut = 35;                               ///< Jet pT cut value
int nbjets = 2;                                 ///< number of b-jet requirement
bool ttbarSelection = false;                    ///< Disable Jet selection

///< Our jet trigger names
std::string trigger_b2j2 = "", trigger_b2j1 = "", trigger_b2j0 = "", trigger_b1j0 = "";
std::string supName_b2j2_low  = "", supName_b2j2_high = "", supName_b2j1_low = "", supName_b2j1_high = "",
            supName_b2j0_base = "", supName_b1j0_base = "";

///< Trigger name to get HLT jets for b2j2, b2j1, b2j0,b1j0 trigger
std::string supJet_b2j2_low  = "", supJet_b2j2_high = "", supJet_b2j1_low = "", supJet_b2j1_high = "",
            supJet_b2j0_base = "", supJet_b1j0_base = "";

// adjacent strings are auto concatenated
#define varPref "resolvedJets_"      ///< Branch prefix for resolved jets
#define truthPref "truth_"           ///< Branch prefix for truth particle
#define muonPref "muon_"             ///< Branch prefix for muons
#define elPref "el_"                 ///< Branch prefix for electrons
#define isoPref "isIsolated_FCTight" ///< Branch prefix for isolated leptons
#define metPref "metFinalTrk"        ///< Branch prefix for MET


/// if the jet is included into refJets, this function will return 'true'.
template<typename T>
bool checkUsedJets(const T& jet, const std::vector<T>& refJets) {
    bool result = false;
    for (const auto& refJet: refJets) {
        if (refJet == jet) result = true;
    }
    return result;
}


/// Emulate L1 trigger decisions
void emulateL1trig(std::vector<LVL1Jet>& jets)  {
    for (auto&& jet : jets) {
        // b2j2 High
        if (jet.p4.E() > getL1EtThr(supName_b2j2_high) and abs(jet.p4.Eta()) < getL1EtaThr(supName_b2j2_high)) jet.isL1Emulated_b2j2_High = true;
        // b2j2 Low
        if (jet.p4.E() > getL1EtThr(supName_b2j2_low)  and abs(jet.p4.Eta()) < getL1EtaThr(supName_b2j2_low))  jet.isL1Emulated_b2j2_Low  = true;
        // b2j1 High
        if (jet.p4.E() > getL1EtThr(supName_b2j1_high) and abs(jet.p4.Eta()) < getL1EtaThr(supName_b2j1_high)) jet.isL1Emulated_b2j1_High = true;
        // b2j1 Low
        if (jet.p4.E() > getL1EtThr(supName_b2j1_low)  and abs(jet.p4.Eta()) < getL1EtaThr(supName_b2j1_low))  jet.isL1Emulated_b2j1_Low  = true;
        // b2j0 Base
        if (jet.p4.E() > getL1EtThr(supName_b2j0_base) and abs(jet.p4.Eta()) < getL1EtaThr(supName_b2j0_base)) jet.isL1Emulated_b2j0_Base = true;
        // b1j0 Base
        if (jet.p4.E() > getL1EtThr(supName_b1j0_base) and abs(jet.p4.Eta()) < getL1EtaThr(supName_b1j0_base)) jet.isL1Emulated_b1j0_Base = true;
    }
}

bool emulateHLTrig(const HLTJet& hltjet, const std::string trig) {
    bool decision = false;
    if (hltjet.p4.E() > getHLTthr(trig)) decision = true;
    return decision;
}

/// Build LVL1Jets
std::vector<LVL1Jet> build_LVL1jets(const std::vector<Float_t>& et8,
                                    const std::vector<Float_t>& eta, const std::vector<Float_t>& phi) {
    // build L1 jets
    std::vector<LVL1Jet> L1Jets = view::zip_with(make_LVL1jet, et8, et8, eta, phi)
                            | view::filter([](const auto& jet) {
                                  return abs(jet.p4.Eta()) < 2.5;
                              })
                            | to<std::vector>;
    emulateL1trig(L1Jets);
   
    ranges::sort(L1Jets, std::greater{}, [](const auto& jet) {
       return jet.p4.E();
    });

    return L1Jets;
}


/// Build HLTJets
std::vector<HLTJet> build_HLTjets(const std::vector<Float_t>& et_hlt,  const std::vector<Float_t>& pt_hlt, 
                                  const std::vector<Float_t>& eta_hlt, const std::vector<Float_t>& phi_hlt) {
    // build HLT jets
    std::vector<HLTJet> HLTJets = view::zip_with(make_HLTjet, et_hlt, pt_hlt, eta_hlt, phi_hlt)
                            | view::filter([](const auto& jet) {
                                  return abs(jet.p4.Eta()) < 2.5;
                              })
                            | to<std::vector>;

    // sort by pT score 
    ranges::sort(HLTJets, std::greater{}, [](const auto& jet) {
       return jet.p4.Pt();
    });
   
    return HLTJets;
}


/// LVL1 and Offline matching
void attachL1info(std::vector<Jet>& jets, const std::vector<LVL1Jet>& lvl1jets)  {

    std::vector<LVL1Jet> dupLVL1jets;
    dupLVL1jets.clear();
    int nth = 0; // LVL 1st, 2nd, 3rd ...
    for (const auto& lvl1jet : lvl1jets) {
        if (checkUsedJets(lvl1jet, dupLVL1jets)) continue;
        dupLVL1jets.push_back(lvl1jet);
        ++nth;
        int nth_off = 0;
        double min_dR = 1.;
        for (size_t i=0 ; i<jets.size(); ++i) {
            double dR = lvl1jet.p4.DeltaR(jets[i].p4);
            if (jets[i].isL1Matched >= 1) continue;
            if (dR < min_dR) { min_dR = dR; nth_off = i; }
        }
 
        if (min_dR < 0.4) {
            jets[nth_off].isL1Matched = nth;
            jets[nth_off].b2j2_high_isL1Emulated = lvl1jet.isL1Emulated_b2j2_High;
            jets[nth_off].b2j2_low_isL1Emulated  = lvl1jet.isL1Emulated_b2j2_Low;
            jets[nth_off].b2j1_high_isL1Emulated = lvl1jet.isL1Emulated_b2j1_High;
            jets[nth_off].b2j1_low_isL1Emulated  = lvl1jet.isL1Emulated_b2j1_Low;
            jets[nth_off].b2j0_base_isL1Emulated = lvl1jet.isL1Emulated_b2j0_Base;
            jets[nth_off].b1j0_base_isL1Emulated = lvl1jet.isL1Emulated_b1j0_Base;
        }
    }
}

/// HLT and Offline matching
void attachHLTinfo(std::vector<Jet>& jets, 
                   const std::vector<HLTJet>& hltjets, const std::string hltName) {

    std::vector<HLTJet> dupHLTjets;
    dupHLTjets.clear();
    int nth = 0; // HLT 1st, 2nd, 3rd ...
    for (const auto& hltjet : hltjets) {
        if (checkUsedJets(hltjet, dupHLTjets)) continue;
        dupHLTjets.push_back(hltjet);
        ++nth;
        int nth_off = 0;
        double min_dR = 1.;
        for (size_t i=0 ; i<jets.size(); ++i) {
            if (hltName == "b2j2_low"  and jets[i].b2j2_low_isHLTMatched  >= 1) continue;
            if (hltName == "b2j2_high" and jets[i].b2j2_high_isHLTMatched >= 1) continue;
            if (hltName == "b2j1_low"  and jets[i].b2j1_low_isHLTMatched  >= 1) continue;
            if (hltName == "b2j1_high" and jets[i].b2j1_high_isHLTMatched >= 1) continue;
            if (hltName == "b2j0_base" and jets[i].b2j0_base_isHLTMatched >= 1) continue;
            if (hltName == "b1j0_base" and jets[i].b1j0_base_isHLTMatched >= 1) continue;
            double dR = hltjet.p4.DeltaR(jets[i].p4);
            if (dR < min_dR) { min_dR = dR; nth_off = i; }
        }
        if (min_dR < 0.30) {
            if (hltName == "b2j2_low")  { jets[nth_off].b2j2_low_isHLTMatched   = nth;
                                          jets[nth_off].b2j2_low_isHLTEmulated  = emulateHLTrig(hltjet, supName_b2j2_low); }
            if (hltName == "b2j2_high") { jets[nth_off].b2j2_high_isHLTMatched  = nth;
                                          jets[nth_off].b2j2_high_isHLTEmulated = emulateHLTrig(hltjet, supName_b2j2_high);}
            if (hltName == "b2j1_low")  { jets[nth_off].b2j1_low_isHLTMatched   = nth;
                                          jets[nth_off].b2j1_low_isHLTEmulated  = emulateHLTrig(hltjet, supName_b2j1_low);}
            if (hltName == "b2j1_high") { jets[nth_off].b2j1_high_isHLTMatched  = nth;
                                          jets[nth_off].b2j1_high_isHLTEmulated = emulateHLTrig(hltjet, supName_b2j1_high);}
            if (hltName == "b2j0_base") { jets[nth_off].b2j0_base_isHLTMatched  = nth;
                                          jets[nth_off].b2j0_base_isHLTEmulated = emulateHLTrig(hltjet, supName_b2j0_base);}
            if (hltName == "b1j0_base") { jets[nth_off].b1j0_base_isHLTMatched  = nth;
                                          jets[nth_off].b1j0_base_isHLTEmulated = emulateHLTrig(hltjet, supName_b1j0_base);}
        }
    }
}

/// Build OfflineJets and Attach L1Jet & HLTJet info.
std::vector<Jet> build_jets(const std::vector<Float_t>& E,   const std::vector<Float_t>& pT, const std::vector<Float_t>& eta, 
                            const std::vector<Float_t>& phi, const std::vector<int>& tagged, const std::vector<LVL1Jet>& lvl1_jets,
                            const std::vector<HLTJet>& b2j2_low,  const std::vector<HLTJet>& b2j2_high,
                            const std::vector<HLTJet>& b2j1_low,  const std::vector<HLTJet>& b2j1_high,
                            const std::vector<HLTJet>& b2j0_base, const std::vector<HLTJet>& b1j0_base) {

    std::vector<Jet> jets = view::zip_with(make_jet, E, pT, eta, phi, tagged)
                            | view::filter([](const auto& jet) {
                                  return jet.p4.Pt() >= pTcut * GeV and abs(jet.p4.Eta()) < 2.5;
                              })
                            | to<std::vector>;
    // sort by pT score 
    ranges::sort(jets, std::greater{}, [](const auto& jet) {
       return jet.p4.Pt();
    });


    // matching and attaching L1 jets into offline jets
    attachL1info(jets, lvl1_jets);

    // matching and attaching HLT jets into offline jets
    attachHLTinfo(jets, b2j2_low,  "b2j2_low");
    attachHLTinfo(jets, b2j2_high, "b2j2_high");
    attachHLTinfo(jets, b2j1_low,  "b2j1_low");
    attachHLTinfo(jets, b2j1_high, "b2j1_high");
    attachHLTinfo(jets, b2j0_base, "b2j0_base");
    attachHLTinfo(jets, b1j0_base, "b1j0_base");

    return jets;
}

JetSummaryInfo makeJetSummary( const std::vector<Jet>& jets,
                               const std::vector<Float_t> lvl1Jets,  const std::vector<Float_t> efJets, 
                               const std::vector<Float_t> splitJets, const std::vector<Float_t> gscJets) {

    JetSummaryInfo summaryInfo;
    summaryInfo.nLVL1Jet  = lvl1Jets.size();
    summaryInfo.nEFJet    = efJets.size();
    summaryInfo.nSplitJet = splitJets.size();
    summaryInfo.nGSCJet   = gscJets.size();

    for (size_t i=0; i < jets.size(); ++i) {
        
        if (jets[i].isL1Matched == 1) summaryInfo.lvl1_1st = i;
        if (jets[i].isL1Matched == 2) summaryInfo.lvl1_2nd = i;
        if (jets[i].isL1Matched == 3) summaryInfo.lvl1_3rd = i;
        if (jets[i].isL1Matched == 4) summaryInfo.lvl1_4th = i;

        if (jets[i].b2j2_high_isHLTMatched == 1) summaryInfo.b2j2_high_1st = i;
        if (jets[i].b2j2_high_isHLTMatched == 2) summaryInfo.b2j2_high_2nd = i;
        if (jets[i].b2j2_high_isHLTMatched == 3) summaryInfo.b2j2_high_3rd = i;
        if (jets[i].b2j2_high_isHLTMatched == 4) summaryInfo.b2j2_high_4th = i;

        if (jets[i].b2j2_low_isHLTMatched  == 3) summaryInfo.b2j2_low_3rd = i;
        if (jets[i].b2j2_low_isHLTMatched  == 4) summaryInfo.b2j2_low_4th = i;

        if (jets[i].b2j1_high_isHLTMatched == 1) summaryInfo.b2j1_high_1st = i;
        if (jets[i].b2j1_low_isHLTMatched  == 2) summaryInfo.b2j1_low_2nd = i;
        if (jets[i].b2j1_low_isHLTMatched  == 3) summaryInfo.b2j1_low_3rd = i;

        if (jets[i].b2j0_base_isHLTMatched == 1) summaryInfo.b2j0_base_1st = i;
        if (jets[i].b2j0_base_isHLTMatched == 2) summaryInfo.b2j0_base_2nd = i;

        if (jets[i].b1j0_base_isHLTMatched == 1) summaryInfo.b1j0_base_1st = i;
   }
   return summaryInfo;
}

float calculateL1HT ( const std::vector<LVL1Jet>& jets ) {
    // We use L1HT190-J15s5.ETA21, 
    // https://twiki.cern.ch/twiki/bin/view/Atlas/TrigBjetEmulation#HT_Js
    // 1. Consider only the top 5 jets with |eta| < 3.1.
    auto jets_eta31 = jets | view::filter([](const auto& jet) {
                                 return abs(jet.p4.Eta()) < 3.1;
                             });
    // 2. Calculate the transverse enrgey (Et) sum of jets with Et > 15 and |eta| < 2.1. 
    float HT = 0.0;
    int size = 0;
    std::vector<LVL1Jet> used_jets;
    used_jets.clear();
    for (const auto& jet : jets_eta31) {
        if (size >= 5) break;
        if (checkUsedJets(jet, used_jets)) continue; // because there are duplicate jets
        if (jet.p4.E() > 15.01 and abs(jet.p4.Eta()) < 2.1) 
            { HT += abs(jet.p4.E()); used_jets.push_back(jet); }
        ++size;
    }
    return HT;
}


float calculateHLTHT ( const std::vector<float>& et, const std::vector<float>& eta ) {
    // In HLT, we use HLT_2j35_gsc55_bmv2c1050_split_ht300, 
    // which requires the transverse enrgey (Et) sum of all HLT (EF) jets 
    // with Et > 30 GeV and abs(Eta) < 3.2 to be above 300 GeV.
    // https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrigBjetEmulation#HT
    float HT = 0.0;
    std::vector<float> used_et;
    used_et.clear();
    for (size_t i=0; i<et.size(); ++i) {
        if (checkUsedJets(et[i], used_et)) continue; // because there are duplicate jets
        if (et[i] > 30 and abs(eta[i]) < 3.2) { HT += abs(et[i]); used_et.push_back(et[i]); }
    }
    return HT;
}


float calculateHT ( const std::vector<Float_t>& pT, const std::vector<Float_t>& eta ) {
    // In offline, we calculate the pT sum of all offline jets with pT > 25 and |eta| < 2.5.
    int i = 0;
    float HT = 0.0;
    for (auto& pT_i : pT) {
        if (pT_i > 25 and std::abs(eta[i]) < 2.5) HT += pT_i;
        i++;
    }
    return HT;
}


std::vector<Lepton> build_leptons(const std::vector<Float_t>& lepton_m, const std::vector<Float_t>& lepton_pT,
                                  const std::vector<Float_t>& lepton_eta, const std::vector<Float_t>& lepton_phi,
                                  const std::vector<int>& lepton_iso) {
    std::vector<Lepton> leptons = view::zip_with(
          [](float m, float pT, float eta, float phi, int is_iso) {
              Lepton temp;
              temp.p4.SetPtEtaPhiM(pT, eta, phi, m);
              temp.iso = is_iso;
              return temp;
          }, lepton_m, lepton_pT, lepton_eta, lepton_phi, lepton_iso)
          | view::filter([](const auto& lepton) {
                return lepton.p4.Pt() > 30 * GeV;
          }) | to<std::vector>;
    return leptons; 
}


// Calculate leptonic W transverse mass
// Based on event selection in https://cds.cern.ch/record/2127958/files/ATLAS-CONF-2016-001.pdf
double mtw(const std::vector<Lepton>& muons,
           const float& w_missEt, const float& w_missPhi) {
    if (muons.empty()) return 0;
    const Lepton w_lep1 = muons[0];
    double Mjlv = sqrt(2*w_lep1.p4.Pt()*w_missEt
                           *(1-std::cos(w_lep1.p4.Phi()-w_missPhi)));
    return Mjlv;
}


/// \Event selections 
/// to get pure ttbar semi-leptonic events
bool run_bounds(const int run_number) {

    int min_run_bound = 0, max_run_bound = 999999;
    if (year == "2015") {min_run_bound = 0;      max_run_bound = 296939;}
    if (year == "2016") {min_run_bound = 296939; max_run_bound = 324320;}
    if (year == "2017") {min_run_bound = 324320; max_run_bound = 348197;}
    if (year == "2018") {min_run_bound = 348197; max_run_bound = 364486;}

    return (run_number > min_run_bound and run_number < max_run_bound);
}

/// \brief Require at least four jets with p<SUB>T</SUB> > 40 GeV, |&eta;| < 2.5
/// with at least 2 being b-tagged (disable).
bool four_j_pT_40_eta_25_2b(const std::vector<Int_t>& tagged, const std::vector<Float_t>& pT,
                            const std::vector<Float_t>& eta) {
    //if (!ttbarSelection) return true;

    // Filter jets with 4 pT > 40 GeV, |eta| < 2.5 jets with >= 2 b tagged
    int count = 0;
    int b_count = 0;
    for (std::size_t i = 0; i < tagged.size(); ++i) {
        if (pT[i] > pTcut * GeV and abs(eta[i]) < 2.5) {
            count++;
            if (tagged[i]) {
                b_count++;
            }
        }
        if (count >= 4 and b_count >= nbjets) return true;
    }
    return false;
}


/// Require exactly one muon and no electron with p<SUB>T</SUB> > 25 GeV
bool lepone_filter(const std::vector<Lepton>& muons, const std::vector<Lepton>& els) {
    if (!ttbarSelection) return true;

    const int nmuons = ranges::count_if(muons, [](auto&& lepton) { 
                          return (lepton.p4.Pt() > 30 * GeV) and lepton.iso; });
    const int nels   = ranges::count_if(els, [](auto&& lepton) { 
                          return (lepton.p4.Pt() > 30 * GeV) and lepton.iso; });
    return (nmuons==1 and nels==0);
}

/// Require MET > 20 GeV
bool met_filter(const float& met) {
    if (!ttbarSelection) return true;
    return met > 20 * GeV;
}

/// Require MET+Mtw > 60 GeV
bool mtw_filter(const float& met, const double& mtw) {
    if (!ttbarSelection) return true;
    return met+mtw > 60 * GeV;
}


/// lepton trigger filter
bool triglepton_filter(const std::map<std::string, double>& triggerMap) {
    if (!ttbarSelection) return true;
    std::string eliso_trigger = "HLT_e26_lhtight_nod0_ivarloose";
    std::string muiso_trigger = "HLT_mu26_ivarmedium";
    if (year == "2015") {
        eliso_trigger = "HLT_e24_lhtight_iloose";
        muiso_trigger = "HLT_mu24_imedium";
    }
    if (triggerMap.count(muiso_trigger)==0)
        return false;
    return true;
}

/// muon isolation trigger filter
bool trigmuon_filter(const std::map<std::string, double>& triggerMap) {
    std::string muiso_trigger = "HLT_mu26_ivarmedium";
    if (year == "2015") muiso_trigger = "HLT_mu24_imedium";

    if (triggerMap.count(muiso_trigger)==0) return false;
    return true;
}

/// electron isolation trigger filter
bool trigelec_filter(const std::map<std::string, double>& triggerMap) {
    std::string eliso_trigger = "HLT_e26_lhtight_nod0_ivarloose";
    if (year == "2015") eliso_trigger = "HLT_e24_lhtight_iloose";
    if (triggerMap.count(eliso_trigger)==0) return false;
    return true;
}

/// b-jet trigger combination filter
bool bjet_filter(const std::map<std::string, double>& triggerMap) {
    if (triggerMap.count(trigger_b2j2)==0 and triggerMap.count(trigger_b2j1)==0 
            and triggerMap.count(trigger_b2j0)==0 and triggerMap.count(trigger_b1j0)==0) 
        return false;
    return true;
}

/// only 2b+2j trigger filter
bool b2j2_filter(const std::map<std::string, double>& triggerMap)  {
    if (triggerMap.count(trigger_b2j2)==0) return false;
    return true;
}

/// only 2b+1j trigger filter
bool b2j1_filter(const std::map<std::string, double>& triggerMap)  {
    if (triggerMap.count(trigger_b2j1)==0) return false;
    return true;
}

/// only 2b+0j trigger filter
bool b2j0_filter(const std::map<std::string, double>& triggerMap)  {
    if (triggerMap.count(trigger_b2j0)==0) return false;
    return true;
}

/// only 1b+0j trigger filter
bool b1j0_filter(const std::map<std::string, double>& triggerMap)  {
    if (triggerMap.count(trigger_b1j0)==0) return false;
    return true;
}

/// main analysis code 
int main(int argc, char* argv[]) {

    // Use iostreams for input only and fmt::print for output only, so fine
    std::ios::sync_with_stdio(false); // Needed for fast iostreams
    using vec_string = std::vector<std::string>;
    // Log to file when running on grid
    bool debugging = false;
    if (std::getenv("RES_RECO_ON_GRID")) {
        spdlog::set_default_logger(spdlog::basic_logger_mt("log", "select-triggers.log"));
        if (std::getenv("ATHENA_PROC_NUMBER")) {
            int num_thread = std::atoi(std::getenv("ATHENA_PROC_NUMBER"));
            if (num_thread >= 2) {
                ROOT::EnableImplicitMT(num_thread);
            }
        }
    }
    else {
#ifndef NDEBUG
        spdlog::flush_on(spdlog::level::debug);
        spdlog::set_pattern("[%Y-%m-%d %T (%z)] [%t] [%^%l%$] %v [%@]");
#else
        spdlog::flush_on(spdlog::level::info);
        spdlog::set_pattern("[%Y-%m-%d %T (%z)] [%t] [%^%l%$] %v");
#endif
        ROOT::EnableImplicitMT();
        //debugging = true;
    }

    struct { // unnamed struct holding mc info
        bool is_mc = false;
        double lumi = 1.0;
        std::unordered_map<int, double> normalizations;
    } mc;

    // SETUP STUFF
    // JUMP TO ~~~ TO GET TO THE ANALYSIS CODE
    //
    // command-line option parsing
    cxxopts::Options options("select-triggers",
                             "Calculate jet trigger scale factors used at our HH4b analysis");
    // clang-format off
    options.add_options("main")
        ("i,input", "Input file", cxxopts::value<vec_string>())
        ("m,mc", "MC", cxxopts::value<bool>())
        ("lumi", "Luminosity to normalize to (MC only)", cxxopts::value<double>()->default_value("1"))
        ("tree", "Tree name", cxxopts::value<std::string>()->default_value("XhhMiniNtuple"))
        ("tagger", "tagger", cxxopts::value<std::string>()->default_value("DL1r"))
        ("working_point", "working_point", cxxopts::value<std::string>()->default_value("FixedCutBEff_77"))
        ("t,trigger-list","Trigger list for jet trigger SF study", cxxopts::value<std::string>())
        ("y,year", "Year", cxxopts::value<std::string>()->default_value("2015"))
        ("o,output", "Output file", cxxopts::value<std::string>()->
                                    default_value("selected.root"))
        // additinal options
        ("pTcut", "Jet pT cut value", cxxopts::value<float>()->default_value("35"))
        ("nbjets", "Number of b-jets requirement", cxxopts::value<int>()->default_value("2"))
        ("ttbarSelection", "Enable ttbar selections", cxxopts::value<bool>()->default_value("false"))
        ("trigger-map", "Write trigger map tree", cxxopts::value<bool>()->default_value("false"))
        ("h,help", "Help", cxxopts::value<bool>())
    ;
    // clang-format on

    options.parse_positional("input");
    options.positional_help("input file");
    auto parsed_options = options.parse(argc, argv);
    if (parsed_options.count("help")) {
        fmt::print("{}", options.help({}));
        return 0;
    }
    if (parsed_options.count("input") < 1) {
        spdlog::error("No input files");
        return 1;
    }

    std::string trigger_file = "";
    if (parsed_options.count("trigger-list") < 1) {
        spdlog::info("No trigger-list provided, using default trigger list");
        trigger_file = "${HH4BDIR}/trigger-list-SF.dat";
    }
    else if (parsed_options.count("trigger-list") == 1) {
        spdlog::info("Using provided list of triggers");
        trigger_file = parsed_options["trigger-list"].as<std::string>();
    }

    if (parsed_options["mc"].as<bool>()) {
        mc.is_mc = true;
        mc.lumi = parsed_options["lumi"].as<double>();
    }
    auto pass_triggers = TriggerTest(trigger_file);

    tagger = parsed_options["tagger"].as<std::string>();
    working_point = parsed_options["working_point"].as<std::string>();

    const std::string tagged_name = fmt::format(varPref "is_{}_{}", tagger, working_point);
    const std::string tagged_quantile = fmt::format(varPref "Quantile_{}_Continuous", tagger);
    const std::string btag_sf_name = fmt::format(varPref "SF_{}_{}", tagger, working_point);
    const std::string btag_syst = "nominal";
    // immediately invoked lambda pattern
    const std::size_t btag_syst_index = [](const std::string& name) -> std::size_t {
        using namespace std::literals;
        // use the preprocessor to put list in a separate file
        // clang-format off
        const std::map<std::string, std::size_t> systematics = {
            #include "data/ft_systematics.inc"
        };
        // clang-format on

        if (name == "nominal"s) {
            return 0;
        }
        if (auto it = systematics.find(name); it != systematics.end()) {
            return it->second;
        }
        else {
            spdlog::error("Invalid flavour tagging systematic");
            std::exit(EX_USAGE);
        }
    }(btag_syst);
    
    pTcut = parsed_options["pTcut"].as<float>();
    nbjets = parsed_options["nbjets"].as<int>();
    if (parsed_options["ttbarSelection"].as<bool>()) ttbarSelection = true;
    spdlog::info("Apply jet pT cut with {} GeV and {} b-tag", pTcut, nbjets);
    spdlog::info("Enable ttbar semi-leptonic selection?     --> {}", ttbarSelection);

    // start processing events
    const std::string tree_name  = parsed_options["tree"].as<std::string>();
    const vec_string input_files = parsed_options["input"].as<vec_string>();
    std::string output_filename  = parsed_options["output"].as<std::string>();
    vec_string files_to_use{};

    double sum_weights = 0.;
    double total_sum_weights = 0.;
    double n_events_initial = 0.;
    double n_events_selected = 0.;
    int mcChannelNumber = 0;
    MCConfig mc_config("${HH4BDIR}/xsec.tsv");
    bool trees_exist = false; // Check if we actually have any input trees
    for (const auto& input : input_files) {
        TFile* input_file = TFile::Open(input.c_str());
        if (input_file->IsZombie()) {
            spdlog::warn("Zombie file skipped: {}", input);
            input_file->Close();
            continue;
        }
        if (input_file->TestBit(TFile::kRecovered)) {
            spdlog::warn("Recovered file NOT skipped: {}", input);
        }
        const bool have_tree = input_file->GetListOfKeys()->Contains(tree_name.c_str());
        trees_exist = trees_exist || have_tree;
        if (!have_tree) {
            // Skip files that don't have the tree we want
            spdlog::info("Skipping file {} -- no {} tree", fs::path(input).filename().string(),
                         tree_name);
            input_file->Close();
            continue;
        }
        TH1D* metadata = dynamic_cast<TH1D*>(input_file->Get("MetaData_EventCount_XhhMiniNtuple"));

        sum_weights = metadata->GetBinContent(3); // sum_weights_initial bin
        total_sum_weights += sum_weights; // To store the normalization when we run on the grid
        n_events_initial += sum_weights;  // For the cutflow, even if it is the same as above
        n_events_selected += metadata->GetBinContent(4);
        if (mc.is_mc) {
            TTree* ntuple = dynamic_cast<TTree*>(input_file->Get(tree_name.c_str()));
            ntuple->SetBranchStatus("*", 0);
            ntuple->SetBranchStatus("mcChannelNumber", 1);
            ntuple->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
            ntuple->GetEntry(0);

            // unordered_map default initializes the double if new dsid
            mc.normalizations[mcChannelNumber] += sum_weights;
        }
        input_file->Close();
        files_to_use.push_back(input);
    }
    if (!trees_exist) {
        spdlog::warn("No input file contains the tree {}. Writing dummy output file {}.",
                     output_filename);
        auto out = TFile(output_filename.c_str(), "RECREATE");
        out.cd();
        auto hist = TH1I("empty_nnts", "Number of Empty Nano NTuples", 1, 0, 2);
        hist.Fill(1);
        hist.Write();
        out.Close();
        std::exit(EX_OK);
    }
    if (mc.is_mc) {
        spdlog::info("MC Run. Found {} DSID(s) in input.", mc.normalizations.size());
        spdlog::info("Normalizing to {} /fb", mc.lumi);
        for (auto&& [dsid, weight] : mc.normalizations) {
            const auto& db_entry = mc_config[dsid];
            spdlog::info("DSID: {} -- {}", dsid, db_entry.generator);
            spdlog::info("Cross-section:               {:.6g} fb", db_entry.xsec);
            spdlog::info("K factor:                    {:.6g}", db_entry.k_factor);
            spdlog::info("Generator filter efficiency: {:.6g}", db_entry.gen_filt_eff);
            spdlog::info("Sum of weights:              {:.3g}", weight);
            weight = mc.lumi * (db_entry.xsec * db_entry.k_factor * db_entry.gen_filt_eff) / weight;
        }
    }
    year = parsed_options["year"].as<std::string>();
    spdlog::info("Year {}, go to setup.", year);
    // Main Trigger
    trigger_b2j2 = b2j2trig(year);
    trigger_b2j1 = b2j1trig(year);
    trigger_b2j0 = b2j0trig(year);
    trigger_b1j0 = b1j0trig(year);
    // Support Trigger
    supName_b2j2_low  = suptrig_b2j2_low(year);
    supName_b2j2_high = suptrig_b2j2_high(year);
    supName_b2j1_low  = suptrig_b2j1_low(year);
    supName_b2j1_high = suptrig_b2j1_high(year);
    supName_b2j0_base = suptrig_b2j0_base(year);
    supName_b1j0_base = suptrig_b1j0_base(year);
    // Support Trigger Jet
    supJet_b2j2_low  = supName_b2j2_low+"_"+suptrigJet(supName_b2j2_low);
    supJet_b2j2_high = supName_b2j2_high+"_"+suptrigJet(supName_b2j2_high);
    supJet_b2j1_low  = supName_b2j1_low+"_"+suptrigJet(supName_b2j1_low);
    supJet_b2j1_high = supName_b2j1_high+"_"+suptrigJet(supName_b2j1_high);
    supJet_b2j0_base = supName_b2j0_base+"_"+suptrigJet(supName_b2j0_base);
    supJet_b1j0_base = supName_b1j0_base+"_"+suptrigJet(supName_b1j0_base);

    spdlog::info("b2j2 trigger      : {}", trigger_b2j2);
    spdlog::info("  >> suptrig high : {}", supName_b2j2_high);
    spdlog::info("  >> suptrig low  : {}", supName_b2j2_low);
    spdlog::info("b2j1 trigger      : {}", trigger_b2j1);
    spdlog::info("  >> suptrig high : {}", supName_b2j1_high);
    spdlog::info("  >> suptrig low  : {}", supName_b2j1_low);
    spdlog::info("b2j0 trigger      : {}", trigger_b2j0);
    spdlog::info("  >> suptrig base : {}", supName_b2j0_base);
    spdlog::info("b1j0 trigger      : {}", trigger_b1j0);
    spdlog::info("  >> suptrig base : {}", supName_b1j0_base);

    ///
    /// ~~~ ANALYSIS STARTS HERE ~~~
    ///
    auto frame = RDataFrame(tree_name, files_to_use).Filter("bool(passResolvedGRL)", "GRL");
    if (!frame.HasColumn(tagged_name.c_str())) {
        spdlog::error("{} {} is missing in these MiniNTuples", tagger, working_point);
        spdlog::error("Probably running on out-of-date MiniNTuples");
        std::exit(EX_DATAERR);
    }
    else if (!frame.HasColumn(tagged_quantile.c_str())) {
        spdlog::error("{} PC quantiles are missing in these MiniNTuples", tagger, working_point);
        spdlog::error("Probably running on out-of-date MiniNTuples");
        std::exit(EX_DATAERR);
    }
    // All this because you can't modify TInterfaces in-place
    // Calculate MC scale factor -- 1.0 for data
    auto mc_weight = mc.is_mc ? frame.Define("mc_sf",
                           [&mc, btag_syst_index](Int_t mcChannelNumber, Float_t mcEventWeight,
                                                  Float_t weight_pileup, const std::vector<Float_t>& pt,
                                                  const std::vector<Float_t>& eta,
                                                  const std::vector<std::vector<Float_t>>& btag_sf,
                                                  const std::vector<int>& tagged, long long evt_num) {
                               double sf = mcEventWeight * weight_pileup
                                           * mc.normalizations[mcChannelNumber];
                               // verify pTs are in descending order
                               // If not, we need to map sorted indices to unsorted
                               std::vector<std::size_t> index_map{};
                               if (!ranges::is_sorted(pt, std::greater{})) {
                                   //spdlog::warn("Jets aren't sorted in event {}", evt_num);
                                   std::vector enum_pt = pt | view::enumerate | to<std::vector>;
                                   ranges::stable_sort(enum_pt, std::greater{},
                                                       [](auto&& x) { return std::get<1>(x); });
                                   index_map = enum_pt | view::transform([](auto&& x) {
                                                   return std::get<0>(x);
                                               })
                                               | to<std::vector>;
                               }
                               else index_map = view::indices(pt.size()) | to<std::vector>;

                               // looping over indices is much easier here
                               // stop when number of tagged jets seen = 4
                               int num_tagged = 0;
                               for (std::size_t j = 0; j < pt.size(); ++j) {
                                   std::size_t i = index_map.at(j);
                                   if (tagged[i]) ++num_tagged;
                                   // All relevant jets included here
                                   if (pt[i] >= pTcut * GeV and abs(eta[i]) <= 2.5) {
                                       if (btag_sf[i].size() <= btag_syst_index) {
                                           if (btag_syst_index == 0) {
                                               // Nominal flavor tagging SF missing
                                               spdlog::warn("Missing flavor tagging SF for event "
                                                            "{} jet {}",
                                                            evt_num, i);
                                               continue;
                                           }
                                           else {
                                               spdlog::warn("Missing flavor tagging systematic "
                                                            "variation for event {} jet {}",
                                                            evt_num, i);
                                               continue;
                                           }
                                       }
                                       sf *= btag_sf[i][btag_syst_index];
                                   }
                                   if (num_tagged == 4) break;
                               }
                               return sf;
                           },
                           {"mcChannelNumber", "mcEventWeight", "weight_pileup", varPref "pt",
                            varPref "eta", btag_sf_name, tagged_name, "eventNumber"})
                              : frame.Define("mc_sf", []() { return 1.0; }, {});


    std::vector<std::string> trigger_branch_list{};
    std::vector<std::string> tree_branch_list{};
    if (mc.is_mc) {
        trigger_branch_list = {"rand_run_nr", "passedTriggerHashes"};
        tree_branch_list = {"rand_run_nr", "eventNumber", "mc_sf", metPref "SumEt", "Mtw", 
                            "pass_mutrig", "pass_eltrig", "pass_b2j2", "pass_b2j1", "pass_b2j0", "pass_b1j0", 
                            "jets", "jetInfo", "muons", "electrons", "L1_HT", "HLT_HT", "Off_HT"};
    }
    else {
        trigger_branch_list = {"runNumber", "passedTriggerHashes"};
        tree_branch_list = {"runNumber", "eventNumber", "mc_sf", metPref "SumEt", "Mtw",
                            "pass_mutrig", "pass_eltrig", "pass_b2j2", "pass_b2j1", "pass_b2j0", "pass_b1j0", 
                            "jets", "jetInfo", "muons", "electrons", "L1_HT", "HLT_HT", "Off_HT"};
    }

    // Baseline
    auto run_bound_cut = mc_weight.Filter(run_bounds, {mc.is_mc?"rand_run_nr":"runNumber"});
    auto trigger_map   = run_bound_cut.Define("triggerMap", pass_triggers, trigger_branch_list);

    // Lepton trigger filter
    auto lepton_trigger = trigger_map.Filter(triglepton_filter, {"triggerMap"}, "lepton trigger");
    // Define leptons with  pT > 30 GeV and isolation
    auto with_muons = lepton_trigger.Define("muons", build_leptons,
                          {muonPref "m", muonPref "pt", muonPref "eta", muonPref "phi", muonPref isoPref});
    auto with_electrons = with_muons.Define("electrons", build_leptons,
                          {elPref "m", elPref "pt", elPref "eta", elPref "phi", elPref isoPref});
    // Baseline filters, nleptons == 1 and MET > 20 GeV
    auto lep_result = with_electrons.Filter(lepone_filter, {"muons", "electrons"}, "nleptons == 1");
    auto met_result = lep_result.Filter(met_filter, {metPref "SumEt"}, "MET > 20 GeV");

    // Define transverse mass of the W boson 
    auto with_mtw = met_result.Define("Mtw", mtw, {"muons", metPref "SumEt", metPref "Phi"});

    // Baseline filters, nleptons == 1,  MET > 20 GeV and MET + Mtw > 60 GeV (if enable).
    auto mtw_result = with_mtw.Filter(mtw_filter, {metPref "SumEt", "Mtw"}, "MET + Mtw > 60 GeV");

    // Require to have at least four jets with at least two b-jets(if enable)
    auto four_jets = mtw_result.Filter(four_j_pT_40_eta_25_2b,
                                      {tagged_name, varPref "pt", varPref "eta"},
                                       u8"4 good jets(pT ≥ 40 GeV, η ≤ 2.5), ≥ 2 tagged");

    // Trigger 
    auto with_mutrig = four_jets.Define("pass_mutrig", trigmuon_filter, {"triggerMap"});
    auto with_eltrig = with_mutrig.Define("pass_eltrig", trigelec_filter, {"triggerMap"});
    auto with_b2j2 = with_eltrig.Define("pass_b2j2",b2j2_filter, {"triggerMap"});
    auto with_b2j1 = with_b2j2.Define("pass_b2j1",  b2j1_filter, {"triggerMap"});
    auto with_b2j0 = with_b2j1.Define("pass_b2j0",  b2j0_filter, {"triggerMap"});
    auto with_b1j0 = with_b2j0.Define("pass_b1j0",  b1j0_filter, {"triggerMap"});

    auto with_LVL1Jet = with_b1j0.Define("LVL1Jet", build_LVL1jets,
                            {"LVL1JetRoIs_et8", "LVL1JetRoIs_eta", "LVL1JetRoIs_phi"});

    // Define HLT jets
    auto with_b2j2_low = with_LVL1Jet.Define("b2j2_low", build_HLTjets,
                                      {suptrigJet(supName_b2j2_low)+"_et",  suptrigJet(supName_b2j2_low)+"_pt", 
                                       suptrigJet(supName_b2j2_low)+"_eta", suptrigJet(supName_b2j2_low)+"_phi"});

    auto with_b2j2_high = with_b2j2_low.Define("b2j2_high", build_HLTjets,
                                      {suptrigJet(supName_b2j2_high)+"_et",  suptrigJet(supName_b2j2_high)+"_pt", 
                                       suptrigJet(supName_b2j2_high)+"_eta", suptrigJet(supName_b2j2_high)+"_phi"});

    auto with_b2j1_low = with_b2j2_high.Define("b2j1_low", build_HLTjets,
                                      {suptrigJet(supName_b2j1_low)+"_et",  suptrigJet(supName_b2j1_low)+"_pt", 
                                       suptrigJet(supName_b2j1_low)+"_eta", suptrigJet(supName_b2j1_low)+"_phi"});

    auto with_b2j1_high = with_b2j1_low.Define("b2j1_high", build_HLTjets,
                                      {suptrigJet(supName_b2j1_high)+"_et",  suptrigJet(supName_b2j1_high)+"_pt", 
                                       suptrigJet(supName_b2j1_high)+"_eta", suptrigJet(supName_b2j1_high)+"_phi"});

    auto with_b2j0_base = with_b2j1_high.Define("b2j0_base", build_HLTjets,
                                      {suptrigJet(supName_b2j0_base)+"_et",  suptrigJet(supName_b2j0_base)+"_pt", 
                                       suptrigJet(supName_b2j0_base)+"_eta", suptrigJet(supName_b2j0_base)+"_phi"});
 
    auto with_b1j0_base = with_b2j0_base.Define("b1j0_base", build_HLTjets,
                                      {suptrigJet(supName_b1j0_base)+"_et",  suptrigJet(supName_b1j0_base)+"_pt", 
                                       suptrigJet(supName_b1j0_base)+"_eta", suptrigJet(supName_b1j0_base)+"_phi"});
 
    // Define offline jets and HLT jets
    auto with_offline = with_b1j0_base.Define("jets", build_jets,
                                      {varPref "E", varPref "pt", varPref "eta", varPref "phi", tagged_name, 
                                       "LVL1Jet", "b2j2_low", "b2j2_high", "b2j1_low", "b2j1_high", "b2j0_base", "b1j0_base"});

    auto with_jetInfo = with_offline.Define("jetInfo", makeJetSummary, 
                                            {"jets", "LVL1JetRoIs_et8", "EFJet_et", "SplitJet_et", "GSCJet_et"});

    // Calculate HT
    auto with_L1HT  = with_jetInfo.Define("L1_HT", calculateL1HT, {"LVL1Jet"});
    auto with_HLTHT = with_L1HT.Define("HLT_HT", calculateHLTHT, {"EFJet_et", "EFJet_eta"});
    auto baseline   = with_HLTHT.Define("Off_HT", calculateHT, {varPref "pt", varPref "eta"});

    // Fill into output file
    spdlog::info("Writing to {}", output_filename);
    TFile output_file(output_filename.c_str(), "RECREATE");
    // Write trees
    int num_threads = ROOT::GetImplicitMTPoolSize();
    if (num_threads < 1) {
        num_threads = 1;
    }

    OutputTree baseline_tree("baseline", &output_file, num_threads, output_format2);
    baseline.write_tree2(std::move(baseline_tree), tree_branch_list).GetValue();

    // Write out normalization factor.
    // On grid, MC runs in multiple jobs so events in each job get normalized assuming that is the
    // whole dataset. Writing out these factors lets us fix this later. We'll only ever have one
    // DSID per job on the grid, this means we can write out one factor.
    // After grid jobs finished, you need to run tools/mergeTrigSF.zsh to merge ROOT files into one.
    output_file.cd();
    if (mc.is_mc) {
        // No memory leak. Parameters owned by output_file.
        auto* mc_param = new TParameter<bool>("isMC", true);
        mc_param->Write();
        if (mc.normalizations.size() == 1) {
            auto* dsid_param = new TParameter<int>("DSID", mcChannelNumber);
            auto* norm_param = new TParameter<double>("normalization", total_sum_weights);
            dsid_param->Write();
            norm_param->Write();
        }
    }
    else {
        auto* mc_param = new TParameter<bool>("isMC", false);
        mc_param->Write();
    }

    output_file.Close();

    if (parsed_options["trigger-map"].as<bool>()) {
        if (parsed_options["mc"].as<bool>()) {
            baseline.Snapshot(
                  "triggerList", output_filename,
                  {"triggerMap", "rand_run_nr", "eventNumber", "mcEventWeight"},
                  ROOT::RDF::RSnapshotOptions("UPDATE", ROOT::kZLIB, 1, 0, 99, false));
        }
        else {
            baseline.Snapshot(
                  "triggerList", output_filename, {"triggerMap", "runNumber", "eventNumber"},
                  ROOT::RDF::RSnapshotOptions("UPDATE", ROOT::kZLIB, 1, 0, 99, false));
        }
        spdlog::info("Written trigger map tree");
    }

    // For debugging
    if (debugging) {
        spdlog::info("=== Display # of counts passing triggers ===");
        spdlog::info("--> Baseline");
        spdlog::info("Initial                    : {}", *mc_weight.Count());
        spdlog::info("Lepton trigger             : {}", *lepton_trigger.Count());
        if (ttbarSelection) {
            spdlog::info("Exactly one lepton         : {}", *lep_result.Count());
            spdlog::info("MET > 20 GeV               : {}", *met_result.Count());
            spdlog::info("MET + Mtw > 60 GeV         : {}", *baseline.Count());
            spdlog::info("At least four jets         : {}", *four_jets.Count());
            spdlog::info("   with at least {} b-jets {}", nbjets);
        }
    }

    return 0;
}
