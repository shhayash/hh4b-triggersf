#ifndef __LINKDEF_H_
#define __LINKDEF_H_
// clang-format off
#if defined(__ROOTCLING__)
#pragma link C++ nestedclasses;
#pragma link C++ class LVL1Jet+;
#pragma link C++ class HLTJet+;
#pragma link C++ class Jet+;
#pragma link C++ class Lepton+;
#pragma link C++ class std::map < std::string, double>+;
#pragma link C++ class JetSummaryInfo+;
#endif
#endif // __LINKDEF_H_
