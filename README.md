# hh4b-triggerSF

Produce multi-bjet trigger SFs for HH4b-resolved analysis.

## NTuples
This software works with ntuples produced using [XhhCommon](https://gitlab.cern.ch/hh4b/XhhCommon/).
Suitable NTuples are available on the grid (UPDATE: 2020/06/13):

- `user.shhayash.HH4B.period*.data17..AB21.2.91-APR20-1-JetSF-JUN02-1.min_MiniNTuple.root/`
- `user.shhayash.HH4B.41047*.ttbar.*.AB21.2.91-APR20-1-JetSF-JUN02-1.min_MiniNTuple.root/`
- `user.shhayash.HH4B.364*.vjets.*.AB21.2.91-APR20-1-JetSF-JUN02-1.min_MiniNTuple.root/`
- `user.shhayash.HH4B.363*.diboson.*.AB21.2.91-MAR20-0-JetSF-JUN02-1.min_MiniNTuple.root/`

They are listed on `datasets/*.txt`.

## Running the trigger jet matching with offline jet
The software requires ROOT 6.16 with the latest patches. On lxplus or a tier 3, this can be loaded by using LCG 96b.

Add the following to your `.bashrc` or `.zshrc`:
``` console
alias lcg96b='source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96b x86_64-centos7-gcc8-opt'
```

You can then load the LCG release with `lcg96b`. 
Having done this, the code is built with CMake:
``` console
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```

The executable will then be found at `build/select-triggers`. 
Source `build/setup.sh` to add it to your path. The help text is:
``` console
$ select-triggers --help
Calculate jet trigger scale factors used at our HH4b analysis
Usage:
  select-triggers [OPTION...] input file

 main options:
  -m, --mc                 MC
      --lumi arg           Luminosity to normalize to (MC only) (default: 1)
      --tree arg           Tree name (default: XhhMiniNtuple)
      --tagger arg         tagger (default: DL1r)
      --working_point arg  working_point (default: FixedCutBEff_77)
  -t, --trigger-list arg   Trigger list for jet trigger SF study
  -y, --year arg           Year (default: 2015)
  -o, --output arg         Output file (default: selected.root)
      --pTcut arg          Jet pT cut value (default: 35)
      --nbjets arg         Number of b-jets requirement (default: 2)
      --ttbarSelection     Enable ttbar selections
      --tdtJet             Enable TDT Jet filling
      --trigger-map        Write trigger map tree
  -h, --help               Help
```

### Running on data
It can be run on data with a command like:
``` console
$ source build/setup.sh
$ select-triggers -o data2017.root -i ${input_dir} -y 2017
```
If you want to select pure ttbar semi-leptonic events, it can apply with a command like:
``` console
$ source build/setup.sh
$ select-triggers -o data2017.root -i ${input_dir} -y 2017 --ttbarSelection
```

### Running on MC
When running on MC, the selection must be run on all files at once, to
correctly calculate the event weights. You should give the `-m` or `--mc` option, 
and the  `--lumi` option if you do not wish to normalize to 1 /fb with a command like:
``` console
$ select-triggers -o tnh2017.root -i ${input_dir} -y 2017 --mc --lumi 43.65
```

### Running on data and MC using grid
The tool to submit jobs on grid is `tools/submit-select-triggers.sh`, and you can do with a command like:
``` console
./tools/submit-select-triggers.sh -i datasets/data17.txt
```
If you want to select pure ttbar semi-leptonic events, it can apply with a command like:
``` console
./tools/submit-select-triggers.sh -i datasets/ttbar.txt -t
```
The tool automatically setups the `--mc` and `--lumi` (2015+2016: 27.8, 2017: 43.65, 2018: 58.45) options, 
and submit jobs on grid.


## Making trigger SFs
The tools using root_numpy and Python 3 to validate and make trigger SFs are at `python/`. 
The environment setup for lxplus is python3 LCG 96.

Add the following to your `.bashrc` or `.zshrc`:
``` console
alias lcg96bpy3='source /cvmfs/sft.cern.ch/lcg/views/LCG_96bpython3/x86_64-centos7-gcc8-opt/setup.sh'
```
You can then load the LCG release with `lcg96bpy3`.

### Making multi-bjet trigger SFs on Et cut
You can make trigger Et SFs by `python/makeTrigSF.py`. The help text is:
``` console
$ python python/makeTrigSF.py --help
Usage: makeTrigSF.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  --dat=DAT_FILE        Input data filename
  --tth=TTH_FILE        Input tth filename
  --tnh=TNH_FILE        Input tnh filename
  --vjj=VJJ_FILE        Input vjets filename
  --dib=DIB_FILE        Input diboson filename
  --trig=TRIG           Trigger, where start to take a product of the SFs
                        (default: b2j1)
  --year=YEAR           Year (default: 2016)
  --doL1TurnOn          Enable to show L1 turn on, (default: HLT turn on)
  --applyJetTrig        Apply the requirement that any jet trigger fires
  --ttbarSelection      Enable to apply ttbar selection
  --label=LABEL         Year (default: 2017)
  --disableSF           Disnable to produce trigger SFs
```

You can run it with a command like:
``` console
$ ### make L1 trigger SFs
$ python python/makeTrigSF.py -i $input_dir -o $output_dir --dat $dat --tnh $tnh --tth $tth --vjj $vjj --dib $dib --trig "b2j2" --year "2017" --doL1TurnOn --applyJetTrig --ttbarSelection
$ ### make HLT SFs
$ python python/makeTrigSF.py -i $input_dir -o $output_dir --dat $dat --tnh $tnh --tth $tth --vjj $vjj --dib $dib --trig "b2j2" --year "2017" --applyJetTrig --ttbarSelection
```
After do that, you can find output root file, which name is like `2020-JUN11-13TeV-HH4bJetLevelTriggerScaleFactor-2017.root`. The file will have the result of trigger SFs. 

You can also see other useful python tools at `python/` to validate the emulation method or apply the SFs into HH4b signal samples for quick check.

