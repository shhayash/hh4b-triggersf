#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage
from numpy.lib.recfunctions import append_fields
from rootpy.plotting import *
import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)
import root_numpy
from array import array

from tqdm import tqdm
import datetime

# Define trigSF files
trigSF_dir  = "/eos/user/s/shhayash/HH4b/HH4b-JetSF/trigSF/"
syst_dir    = "/afs/cern.ch/user/s/shhayash/workspace/HH4b/HH4b-JetSF-21.2.91-APR20-1/hh4b-triggersf/pyrun/"

def nths(trig):
    nths = { 'b2j2' : ['1st', '2nd', '3rd', '4th'],
             'b2j1' : ['1st', '2nd', '3rd'],
             'b2j0' : ['1st', '2nd', 'HT'],
             'b1j0' : ['1st'] }
    return nths[trig]


def nthTrigs(trig, year):
    nthTrigs = { 'b2j2' : ['b2j2_high', 'b2j2_high', 'b2j2_low', 'b2j2_low'],
                 'b2j1' : ['b2j1_high', 'b2j1_low', 'b2j1_low'],
                 'b2j0' : ['b2j0_base', 'b2j0_base', 'b2j0_HT'],
                 'b1j0' : ['b1j0_base'] }
    nthTrig = nthTrigs[trig]
    if (trig == 'b2j2' and year == '2015'):
        nthTrig = ['b2j2_high', 'b2j2_high', 'b2j2_high', 'b2j2_low']
    return nthTrig


def run_bounds(year):
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486], '0000' : [0, 999999] }
    return run_bounds[year]

def prefixJetNames(doL1TurnOn):
    prefixJetName = "HLTrig_"
    if doL1TurnOn:
        prefixJetName = "L1trig_"
    return prefixJetName


def mc_lumi(year):
    lumi15 = 3.2
    lumi16 = 24.6
    lumi17 = 43.65
    lumi18 = 58.45 
    lumi = ('2015' in year)*lumi15+('2016' in year)*lumi16+('2017' in year)*lumi17+('2018' in year)*lumi18
    return lumi


def appendTrigDecision(it, nths, label):
    for nth in nths:
        it = append_fields(it, 'pass_'+nth+label, np.full(len(it), False), usemask=False)
    return it


def attachTrigDecision(it, yr_short, trig, doL1TurnOn, doL1andHLTTurnOn):
    nth = nths(trig)
    nthTrig = nthTrigs(trig, '20'+yr_short)
    prefixJetName = prefixJetNames(doL1TurnOn)

    # Reference
    it = appendTrigDecision(it, nth, 'Ref')
    # Trigger
    it = appendTrigDecision(it, nth, 'Tri')

    # Loop by nth jet decisions
    for j in range(len(nthTrig)):
        if doL1TurnOn:
            if nthTrig[j].find('HT') != -1: # -> Go to HT cut
                function = 'HT_Off'
                haveL1_suffix = 'HT'
                deci_suffix   = nthTrig[j]
            else:                           # -> Go to Et cut
                function = prefixJetName+'pT_j'+str(j+1)
                haveL1_suffix = 'j'+str(j+1)
                deci_suffix   = nthTrig[j]+'_j'+str(j+1)
        else:
            if nthTrig[j].find('HT') != -1: # -> Go to HT cut
                function = 'HT_Off'
                deci_suffix  = nthTrig[j]
            else:                           # -> Go to Et cut
                function = prefixJetName+'pT_'+nthTrig[j]+'_j'+str(j+1)
                deci_suffix  = nthTrig[j]+'_j'+str(j+1)
 
 
        print("Filling "+nth[j]+" jet trigger decision ...")
        for i in tqdm(range(len(it))):
            ### L1 and HLT turn in curve ###
            if doL1andHLTTurnOn:
                if (it[function][i] > 0.00001 and it[prefixJetName+'hasL1Jet_'+deci_suffix][i] and it[prefixJetName+'hasHLTJet_'+deci_suffix][i]):
                    # Reference requirement : 
                    it['pass_'+nth[j]+'Ref'][i] = True
                    # Trigger requirement : L1 emulation and HLT emulation
                    if (it[prefixJetName+'isL1Emulated_'+deci_suffix][i] and it[prefixJetName+'isHLTEmulated_'+deci_suffix][i]):
                        it['pass_'+nth[j]+'Tri'][i] = True

            ### L1 turn in curve ###
            elif doL1TurnOn:
                if (it[function][i] > 0.00001 and it[prefixJetName+'hasL1Jet_'+haveL1_suffix][i]):
                    # Reference requirement : 
                    it['pass_'+nth[j]+'Ref'][i] = True
                    # Trigger requirement : L1 emulation 
                    if (it[prefixJetName+'isL1Emulated_'+deci_suffix][i]):
                        it['pass_'+nth[j]+'Tri'][i] = True

            ### HLT turn in curve ###
            else:
                if (it[function][i] > 0.00001 and it[prefixJetName+'hasL1Jet_'+deci_suffix][i] and it[prefixJetName+'hasHLTJet_'+deci_suffix][i]):
                    # Reference requirement : L1 emulation
                    if (it[prefixJetName+'isL1Emulated_'+deci_suffix][i]):
                        it['pass_'+nth[j]+'Ref'][i] = True
                    # Trigger requirement : L1 emulation and HLT emulation
                    if (it[prefixJetName+'isL1Emulated_'+deci_suffix][i] and it[prefixJetName+'isHLTEmulated_'+deci_suffix][i]):
                        it['pass_'+nth[j]+'Tri'][i] = True
    return it


def make_arrs(filename, columns, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn, \
              ttbarPurity = False, sigTrig = False):
    if not filename:
        empty = []
        return empty
    it = root_numpy.root2array(filename,
                                 treename='baseline', branches=columns)

    ### If you want to do any selection, please define here. ###

    year_run_range = run_bounds('20'+yr_short)
    if sigTrig:
        # To check the jet-level trigger efficiency onn HH4b signal, we don't require lepton trigger
        ave_mc_sf = np.mean(it['mc_sf'])
        it = it[(it['mc_sf'] < ave_mc_sf*100) & (it['mc_sf'] > ave_mc_sf*(-100))]
        it = it[(it['run_number'] > year_run_range[0]) & (it['run_number'] < year_run_range[1])]
    else:
        it = it[(it['run_number'] > year_run_range[0]) & (it['run_number'] < year_run_range[1])]
        it = it[(it['pass_mutrig'])]
        if ttbarSelection:
            it = it[(it['ntag'] >= 2)]
            it = append_fields(it, 'met_mtw', (it['met']+it['mtw']), usemask=False)
            it = it[(it['met'] > 20) & (it['met_mtw'] > 60)]
            it = it[(it['nmuons'] == 1)]

    if ttbarPurity:
        nth = ['1st', '2nd', '3rd', '4th', 'HT']
        functions = ['pT_j1', 'pT_j2', 'pT_j3', 'pT_j4', 'HT_Off']

        it = appendTrigDecision(it, nth, 'Base')
        for j in range(len(functions)):
            function = functions[j]
            if function.find('HT') != -1: # -> Go to HT cut
                print("Filling "+nth[j]+" distrubution ...")
            else:
                print("Filling "+nth[j]+" jet distrubution ...")
            for i in tqdm(range(len(it))):
                # Baseline requirement
                if (it[function][i] > 0.00001):
                    it['pass_'+nth[j]+'Base'][i] = True
                else:
                    it['pass_'+nth[j]+'Base'][i] = False
        return it

    # Attach trigger decisions
    it = attachTrigDecision(it, yr_short, trig, doL1TurnOn, doL1andHLTTurnOn)
    return it


################### SYSTEMATIC AND STATISTIC ERRORS CONFIGURATION ###################

def calcEffStatError(numerator, denominator, eff, forUP = False):  # efficiency = numerator/denominator
    if denominator == 0:
        return 0.
    # Bayesian Statistics method
    variance = (numerator+1)*(numerator+2)/((denominator+2)*(denominator+3)) \
                   -(numerator+1)**2/((denominator+2)**2)
    uncertainty = np.sqrt(variance)
    if forUP:
        if eff + uncertainty > 1.0:
            uncertainty = 1.0 - eff

    return uncertainty


def calcSignalSystError(err_sig1, err_sig2, err_sig3, err_sig4, err_sig5, \
                        unc_sig1, unc_sig2, unc_sig3, unc_sig4, unc_sig5):

    errsList = [err_sig1, err_sig2, err_sig3, err_sig4, err_sig5]
    uncsList = [unc_sig1, unc_sig2, unc_sig3, unc_sig4, unc_sig5]
    posi = 0
    nega = 0
    for i in range(len(errsList)):
        if errsList[i] == 0.:
            errsList[i] = 1.0
            uncsList[i] = 99999.9
        else:
            if errsList[i] >= 1.0:
                posi += 1
            else:
                nega += 1
            errsList[i] = abs(1-errsList[i]) + 1

    sum_weight = 0.0
    for i in range(len(uncsList)):
        sum_weight = sum_weight + (1/uncsList[i])**2

    reci_err = 0.0
    for i in range(len(errsList)):
        reci_err = reci_err + (1/uncsList[i])**2*(1/errsList[i])

    if posi < nega:
        return 2 - sum_weight/reci_err
    return sum_weight/reci_err


def getSystErrors(nbin, year, trig, nth, doL1TurnOn, doL1andHLTTurnOn, fullSyst, sfSyst):

    # trigger SF files
    syst_name = "2020-AUG08-13TeV-HH4bSystError-"+year+".root"
    syst_file = ROOT.TFile(syst_dir+syst_name, 'read')
    if doL1andHLTTurnOn:
        trigName = "SystError_"+multibTrigName(year, trig)
    elif doL1TurnOn:
        trigName = "L1SystError_"+multibTrigName(year, trig)
    else:
        trigName = "HLTSystError_"+multibTrigName(year, trig)
    trigName = trigName.replace('.', '-')
    tree = syst_file.Get(trigName)

    syst_err = 0.0
    systList = []
    if fullSyst:
        systList += ["_diff_ttbarGenerator", "_diff_ttbarShower"]
    if sfSyst:
        systList += ["_diff_ttbarVersusSignal"]
    for systName in systList:
        have_syst = tree.GetListOfKeys().Contains(nth+systName)
        if have_syst:
            hist_syst = tree.Get(nth+systName)
            syst_err = np.sqrt(abs(1-hist_syst.GetBinContent(nbin))**2 + syst_err**2)
        else:
            print("WARNING: Not found " + systName)
    return syst_err


def convSFError(stat_err, syst_err, mc_eff, dat_eff):
    if mc_eff == 0:
        return 0
    propagation = np.sqrt( (1/mc_eff*stat_err)**2 + (1/(mc_eff)*syst_err)**2 )
    return propagation


################### ROOT CONFIGURATION ###################

def ATLASLabel(x, y, size, text1, text2, text3, small=1.0, dely=0.072, sepAT=1.0):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextSize(size)
    delx = sepAT*0.0945*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())
    l.DrawLatex(x,y,"ATLAS")
    if (text1):
        p = ROOT.TLatex() 
        p.SetNDC()
        p.SetTextFont(42)
        p.SetTextSize(size-0.005);
        p.DrawLatex(x+delx,y,text1)
    if (text2):
        p.DrawLatex(x,y-dely,text2)
    if (text3):
        ps = ROOT.TLatex() 
        ps.SetNDC()
        ps.SetTextFont(42)
        ps.SetTextSize((size-0.005)*small);
        ps.DrawLatex(x,y-dely*2,text3)
    return 1

def Latex(x, y, size, text1):
    p = ROOT.TLatex() 
    p.SetNDC()
    p.SetTextFont(42)
    p.SetTextSize(size)
    p.DrawLatex(x,y,text1)
    return 1

def DateLatex(pad = 2):
    d_today = datetime.date.today()
    if pad == 1:
        #Latex(0.75, 0.930, 0.031, d_today.strftime('%B %d, %Y'))
        Latex(0.75, 0.930, 0.031, "AUG08, 2020")
    else:
        #Latex(0.80, 0.962, 0.031, d_today.strftime('%B %d, %Y'))
        Latex(0.80, 0.962, 0.031, "AUG08, 2020")
    return 1

def TEGraph(n, x, y, ex, ey):
    xx = array('d', x)
    yy = array('d', y)
    exx = array('d', ex)
    eyy = array('d', ey)
    return ROOT.TGraphErrors(n, xx, yy, exx, eyy)

def TEAsymGraph(n, x, y, exdown, exup, eydown, eyup):
    xx = array('d', x)
    yy = array('d', y)
    exxdown = array('d', exdown)
    exxup   = array('d', exup)
    eyydown = array('d', eydown)
    eyyup   = array('d', eyup)
    return ROOT.TGraphAsymmErrors(n, xx, yy, exxdown, exxup, eyydown, eyyup)

def x_label_dict(column):
    func  = column
    if column.find('L1trig')!=-1 or column.find('HLTrig')!=-1 :
        func = column[column.find('_')+1:]
    if column.find('nth')!=-1:
        func = 'nth'

    # Set up x labels and plotting variables
    x_label_dict = {'pT_b2j2_high_j1': '1st jet p_{T} [GeV]', 'pT_b2j2_high_j2': '2nd jet p_{T} [GeV]', 
                    'pT_b2j2_high_j3': '3rd jet p_{T} [GeV]', 'pT_b2j2_high_j4': '4th jet p_{T} [GeV]',
                    'pT_b2j2_low_j1' : '1st jet p_{T} [GeV]', 'pT_b2j2_low_j2' : '2nd jet p_{T} [GeV]', 
                    'pT_b2j2_low_j3' : '3rd jet p_{T} [GeV]', 'pT_b2j2_low_j4' : '4th jet p_{T} [GeV]',
                    'pT_b2j1_high_j1': '1st jet p_{T} [GeV]', 'pT_b2j1_low_j2' : '2nd jet p_{T} [GeV]', 
                    'pT_b2j1_low_j3' : '3rd jet p_{T} [GeV]', 
                    'pT_b2j0_base_j1': '1st jet p_{T} [GeV]', 'pT_b2j0_base_j2': '2nd jet p_{T} [GeV]', 
                    'pT_b1j0_base_j1': '1st jet p_{T} [GeV]', 
                    'pT_j1' : '1st jet p_{T} [GeV]', 'pT_j2': '2nd jet p_{T} [GeV]', 
                    'pT_j3' : '3rd jet p_{T} [GeV]', 'pT_j4': '4th jet p_{T} [GeV]', 
                    'eta_j1': '1st jet #eta', 'eta_j2': '2nd jet #eta', 
                    'eta_j3': '3rd jet #eta', 'eta_j4': '4th jet #eta',
                    'phi_j1': '1st jet #phi', 'phi_j2': '2nd jet #phi', 
                    'phi_j3': '3rd jet #phi', 'phi_j4': '4th jet #phi',
                    'HT_Off': 'HT [GeV]', 'njets': 'Number of offline jets',
                    'm_hh': 'm_{HH} [GeV]', 'm_hh_cor': 'correct m_{HH} [GeV]',
                    'met': 'Missing E_{T} [GeV]', 'mtw': 'Transverse mass of W boson candidate [GeV]',
                    'met_mtw': 'Missing E_{T} + m_{T,W} [GeV]', 'nth': 'Matched offline Nth'
                   }
    return x_label_dict[func]

def xlims(column, arr):
    xlim = [0.0, 1000.0]
    if column.find('pT')!=-1:
        xlim[0] = 0.0
        if column.find('_j1')!=-1:
            xlim[1] = 550.0
        if column.find('_j2')!=-1:
            xlim[1] = 350.0
        if column.find('_j3')!=-1:
            xlim[1] = 250.0
        if column.find('_j4')!=-1:
            xlim[1] = 150.0
    elif column.find('HT')!=-1:
        xlim[0] = 0.0
        xlim[1] = 1200.0
    elif column.find('eta')!=-1:
        xlim[0] = -2.8
        xlim[1] = 2.8
    elif column.find('phi')!=-1:
        xlim[0] = -3.3
        xlim[1] = 3.3
    elif column.find('njets')!=-1:
        xlim[0] = -0.5
        xlim[1] = 9.5
    elif column.find('m_hh')!=-1:
        xlim[0] = 0.0
        xlim[1] = 1300.0
    elif column.find('nth')!=-1:
        xlim[0] = -0.5
        xlim[1] = 9.5
    else:
        xlim = np.percentile(np.hstack(arr[column]), [0.01, 99.99])
        width = xlim[1]-xlim[0]
        xlim[0] -= width*0.1
        xlim[1] += width*0.1
 
    return xlim

def xlimsTrigSF(year, trig, nth):
    xlim_dict = {
        ### 2015 ###                     ### 2016 ###
        "2015_b2j2_1st" : [35.0, 105.0, 14], "2016_b2j2_1st" : [35.0, 105.0, 14], 
        "2015_b2j2_2nd" : [35.0, 105.0, 14], "2016_b2j2_2nd" : [35.0, 105.0, 14],
        "2015_b2j2_3rd" : [35.0, 105.0, 14], "2016_b2j2_3rd" : [35.0, 105.0, 14],
        "2015_b2j2_4th" : [35.0, 105.0, 14], "2016_b2j2_4th" : [35.0, 105.0, 14],
        "2015_b2j1_1st" : [35.0, 235.0, 20], "2016_b2j1_1st" : [35.0, 235.0, 20],
        "2015_b2j1_2nd" : [35.0, 135.0, 20], "2016_b2j1_2nd" : [35.0, 135.0, 20],
        "2015_b2j1_3rd" : [35.0, 135.0, 20], "2016_b2j1_3rd" : [35.0, 135.0, 20],
        "2015_b2j0_1st" : [35.0, 999.0, 10], "2016_b2j0_1st" : [35.0, 999.0, 10], # <- fake
        "2015_b2j0_2nd" : [35.0, 999.0, 10], "2016_b2j0_2nd" : [35.0, 999.0, 10], # <- fake
        "2015_b2j0_HT"  : [35.0, 999.0, 10], "2016_b2j0_HT"  : [35.0, 999.0, 10], # <- fake
        "2015_b1j0_1st" : [35.0, 435.0, 40], "2016_b1j0_1st" : [35.0, 435.0, 40],
        ### 2017 ###                     ### 2018 ###
        "2017_b2j2_1st" : [35.0, 105.0, 14], "2018_b2j2_1st" : [35.0, 105.0, 14],
        "2017_b2j2_2nd" : [35.0, 105.0, 14], "2018_b2j2_2nd" : [35.0, 105.0, 14],
        "2017_b2j2_3rd" : [35.0, 105.0, 14], "2018_b2j2_3rd" : [35.0, 105.0, 14],
        "2017_b2j2_4th" : [35.0, 105.0, 14], "2018_b2j2_4th" : [35.0, 105.0, 14],
        "2017_b2j1_1st" : [35.0, 285.0, 24], "2018_b2j1_1st" : [35.0, 285.0, 24],
        "2017_b2j1_2nd" : [35.0, 135.0, 20], "2018_b2j1_2nd" : [35.0, 135.0, 20],
        "2017_b2j1_3rd" : [35.0, 135.0, 20], "2018_b2j1_3rd" : [35.0, 135.0, 20],
        "2017_b2j0_1st" : [35.0, 135.0, 20], "2018_b2j0_1st" : [35.0, 135.0, 20],
        "2017_b2j0_2nd" : [35.0, 135.0, 20], "2018_b2j0_2nd" : [35.0, 135.0, 20],
        "2017_b2j0_2nd" : [35.0, 135.0, 20], "2018_b2j0_2nd" : [35.0, 135.0, 20],
        "2017_b2j0_HT"  : [0.0, 1200.0, 30], "2018_b2j0_HT"  : [0.0, 1200.0, 30],
        "2017_b1j0_1st" : [35.0, 435.0, 40], "2018_b1j0_1st" : [35.0, 435.0, 40]
    }
    return xlim_dict[year+'_'+trig+'_'+nth];

def rebinTrigSF_L1(year, trig, nth):
    rebin_dict = {
        ### 2015 ###                                               ### 2016 ###
        "2015_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1],                 "2016_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1], 
        "2015_b2j2_2nd" : [3,2,1,1,1,1,1,1,1,1,1],                 "2016_b2j2_2nd" : [3,2,1,1,1,1,1,1,1,1,1],
        "2015_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],             "2016_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],           "2016_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j1_1st" : [5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],       "2016_b2j1_1st" : [5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j1_2nd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2016_b2j1_2nd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2016_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j0_1st" : [1,1,1,1,1,1,1,1,1,1],                   "2016_b2j0_1st" : [1,1,1,1,1,1,1,1,1,1], # <- fake
        "2015_b2j0_2nd" : [1,1,1,1,1,1,1,1,1,1],                   "2016_b2j0_2nd" : [1,1,1,1,1,1,1,1,1,1], # <- fake
        "2015_b2j0_HT"  : [1,1,1,1,1,1,1,1,1,1],                   "2016_b2j0_HT"  : [1,1,1,1,1,1,1,1,1,1], # <- fake
        "2015_b1j0_1st" : [8,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 
        "2016_b1j0_1st" : [8,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        ### 2017 ###                                               ### 2018 ###
        "2017_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1],                 "2018_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1],
        "2017_b2j2_2nd" : [2,2,1,1,1,1,1,1,1,1,1,1],               "2018_b2j2_2nd" : [2,2,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],             "2018_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],           "2018_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j1_1st" : [7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],   "2018_b2j1_1st" : [7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j1_2nd" : [3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],   "2018_b2j1_2nd" : [3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2018_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j0_1st" : [3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],     "2018_b2j0_1st" : [3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j0_2nd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2018_b2j0_2nd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j0_HT"  : [5,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,4],   
        "2018_b2j0_HT"  : [5,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,4],
        "2017_b1j0_1st" : [8,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 
        "2018_b1j0_1st" : [8,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    }
    return rebin_dict[year+'_'+trig+'_'+nth];

def rebinTrigSF_HLT(year, trig, nth):
    rebin_dict = {
        ### 2015 ###                                                 ### 2016 ###
        "2015_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1],                 "2016_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1], 
        "2015_b2j2_2nd" : [3,2,1,1,1,1,1,1,1,1,1],                 "2016_b2j2_2nd" : [3,2,1,1,1,1,1,1,1,1,1],
        "2015_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],             "2016_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],           "2016_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j1_1st" : [3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],     "2016_b2j1_1st" : [3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j1_2nd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2016_b2j1_2nd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2016_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2015_b2j0_1st" : [1,1,1,1,1,1,1,1,1,1],                   "2016_b2j0_1st" : [1,1,1,1,1,1,1,1,1,1], # <- fake
        "2015_b2j0_2nd" : [1,1,1,1,1,1,1,1,1,1],                   "2016_b2j0_2nd" : [1,1,1,1,1,1,1,1,1,1], # <- fake
        "2015_b2j0_HT"  : [1,1,1,1,1,1,1,1,1,1],                   "2016_b2j0_HT"  : [1,1,1,1,1,1,1,1,1,1], # <- fake
        "2015_b1j0_1st" : [15,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 
        "2016_b1j0_1st" : [15,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 
        ### 2017 ###                                               ### 2018 ###
        "2017_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1],                 "2018_b2j2_1st" : [3,2,1,1,1,1,1,1,1,1,1],
        "2017_b2j2_2nd" : [3,2,1,1,1,1,1,1,1,1,1],                 "2018_b2j2_2nd" : [3,2,1,1,1,1,1,1,1,1,1],
        "2017_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],             "2018_b2j2_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],           "2018_b2j2_4th" : [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j1_1st" : [7,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],     "2018_b2j1_1st" : [7,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j1_2nd" : [3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],   "2018_b2j1_2nd" : [3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], "2018_b2j1_3rd" : [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j0_1st" : [4,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1],       "2018_b2j0_1st" : [4,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j0_2nd" : [3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],   "2018_b2j0_2nd" : [3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        "2017_b2j0_HT"  : [5,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,4],   
        "2018_b2j0_HT"  : [5,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,4],   
        "2017_b1j0_1st" : [20,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 
        "2018_b1j0_1st" : [20,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    }
    return rebin_dict[year+'_'+trig+'_'+nth]


################### TRIGGER NAMES  ###################

def multibTrigName(year, trig):
    trig_dict = {"2015_b2j2" : "HLT_2j35_btight_2j35_L13J25.0ETA23",
                 "2015_b2j1" : "HLT_j100_2j55_bmedium",
                 "2015_b2j0" : "Unknown",
                 "2015_b1j0" : "HLT_j225_bloose",
                 "2016_b2j2" : "HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25",
                 "2016_b2j1" : "HLT_j100_2j55_bmv2c2060_split", 
                 "2016_b2j0" : "Unknown",
                 "2016_b1j0" : "HLT_j225_bmv2c2060_split",
                 "2017_b2j2" : "HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25",
                 "2017_b2j1" : "HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30",
                 "2017_b2j0" : "HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21",
                 "2017_b1j0" : "HLT_j225_gsc300_bmv2c1070_split", 
                 "2018_b2j2" : "HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25",
                 "2018_b2j1" : "HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30",
                 "2018_b2j0" : "HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21",
                 "2018_b1j0" : "HLT_j225_gsc300_bmv2c1070_split"
    }
    return trig_dict[year+'_'+trig]

def HLTrigName(year, trig, nth):
    trig_dict = {"2015_b2j2_1st" : "HLT_j35_L1_J25.0ETA23",
                 "2015_b2j2_2nd" : "HLT_j35_L1_J25.0ETA23",
                 "2015_b2j2_3rd" : "HLT_j35_L1_J25.0ETA23",
                 "2015_b2j2_4th" : "HLT_j35_L1_noL1",
                 "2015_b2j1_1st" : "HLT_j100_L1_J75",
                 "2015_b2j1_2nd" : "HLT_j55_L1_J20",
                 "2015_b2j1_3rd" : "HLT_j55_L1_J20",
                 "2015_b2j0_1st" : "Unknown",
                 "2015_b2j0_2nd" : "Unknown",
                 "2015_b2j0_HT"  : "Unknown",
                 "2015_b1j0_1st" : "HLT_j225_L1_J100",
                 # 2016
                 "2016_b2j2_1st" : "HLT_j35_boffperf_split_L1_J15.0ETA25",
                 "2016_b2j2_2nd" : "HLT_j35_boffperf_split_L1_J15.0ETA25",
                 "2016_b2j2_3rd" : "HLT_j35_L1_J15.0ETA25",
                 "2016_b2j2_4th" : "HLT_j35_L1_J15.0ETA25",
                 "2016_b2j1_1st" : "HLT_j100_L1_J75",
                 "2016_b2j1_2nd" : "HLT_j55_boffperf_split_L1_J20",
                 "2016_b2j1_3rd" : "HLT_j55_boffperf_split_L1_J20",
                 "2016_b2j0_1st" : "Unknown",
                 "2016_b2j0_2nd" : "Unknown",
                 "2016_b2j0_HT"  : "Unknown",
                 "2016_b1j0_1st" : "HLT_j225_boffperf_split_L1_J100",
                 # 2017
                 "2017_b2j2_1st" : "HLT_j15_gsc35_boffperf_split_L1_J15.0ETA25",
                 "2017_b2j2_2nd" : "HLT_j15_gsc35_boffperf_split_L1_J15.0ETA25",
                 "2017_b2j2_3rd" : "HLT_j15_gsc35_boffperf_split_L1_J15.0ETA25",
                 "2017_b2j2_4th" : "HLT_j15_gsc35_boffperf_split_L1_J15.0ETA25",
                 "2017_b2j1_1st" : "HLT_j110_gsc150_boffperf_split_L1_J85",
                 "2017_b2j1_2nd" : "HLT_j35_gsc55_boffperf_split_L1_J30",
                 "2017_b2j1_3rd" : "HLT_j35_gsc55_boffperf_split_L1_J30",
                 "2017_b2j0_1st" : "HLT_j35_gsc55_boffperf_split_L1_noL1", 
                 "2017_b2j0_2nd" : "HLT_j35_gsc55_boffperf_split_L1_noL1", 
                 "2017_b2j0_HT"  : "HLT_ht300_L1HT190-J15s5.ETA21",
                 "2017_b1j0_1st" : "HLT_j225_gsc300_boffperf_split_L1_J100",
                 # 2018
                 "2018_b2j2_1st" : "HLT_j35_boffperf_split_L1_J15.0ETA25",
                 "2018_b2j2_2nd" : "HLT_j35_boffperf_split_L1_J15.0ETA25",
                 "2018_b2j2_3rd" : "HLT_j35_L1_J15.0ETA25",
                 "2018_b2j2_4th" : "HLT_j35_L1_J15.0ETA25",
                 "2018_b2j1_1st" : "HLT_j110_gsc150_boffperf_split_L1_J85",
                 "2018_b2j1_2nd" : "HLT_j45_gsc55_boffperf_split_L1_J30",
                 "2018_b2j1_3rd" : "HLT_j45_gsc55_boffperf_split_L1_J30",
                 "2018_b2j0_1st" : "HLT_j45_gsc55_boffperf_split_L1_noL1", 
                 "2018_b2j0_2nd" : "HLT_j45_gsc55_boffperf_split_L1_noL1", 
                 "2018_b2j0_HT"  : "HLT_ht300_L1HT190-J15s5.ETA21",
                 "2018_b1j0_1st" : "HLT_j225_gsc300_boffperf_split_L1_J100"
    }
    return trig_dict[year+'_'+trig+'_'+nth];


def L1TrigName(year, trig, nth):
    trig_dict = {# 2015                             # 2016
                 "2015_b2j2_1st" : "L1_J25.0ETA23", "2016_b2j2_1st" : "L1_J15.0ETA25",
                 "2015_b2j2_2nd" : "L1_J25.0ETA23", "2016_b2j2_2nd" : "L1_J15.0ETA25",
                 "2015_b2j2_3rd" : "L1_J25.0ETA23", "2016_b2j2_3rd" : "L1_J15.0ETA25",
                 "2015_b2j2_4th" : "L1_noL1",       "2016_b2j2_4th" : "L1_J15.0ETA25",
                 "2015_b2j1_1st" : "L1_J75",        "2016_b2j1_1st" : "L1_J75",
                 "2015_b2j1_2nd" : "L1_J20",        "2016_b2j1_2nd" : "L1_J20",
                 "2015_b2j1_3rd" : "L1_J20",        "2016_b2j1_3rd" : "L1_J20",
                 "2015_b2j0_1st" : "Unknown",       "2016_b2j0_1st" : "Unknown",
                 "2015_b2j0_2nd" : "Unknown",       "2016_b2j0_2nd" : "Unknown",
                 "2015_b2j0_HT"  : "Unknown",       "2016_b2j0_HT"  : "Unknown",
                 "2015_b1j0_1st" : "L1_J100",       "2016_b1j0_1st" : "L1_J100",
                 # 2017                             # 2018
                 "2017_b2j2_1st" : "L1_J15.0ETA25", "2018_b2j2_1st" : "L1_J15.0ETA25",
                 "2017_b2j2_2nd" : "L1_J15.0ETA25", "2018_b2j2_2nd" : "L1_J15.0ETA25",
                 "2017_b2j2_3rd" : "L1_J15.0ETA25", "2018_b2j2_3rd" : "L1_J15.0ETA25",
                 "2017_b2j2_4th" : "L1_J15.0ETA25", "2018_b2j2_4th" : "L1_J15.0ETA25",
                 "2017_b2j1_1st" : "L1_J85",        "2018_b2j1_1st" : "L1_J85",
                 "2017_b2j1_2nd" : "L1_J30",        "2018_b2j1_2nd" : "L1_J30",
                 "2017_b2j1_3rd" : "L1_J30",        "2018_b2j1_3rd" : "L1_J30",
                 "2017_b2j0_1st" : "L1_noL1",       "2018_b2j0_1st" : "L1_noL1",
                 "2017_b2j0_2nd" : "L1_noL1",       "2018_b2j0_2nd" : "L1_noL1",
                 "2017_b2j0_HT"  : "L1_HT190_J15s5.ETA21", "2018_b2j0_HT"  : "L1_HT190_J15s5.ETA21", 
                 "2017_b1j0_1st" : "L1_J100",       "2018_b1j0_1st" : "L1_J100"
    }
    return trig_dict[year+'_'+trig+'_'+nth]

