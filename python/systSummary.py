#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from numpy.lib.recfunctions import append_fields
from trigUtils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")

parser.add_option("--trig", dest="trig", default="b2j1",
                  help="Trigger, where start to take a product of the SFs (default: b2j1)")
parser.add_option("--year", dest="year", default="2016",
                  help="Year (default: 2016)")
parser.add_option("--doL1TurnOn", action="store_true",
                  dest="doL1TurnOn", default=False,
                  help="Enable to show L1 turn on, (default: HLT turn on)")
parser.add_option("--doL1andHLTTurnOn", action="store_true",
                  dest="doL1andHLTTurnOn", default=False,
                  help="Enable to show L1 and HLT combination turn on, (default: HLT turn on)")
parser.add_option("--label", dest="label", default="",
                  help="Year (default: 2017)")

(options, args) = parser.parse_args()

import root_numpy
import ROOT

# configure local options
trig = options.trig
doL1TurnOn = options.doL1TurnOn
doL1andHLTTurnOn = options.doL1andHLTTurnOn
label = options.label
if doL1andHLTTurnOn:
    label += "_L1andHLT"
elif doL1TurnOn:
    label += "_L1"
else:
    label += "_HLT"

print("Start to make systematic summary plots")
if doL1andHLTTurnOn:
    print("Setup to L1 and HLT combination systematic")
elif doL1TurnOn:
    print("Setup to L1 systematic")
else:
    print("Setup to HLT systematic")

year_in = options.year
year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2016")
    year='2016'
    yr_short='16'

# configure input files
output_dir = options.output_dir
if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

print("Initialization done!")
print("Define trigger : ", trig, " in ", year)

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)
from array import array

label_lumi = mc_lumi(year)
nth = nths(trig)
columns_to_plot = []
for i in range(len(nth)):
    if nth[i].find('HT') != -1:
        columns_to_plot += ['HT']
    else:
        columns_to_plot += [nth[i]+"Et"]

# Setup input file
syst_name = "2020-AUG08-13TeV-HH4bSystError-"+year+".root"
syst_dir  = "/afs/cern.ch/user/s/shhayash/workspace/HH4b/HH4b-JetSF-21.2.91-APR20-1/hh4b-triggersf/pyrun/"
syst_file = ROOT.TFile(syst_dir+syst_name, 'read')
syst_file = ROOT.TFile(syst_dir+syst_name, 'read')
if doL1andHLTTurnOn:
    trigName = "SystError_"+multibTrigName(year, trig)
elif doL1TurnOn:
    trigName = "L1SystError_"+multibTrigName(year, trig)                                                                                                             
else:
    trigName = "HLTSystError_"+multibTrigName(year, trig)
trigName = trigName.replace('.', '-')
tree = syst_file.Get(trigName)    

systList  = ["_diff_ttbarVersusSignal"]
systList += ["_diff_ttbarGenerator", "_diff_ttbarShower"]

############################# START MAKING TRIGGER SCALE FACTORS #############################
for n in range(len(columns_to_plot)):
   
    hist_sigSyst = tree.Get(nth[n]+'_diff_ttbarVersusSignal')
    hist_sigSyst.GetYaxis().SetTitle("Systematic unc. / Trigger Efficiency")
    hist_sigSyst.SetMarkerStyle(20)
    #hist_sigSyst.GetYaxis().SetRangeUser(-1.0, 1.0)
    hist_sigSyst.GetYaxis().SetRangeUser(-0.2, 0.2)

    hist_genSyst = tree.Get(nth[n]+'_diff_ttbarGenerator')
    hist_genSyst.SetMarkerStyle(20)

    hist_shwSyst = tree.Get(nth[n]+'_diff_ttbarShower')
    hist_shwSyst.SetMarkerStyle(20)

    # set error
    gx_syst = []
    gy_syst = []
    gex_syst = []
    gey_syst = []
    n_bins = hist_sigSyst.GetNbinsX()
    for n_bin in range(n_bins):
        hist_sigSyst.SetBinContent(n_bin+1, (hist_sigSyst.GetBinContent(n_bin+1)-1))
        hist_genSyst.SetBinContent(n_bin+1, (hist_genSyst.GetBinContent(n_bin+1)-1))
        hist_shwSyst.SetBinContent(n_bin+1, (hist_shwSyst.GetBinContent(n_bin+1)-1))

        hist_sigSyst.SetBinError(n_bin+1, 0)
        hist_genSyst.SetBinError(n_bin+1, 0)
        hist_shwSyst.SetBinError(n_bin+1, 0)

        gx_syst.append(hist_sigSyst.GetBinCenter(n_bin+1))
        gex_syst.append(hist_sigSyst.GetBinWidth(n_bin+1)/2)

        systAll = np.sqrt(abs(hist_sigSyst.GetBinContent(n_bin+1))**2+ \
                          abs(hist_genSyst.GetBinContent(n_bin+1))**2+ \
                          abs(hist_shwSyst.GetBinContent(n_bin+1))**2)
        gy_syst.append(0)
        gey_syst.append(systAll)

    graph_allSyst = TEGraph(n_bins, gx_syst, gy_syst, gex_syst, gey_syst)
    graph_allSyst.SetFillColor(ROOT.kGray+1)
    graph_allSyst.SetFillStyle(3004)

    # Canvas for jet-level trigger efficiency
    ROOT.gStyle.SetOptStat(0) 
    canvas2 = Canvas()
    canvas2.cd()
    pad2_1 = Pad(0.0,0.0,1.0,1.0)
    pad2_1.Draw()
    pad2_1.cd()
    pad2_1.SetTicks(1, 1)
   
    pad2_1.cd()
    hist_sigSyst.Draw("lp") 
    graph_allSyst.Draw("same 2") 
    hist_sigSyst.Draw("same lp") 
    hist_genSyst.Draw("same lp")
    hist_shwSyst.Draw("same lp")

    pad2_1.RedrawAxis()
    
    line2 = ROOT.TLine(hist_sigSyst.GetXaxis().GetXmin(), 0.0, hist_sigSyst.GetXaxis().GetXmax(), 0.0)
    line2.SetLineColor(1)
    line2.SetLineWidth(1)
    line2.SetLineStyle(2)
    line2.Draw()
    
    legend2 = ROOT.TLegend(0.55, 0.64, 0.85, 0.84)
    legend2.AddEntry(hist_sigSyst, 'HH4b signal diff.', 'p')
    legend2.AddEntry(hist_genSyst, 'Generator', 'p')
    legend2.AddEntry(hist_shwSyst, 'Shower', 'p')
    legend2.AddEntry(graph_allSyst, 'Total systematic', 'F')
    legend2.SetBorderSize(0)
    legend2.SetFillStyle(0)
    legend2.Draw() 
    
    lat_label2 = HLTrigName(year, trig, nth[n])
    if doL1TurnOn:
        lat_label2 = L1TrigName(year, trig, nth[n])
    ATLASLabel(0.15, 0.83, 0.041, 'Internal', ('#sqrt{s} = 13 TeV, '+year+', '+str(label_lumi)+' fb^{-1}'), lat_label2, 0.80, 0.048)
    DateLatex(1)
 
    canvas2.Draw()
    canvas2.SaveAs((output_dir+'syst'+yr_short+'_'+trig+'_'+columns_to_plot[n]+label+'.C'))
    canvas2.SaveAs((output_dir+'syst'+yr_short+'_'+trig+'_'+columns_to_plot[n]+label+'.pdf'))
    

