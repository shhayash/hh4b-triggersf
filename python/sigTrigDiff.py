#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from numpy.lib.recfunctions import append_fields
from trigUtils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("--tnh", dest="tnh_file", default="",
                  help="Input ttbar-leptonic filename")
parser.add_option("--smhh", dest="smhh_file", default="",
                  help="Input SM HH filename")
parser.add_option("--grav400", dest="grav400_file", default="",
                  help="Input RS graviton 400 GeV filename (DSID: 301489)")
parser.add_option("--grav600", dest="grav600_file", default="",
                  help="Input RS graviton 600 GeV filename (DSID: 301491)")
parser.add_option("--scal400", dest="scal400_file", default="",
                  help="Input scalar 400 GeV filename (DSID: 450253)")
parser.add_option("--scal600", dest="scal600_file", default="",
                  help="Input scalar 600 GeV filename (DSID: 450255)")
parser.add_option("--trig", dest="trig", default="b2j1",
                  help="Trigger, where start to validate trigger efficiency (default: b2j1)")
parser.add_option("--year", dest="year", default="2017",
                  help="Year (default: 2017)")
parser.add_option("--doL1TurnOn", action="store_true",
                  dest="doL1TurnOn", default=False,
                  help="Enable to show L1 turn on, (default: HLT turn on)")
parser.add_option("--doL1andHLTTurnOn", action="store_true",
                  dest="doL1andHLTTurnOn", default=False,
                  help="Enable to show L1 and HLT turn on, (default: HLT turn on)")
parser.add_option("--label", dest="label", default="",
                  help="Year (default: 2017)")
(options, args) = parser.parse_args()

import root_numpy
import ROOT
 

# configure local options
trig = options.trig
doL1TurnOn = options.doL1TurnOn
doL1andHLTTurnOn = options.doL1andHLTTurnOn
label = options.label
if doL1andHLTTurnOn:
    label += "_L1andHLT"
elif doL1TurnOn:
    label += "_L1"
else:
    label += "_HLT"
label += "_NOM"

print("Start to validate jet-level trigger efficiency on between signal and ttbar-leptonic samples")
if doL1andHLTTurnOn:
    print("Setup to L1 and HLT combination turn on curve")
elif doL1TurnOn:
    print("Setup to L1 turn on curve")
else:
    print("Setup to HLT turn on curve")

year_in = options.year
year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2017")
    year='2017'
    yr_short='17'

# configure input files
input_dir = options.input_dir
output_dir = options.output_dir
if input_dir:
    if input_dir[-1] != '/': input_dir+='/'
if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

tnh_file = options.tnh_file
if tnh_file:
    if tnh_file[0] == '/':
        input_dir = ''
    tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! ttbar-leptonic file not a root file")

smhh_file = options.smhh_file
if smhh_file:
    if smhh_file[0] == '/':
        input_dir = ''
    smhh_file = input_dir+smhh_file
    if smhh_file[-4:] != 'root':
        print("Warning! SM HH file not a root file")

grav400_file = options.grav400_file
if grav400_file:
    if grav400_file[0] == '/':
        input_dir = ''
    grav400_file = input_dir+grav400_file
    if grav400_file[-4:] != 'root':
        print("Warning! RS graviton 400 GeV file not a root file")

grav600_file = options.grav600_file
if grav600_file:
    if grav600_file[0] == '/':
        input_dir = ''
    grav600_file = input_dir+grav600_file
    if grav600_file[-4:] != 'root':
        print("Warning! RS graviton 600 GeV file not a root file")

scal400_file = options.scal400_file
if scal400_file:
    if scal400_file[0] == '/':
        input_dir = ''
    scal400_file = input_dir+scal400_file
    if scal400_file[-4:] != 'root':
        print("Warning! scalar 400 GeV file not a root file")

scal600_file = options.scal600_file
if scal600_file:
    if scal600_file[0] == '/':
        input_dir = ''
    scal600_file = input_dir+scal600_file
    if scal600_file[-4:] != 'root':
        print("Warning! scalar 600 GeV file not a root file")

print("Initialization done!")
print("Define trigger : ", trig, " in ", year)

# Set up columns
columns_mc = ['pass_mutrig', 'pass_eltrig', 'run_number', 'mc_sf']

# Set up each single jet trigger of Nth jets
nthTrig = nthTrigs(trig, year)
prefixJetName = prefixJetNames(doL1TurnOn) 
for i in range(len(nthTrig)):
    if doL1TurnOn:
        if nthTrig[i].find('HT') != -1: # -> Go to HT cut
            columns_mc += ['HT_Off', 
                           prefixJetName+'isL1Emulated_'+nthTrig[i], prefixJetName+'hasL1Jet_HT']
        else: # -> Go to Et cut
            columns_mc += [prefixJetName+'pT_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasL1Jet_j'+str(i+1)]
    else:
        if nthTrig[i].find('HT') != -1: # -> Go to HT cut
            columns_mc += ['HT_Off', 
                           prefixJetName+'isL1Emulated_'+nthTrig[i],  prefixJetName+'hasL1Jet_'+nthTrig[i],
                           prefixJetName+'isHLTEmulated_'+nthTrig[i], prefixJetName+'hasHLTJet_'+nthTrig[i]]
        else: # -> Go to Et cut
            columns_mc += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1),  prefixJetName+'hasL1Jet_'+nthTrig[i]+'_j'+str(i+1),
                           prefixJetName+'isHLTEmulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasHLTJet_'+nthTrig[i]+'_j'+str(i+1)]

# Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
arrs = {}
print("Start preparing ttbar-leptonic file")
arrs['tnh']  = make_arrs(tnh_file,  columns_mc, yr_short, trig, False, doL1TurnOn, doL1andHLTTurnOn, sigTrig=True)
print("Start preparing SMHH file")
arrs['smhh'] = make_arrs(smhh_file, columns_mc, yr_short, trig, False, doL1TurnOn, doL1andHLTTurnOn, sigTrig=True)
print("Start preparing Graviton 400 GeV file")
arrs['grav400'] = make_arrs(grav400_file, columns_mc, yr_short, trig, False, doL1TurnOn, doL1andHLTTurnOn, sigTrig=True)
print("Start preparing Graviton 600 GeV file")
arrs['grav600'] = make_arrs(grav600_file, columns_mc, yr_short, trig, False, doL1TurnOn, doL1andHLTTurnOn, sigTrig=True)
print("Start preparing Scalar 400 GeV file")
arrs['scal400'] = make_arrs(scal400_file, columns_mc, yr_short, trig, False, doL1TurnOn, doL1andHLTTurnOn, sigTrig=True)
print("Start preparing Scalar 600 GeV file")
arrs['scal600'] = make_arrs(scal600_file, columns_mc, yr_short, trig, False, doL1TurnOn, doL1andHLTTurnOn, sigTrig=True)
print("Filling done!")

print(">>> Go to produce trigger efficiency plots!")

lumi15 = 3.2
lumi16 = 24.6
lumi17 = 43.65
lumi18 = 58.45 
label_lumi = ('2015' in year)*lumi15+('2016' in year)*lumi16+('2017' in year)*lumi17+('2018' in year)*lumi18
internal = "Simulation Internal" 

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)
from array import array

columns_to_plot = []
for i in range(len(nthTrig)):
    if nthTrig[i].find('HT') != -1: # -> Go to HT cut 
        columns_to_plot += ['HT_Off']
    else:
        if doL1TurnOn:
            columns_to_plot += [prefixJetName+'pT_j'+str(i+1)]
        else:
            columns_to_plot += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1)]

nth = nths(trig)
# setup output file
foutName = "2020-AUG08-13TeV-HH4bSystError-"+year+".root"
fout = ROOT.TFile(foutName,'update')
if doL1andHLTTurnOn: 
    trigName = "SystError_"+multibTrigName(year, trig)
elif doL1TurnOn:
    trigName = "L1SystError_"+multibTrigName(year, trig)
else:
    trigName = "HLTSystError_"+multibTrigName(year, trig)
trigName = trigName.replace('.', '-')

have_tree = fout.GetListOfKeys().Contains(trigName)
if (have_tree):
    tree = fout.Get(trigName)
    for nt in nth:
        have_syst = tree.GetListOfKeys().Contains(nt+"_diff_ttbarVersusSignal")
        if (have_syst):
            tree.FindKey(nt+"_diff_ttbarVersusSignal").Delete()
else:
    tree = fout.mkdir(trigName, (year+" "+trig+" trigger"))
fout.Write()
fout.Close() 


############################# START COMPARING TRIGGER EFFICIENCY BETWEEN TTBAR AND SIGNALS #############################
for column in columns_to_plot:

    if column.find('HT') != -1:
       n = 2
    else:
       n = int(column[-1])-1
    passNth = 'pass_'+nth[n]

    n_bins = 40
    xlim = xlims(column, arrs['tnh'])  
    if (column.find('pT')!=-1 or column.find('HT')!=-1):
        xlim = xlimsTrigSF(year, trig, nth[n])
        n_bins = xlim[2]
    
    ### 1. Trigger Efficiency ###
    # ttbar-leptonic reference
    hist_tnhRef = Hist(n_bins, xlim[0], xlim[1], title='MC Reference', legendstyle='F')
    hist_tnhRef.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Ref']][column], weights=arrs['tnh'][arrs['tnh'][passNth+'Ref']]['mc_sf'])
    hist_tnhRef.SetBinContent(n_bins, hist_tnhRef.GetBinContent(n_bins)+hist_tnhRef.GetBinContent(n_bins+1))

    err_tnhRef = Hist(n_bins, xlim[0], xlim[1], title='MC Reference (No weight)', legendstyle='F')
    err_tnhRef.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Ref']][column])
    err_tnhRef.SetBinContent(n_bins, err_tnhRef.GetBinContent(n_bins)+err_tnhRef.GetBinContent(n_bins+1))

    # SM HH reference
    hist_smhhRef = Hist(n_bins, xlim[0], xlim[1], title='MC Reference', legendstyle='F')
    hist_smhhRef.fill_array(arrs['smhh'][arrs['smhh'][passNth+'Ref']][column], weights=arrs['smhh'][arrs['smhh'][passNth+'Ref']]['mc_sf'])
    hist_smhhRef.SetBinContent(n_bins, hist_smhhRef.GetBinContent(n_bins)+hist_smhhRef.GetBinContent(n_bins+1))

    err_smhhRef = Hist(n_bins, xlim[0], xlim[1], title='MC Reference (No weight)', legendstyle='F')
    err_smhhRef.fill_array(arrs['smhh'][arrs['smhh'][passNth+'Ref']][column])
    err_smhhRef.SetBinContent(n_bins, err_smhhRef.GetBinContent(n_bins)+err_smhhRef.GetBinContent(n_bins+1))

    # RS graviton 400 GeV reference
    hist_grav400Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference', legendstyle='F')
    hist_grav400Ref.fill_array(arrs['grav400'][arrs['grav400'][passNth+'Ref']][column], \
                               weights=arrs['grav400'][arrs['grav400'][passNth+'Ref']]['mc_sf'])
    hist_grav400Ref.SetBinContent(n_bins, hist_grav400Ref.GetBinContent(n_bins)+hist_grav400Ref.GetBinContent(n_bins+1))

    err_grav400Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference (No weight)', legendstyle='F')
    err_grav400Ref.fill_array(arrs['grav400'][arrs['grav400'][passNth+'Ref']][column])
    err_grav400Ref.SetBinContent(n_bins, err_grav400Ref.GetBinContent(n_bins)+err_grav400Ref.GetBinContent(n_bins+1))

    # RS graviton 600 GeV reference
    hist_grav600Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference', legendstyle='F')
    hist_grav600Ref.fill_array(arrs['grav600'][arrs['grav600'][passNth+'Ref']][column], \
                               weights=arrs['grav600'][arrs['grav600'][passNth+'Ref']]['mc_sf'])
    hist_grav600Ref.SetBinContent(n_bins, hist_grav600Ref.GetBinContent(n_bins)+hist_grav600Ref.GetBinContent(n_bins+1))

    err_grav600Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference (No weight)', legendstyle='F')
    err_grav600Ref.fill_array(arrs['grav600'][arrs['grav600'][passNth+'Ref']][column])
    err_grav600Ref.SetBinContent(n_bins, err_grav600Ref.GetBinContent(n_bins)+err_grav600Ref.GetBinContent(n_bins+1))

    # Scalar 400 GeV reference
    hist_scal400Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference', legendstyle='F')
    hist_scal400Ref.fill_array(arrs['scal400'][arrs['scal400'][passNth+'Ref']][column], \
                               weights=arrs['scal400'][arrs['scal400'][passNth+'Ref']]['mc_sf'])
    hist_scal400Ref.SetBinContent(n_bins, hist_scal400Ref.GetBinContent(n_bins)+hist_scal400Ref.GetBinContent(n_bins+1))

    err_scal400Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference (No weight)', legendstyle='F')
    err_scal400Ref.fill_array(arrs['scal400'][arrs['scal400'][passNth+'Ref']][column])
    err_scal400Ref.SetBinContent(n_bins, err_scal400Ref.GetBinContent(n_bins)+err_scal400Ref.GetBinContent(n_bins+1))

    # Scalar 600 GeV reference
    hist_scal600Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference', legendstyle='F')
    hist_scal600Ref.fill_array(arrs['scal600'][arrs['scal600'][passNth+'Ref']][column], \
                               weights=arrs['scal600'][arrs['scal600'][passNth+'Ref']]['mc_sf'])
    hist_scal600Ref.SetBinContent(n_bins, hist_scal600Ref.GetBinContent(n_bins)+hist_scal600Ref.GetBinContent(n_bins+1))

    err_scal600Ref = Hist(n_bins, xlim[0], xlim[1], title='MC Reference (No weight)', legendstyle='F')
    err_scal600Ref.fill_array(arrs['scal600'][arrs['scal600'][passNth+'Ref']][column])
    err_scal600Ref.SetBinContent(n_bins, err_scal600Ref.GetBinContent(n_bins)+err_scal600Ref.GetBinContent(n_bins+1))

    # ttbar-leptonic trigger (Emulated)
    hist_tnhTri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate', legendstyle='PE')
    hist_tnhTri.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Tri']][column], weights=arrs['tnh'][arrs['tnh'][passNth+'Tri']]['mc_sf'])
    hist_tnhTri.SetBinContent(n_bins, hist_tnhTri.GetBinContent(n_bins)+hist_tnhTri.GetBinContent(n_bins+1))

    err_tnhTri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate (No weight)', legendstyle='PE')
    err_tnhTri.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Tri']][column])
    err_tnhTri.SetBinContent(n_bins, err_tnhTri.GetBinContent(n_bins)+err_tnhTri.GetBinContent(n_bins+1))

    # SM HH trigger (Emulated)
    hist_smhhTri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate', legendstyle='PE')
    hist_smhhTri.fill_array(arrs['smhh'][arrs['smhh'][passNth+'Tri']][column], weights=arrs['smhh'][arrs['smhh'][passNth+'Tri']]['mc_sf'])
    hist_smhhTri.SetBinContent(n_bins, hist_smhhTri.GetBinContent(n_bins)+hist_smhhTri.GetBinContent(n_bins+1))

    err_smhhTri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate (No weight)', legendstyle='PE')
    err_smhhTri.fill_array(arrs['smhh'][arrs['smhh'][passNth+'Tri']][column])
    err_smhhTri.SetBinContent(n_bins, err_smhhTri.GetBinContent(n_bins)+err_smhhTri.GetBinContent(n_bins+1))

    # RS graviton 400 GeV trigger (Emulated)
    hist_grav400Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate', legendstyle='PE')
    hist_grav400Tri.fill_array(arrs['grav400'][arrs['grav400'][passNth+'Tri']][column], \
                               weights=arrs['grav400'][arrs['grav400'][passNth+'Tri']]['mc_sf'])
    hist_grav400Tri.SetBinContent(n_bins, hist_grav400Tri.GetBinContent(n_bins)+hist_grav400Tri.GetBinContent(n_bins+1))

    err_grav400Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate (No weight)', legendstyle='PE')
    err_grav400Tri.fill_array(arrs['grav400'][arrs['grav400'][passNth+'Tri']][column])
    err_grav400Tri.SetBinContent(n_bins, err_grav400Tri.GetBinContent(n_bins)+err_grav400Tri.GetBinContent(n_bins+1))

    # RS graviton 600 GeV trigger (Emulated)
    hist_grav600Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate', legendstyle='PE')
    hist_grav600Tri.fill_array(arrs['grav600'][arrs['grav600'][passNth+'Tri']][column], \
                               weights=arrs['grav600'][arrs['grav600'][passNth+'Tri']]['mc_sf'])
    hist_grav600Tri.SetBinContent(n_bins, hist_grav600Tri.GetBinContent(n_bins)+hist_grav600Tri.GetBinContent(n_bins+1))

    err_grav600Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate (No weight)', legendstyle='PE')
    err_grav600Tri.fill_array(arrs['grav600'][arrs['grav600'][passNth+'Tri']][column])
    err_grav600Tri.SetBinContent(n_bins, err_grav600Tri.GetBinContent(n_bins)+err_grav600Tri.GetBinContent(n_bins+1))

    # Scalar 400 GeV trigger (Emulated)
    hist_scal400Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate', legendstyle='PE')
    hist_scal400Tri.fill_array(arrs['scal400'][arrs['scal400'][passNth+'Tri']][column], \
                               weights=arrs['scal400'][arrs['scal400'][passNth+'Tri']]['mc_sf'])
    hist_scal400Tri.SetBinContent(n_bins, hist_scal400Tri.GetBinContent(n_bins)+hist_scal400Tri.GetBinContent(n_bins+1))

    err_scal400Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate (No weight)', legendstyle='PE')
    err_scal400Tri.fill_array(arrs['scal400'][arrs['scal400'][passNth+'Tri']][column])
    err_scal400Tri.SetBinContent(n_bins, err_scal400Tri.GetBinContent(n_bins)+err_scal400Tri.GetBinContent(n_bins+1))

    # Scalar 600 GeV trigger (Emulated)
    hist_scal600Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate', legendstyle='PE')
    hist_scal600Tri.fill_array(arrs['scal600'][arrs['scal600'][passNth+'Tri']][column], \
                               weights=arrs['scal600'][arrs['scal600'][passNth+'Tri']]['mc_sf'])
    hist_scal600Tri.SetBinContent(n_bins, hist_scal600Tri.GetBinContent(n_bins)+hist_scal600Tri.GetBinContent(n_bins+1))

    err_scal600Tri = Hist(n_bins, xlim[0], xlim[1], title='MC Emulate (No weight)', legendstyle='PE')
    err_scal600Tri.fill_array(arrs['scal600'][arrs['scal600'][passNth+'Tri']][column])
    err_scal600Tri.SetBinContent(n_bins, err_scal600Tri.GetBinContent(n_bins)+err_scal600Tri.GetBinContent(n_bins+1))

    # Rebin
    bin_width = (xlim[1]-xlim[0])/n_bins
    rebin_base = rebinTrigSF_HLT(year, trig, nth[n])
    if doL1TurnOn:
        rebin_base = rebinTrigSF_L1(year, trig, nth[n])
    rebins = []
    for i in range(len(rebin_base)+1):
        if i == 0:
            rebins.append(xlim[0])
        else:
            rebins.append(rebins[i-1]+bin_width*rebin_base[i-1])

    hist_tnhRef_rebin  = hist_tnhRef.Rebin(len(rebins)-1, 'tnhRef_rebin', array('d', rebins))
    hist_smhhRef_rebin = hist_smhhRef.Rebin(len(rebins)-1, 'smhhRef_rebin', array('d', rebins))
    hist_grav400Ref_rebin = hist_grav400Ref.Rebin(len(rebins)-1, 'grav400Ref_rebin', array('d', rebins))
    hist_grav600Ref_rebin = hist_grav600Ref.Rebin(len(rebins)-1, 'grav600Ref_rebin', array('d', rebins))
    hist_scal400Ref_rebin = hist_scal400Ref.Rebin(len(rebins)-1, 'scal400Ref_rebin', array('d', rebins))
    hist_scal600Ref_rebin = hist_scal600Ref.Rebin(len(rebins)-1, 'scal600Ref_rebin', array('d', rebins))

    hist_tnhTri_rebin  = hist_tnhTri.Rebin(len(rebins)-1, 'tnhTri_rebin', array('d', rebins))
    hist_smhhTri_rebin = hist_smhhTri.Rebin(len(rebins)-1, 'smhhTri_rebin', array('d', rebins))
    hist_grav400Tri_rebin = hist_grav400Tri.Rebin(len(rebins)-1, 'grav400Tri_rebin', array('d', rebins))
    hist_grav600Tri_rebin = hist_grav600Tri.Rebin(len(rebins)-1, 'grav600Tri_rebin', array('d', rebins))
    hist_scal400Tri_rebin = hist_scal400Tri.Rebin(len(rebins)-1, 'scal400Tri_rebin', array('d', rebins))
    hist_scal600Tri_rebin = hist_scal600Tri.Rebin(len(rebins)-1, 'scal600Tri_rebin', array('d', rebins))

    err_tnhRef_rebin  = err_tnhRef.Rebin(len(rebins)-1, 'err_tnhRef_rebin', array('d', rebins))
    err_smhhRef_rebin = err_smhhRef.Rebin(len(rebins)-1, 'err_smhhRef_rebin', array('d', rebins))
    err_grav400Ref_rebin = err_grav400Ref.Rebin(len(rebins)-1, 'err_grav400Ref_rebin', array('d', rebins))
    err_grav600Ref_rebin = err_grav600Ref.Rebin(len(rebins)-1, 'err_grav600Ref_rebin', array('d', rebins))
    err_scal400Ref_rebin = err_scal400Ref.Rebin(len(rebins)-1, 'err_scal400Ref_rebin', array('d', rebins))
    err_scal600Ref_rebin = err_scal600Ref.Rebin(len(rebins)-1, 'err_scal600Ref_rebin', array('d', rebins))

    err_tnhTri_rebin  = err_tnhTri.Rebin(len(rebins)-1, 'err_tnhTri_rebin', array('d', rebins))
    err_smhhTri_rebin = err_smhhTri.Rebin(len(rebins)-1, 'err_smhhTri_rebin', array('d', rebins))
    err_grav400Tri_rebin = err_grav400Tri.Rebin(len(rebins)-1, 'err_grav400Tri_rebin', array('d', rebins))
    err_grav600Tri_rebin = err_grav600Tri.Rebin(len(rebins)-1, 'err_grav600Tri_rebin', array('d', rebins))
    err_scal400Tri_rebin = err_scal400Tri.Rebin(len(rebins)-1, 'err_scal400Tri_rebin', array('d', rebins))
    err_scal600Tri_rebin = err_scal600Tri.Rebin(len(rebins)-1, 'err_scal600Tri_rebin', array('d', rebins))

    hist_eff_tnh = hist_tnhTri_rebin.Clone("eff_mc_tnh")
    hist_eff_tnh.Divide(hist_tnhRef_rebin)
    hist_eff_tnh.SetMarkerStyle(24)
    hist_eff_tnh.SetMarkerSize(0.7)
    hist_eff_tnh.SetMarkerColor(ROOT.kBlack)
    hist_eff_tnh.SetLineColor(ROOT.kBlack)
    hist_eff_tnh.GetYaxis().SetRangeUser(0.00, 1.45)
    hist_eff_tnh.GetXaxis().SetLabelSize(0.0)
    hist_eff_tnh.GetYaxis().SetLabelSize(0.045)
    hist_eff_tnh.GetYaxis().SetLabelOffset(0.005)
    if doL1andHLTTurnOn:
        hist_eff_tnh.GetYaxis().SetTitle("Trigger Efficiency #varepsilon")
    elif doL1TurnOn:
        hist_eff_tnh.GetYaxis().SetTitle("Trigger Efficiency #varepsilon_{L1}")
    else:
        hist_eff_tnh.GetYaxis().SetTitle("Trigger Efficiency #varepsilon_{HLT}")
    hist_eff_tnh.SetTitle('')
    hist_eff_tnh.GetYaxis().SetTitleSize(0.045)
    hist_eff_tnh.GetYaxis().SetTitleOffset(0.95)

    hist_eff_smhh = hist_smhhTri_rebin.Clone("eff_mc_smhh")
    hist_eff_smhh.Divide(hist_smhhRef_rebin)
    hist_eff_smhh.SetMarkerStyle(25)
    hist_eff_smhh.SetMarkerSize(0.7)
    hist_eff_smhh.SetMarkerColor(ROOT.kViolet)
    hist_eff_smhh.SetLineColor(ROOT.kViolet)

    hist_eff_grav400 = hist_grav400Tri_rebin.Clone("eff_mc_grav400")
    hist_eff_grav400.Divide(hist_grav400Ref_rebin)
    hist_eff_grav400.SetMarkerStyle(26)
    hist_eff_grav400.SetMarkerSize(0.7)
    hist_eff_grav400.SetMarkerColor(ROOT.kSpring-1)
    hist_eff_grav400.SetLineColor(ROOT.kSpring-1)

    hist_eff_grav600 = hist_grav600Tri_rebin.Clone("eff_mc_grav600")
    hist_eff_grav600.Divide(hist_grav600Ref_rebin)
    hist_eff_grav600.SetMarkerStyle(26)
    hist_eff_grav600.SetMarkerSize(0.7)
    hist_eff_grav600.SetMarkerColor(ROOT.kSpring-7)
    hist_eff_grav600.SetLineColor(ROOT.kSpring-7)

    hist_eff_scal400 = hist_scal400Tri_rebin.Clone("eff_mc_scal400")
    hist_eff_scal400.Divide(hist_scal400Ref_rebin)
    hist_eff_scal400.SetMarkerStyle(32)
    hist_eff_scal400.SetMarkerSize(0.7)
    hist_eff_scal400.SetMarkerColor(ROOT.kAzure+7)
    hist_eff_scal400.SetLineColor(ROOT.kAzure+7)

    hist_eff_scal600 = hist_scal600Tri_rebin.Clone("eff_mc_scal600")
    hist_eff_scal600.Divide(hist_scal600Ref_rebin)
    hist_eff_scal600.SetMarkerStyle(32)
    hist_eff_scal600.SetMarkerSize(0.7)
    hist_eff_scal600.SetMarkerColor(ROOT.kAzure-1)
    hist_eff_scal600.SetLineColor(ROOT.kAzure-1)

    asym_eff_tnh  = ROOT.TGraphAsymmErrors(hist_eff_tnh)
    asym_eff_smhh = ROOT.TGraphAsymmErrors(hist_eff_smhh)
    asym_eff_grav400 = ROOT.TGraphAsymmErrors(hist_eff_grav400)
    asym_eff_grav600 = ROOT.TGraphAsymmErrors(hist_eff_grav600)
    asym_eff_scal400 = ROOT.TGraphAsymmErrors(hist_eff_scal400)
    asym_eff_scal600 = ROOT.TGraphAsymmErrors(hist_eff_scal600)
    # Set error 
    for i in range(len(rebins)-1):
        hist_eff_tnh.SetBinError(i+1, 
            calcEffStatError(err_tnhTri_rebin.GetBinContent(i+1), err_tnhRef_rebin.GetBinContent(i+1), hist_eff_tnh.GetBinContent(i+1)));
        hist_eff_smhh.SetBinError(i+1, 
            calcEffStatError(err_smhhTri_rebin.GetBinContent(i+1), err_smhhRef_rebin.GetBinContent(i+1), hist_eff_smhh.GetBinContent(i+1)));
        hist_eff_grav400.SetBinError(i+1, 
            calcEffStatError(err_grav400Tri_rebin.GetBinContent(i+1), err_grav400Ref_rebin.GetBinContent(i+1), hist_eff_grav400.GetBinContent(i+1)));
        hist_eff_grav600.SetBinError(i+1, 
            calcEffStatError(err_grav600Tri_rebin.GetBinContent(i+1), err_grav600Ref_rebin.GetBinContent(i+1), hist_eff_grav600.GetBinContent(i+1)));
        hist_eff_scal400.SetBinError(i+1, 
            calcEffStatError(err_scal400Tri_rebin.GetBinContent(i+1), err_scal400Ref_rebin.GetBinContent(i+1), hist_eff_scal400.GetBinContent(i+1)));
        hist_eff_scal600.SetBinError(i+1, 
            calcEffStatError(err_scal600Tri_rebin.GetBinContent(i+1), err_scal600Ref_rebin.GetBinContent(i+1), hist_eff_scal600.GetBinContent(i+1)));

        asym_eff_tnh.SetPointEYhigh(i, 
                calcEffStatError(err_tnhTri_rebin.GetBinContent(i+1), err_tnhRef_rebin.GetBinContent(i+1), hist_eff_tnh.GetBinContent(i+1), True));
        asym_eff_tnh.SetPointEYlow(i, 
                calcEffStatError(err_tnhTri_rebin.GetBinContent(i+1), err_tnhRef_rebin.GetBinContent(i+1), hist_eff_tnh.GetBinContent(i+1), False));
        asym_eff_smhh.SetPointEYhigh(i, 
                calcEffStatError(err_smhhTri_rebin.GetBinContent(i+1), err_smhhRef_rebin.GetBinContent(i+1), hist_eff_smhh.GetBinContent(i+1), True));
        asym_eff_smhh.SetPointEYlow(i, 
                calcEffStatError(err_smhhTri_rebin.GetBinContent(i+1), err_smhhRef_rebin.GetBinContent(i+1), hist_eff_smhh.GetBinContent(i+1), False));
        asym_eff_grav400.SetPointEYhigh(i, 
                calcEffStatError(err_grav400Tri_rebin.GetBinContent(i+1), err_grav400Ref_rebin.GetBinContent(i+1), hist_eff_grav400.GetBinContent(i+1), True));
        asym_eff_grav400.SetPointEYlow(i, 
                calcEffStatError(err_grav400Tri_rebin.GetBinContent(i+1), err_grav400Ref_rebin.GetBinContent(i+1), hist_eff_grav400.GetBinContent(i+1), False));
        asym_eff_grav600.SetPointEYhigh(i, 
                calcEffStatError(err_grav600Tri_rebin.GetBinContent(i+1), err_grav600Ref_rebin.GetBinContent(i+1), hist_eff_grav600.GetBinContent(i+1), True));
        asym_eff_grav600.SetPointEYlow(i, 
                calcEffStatError(err_grav600Tri_rebin.GetBinContent(i+1), err_grav600Ref_rebin.GetBinContent(i+1), hist_eff_grav600.GetBinContent(i+1), False));
        asym_eff_scal400.SetPointEYhigh(i, 
                calcEffStatError(err_scal400Tri_rebin.GetBinContent(i+1), err_scal400Ref_rebin.GetBinContent(i+1), hist_eff_scal400.GetBinContent(i+1), True));
        asym_eff_scal400.SetPointEYlow(i, 
                calcEffStatError(err_scal400Tri_rebin.GetBinContent(i+1), err_scal400Ref_rebin.GetBinContent(i+1), hist_eff_scal400.GetBinContent(i+1), False));
        asym_eff_scal600.SetPointEYhigh(i, 
                calcEffStatError(err_scal600Tri_rebin.GetBinContent(i+1), err_scal600Ref_rebin.GetBinContent(i+1), hist_eff_scal600.GetBinContent(i+1), True));
        asym_eff_scal600.SetPointEYlow(i, 
                calcEffStatError(err_scal600Tri_rebin.GetBinContent(i+1), err_scal600Ref_rebin.GetBinContent(i+1), hist_eff_scal600.GetBinContent(i+1), False));

    # Canvas for jet-level trigger efficiency
    ROOT.gStyle.SetOptStat(0)
    canvas = Canvas()
    canvas.cd()
    pad1 = Pad(0.0,0.33,1.0,1.0)
    pad2 = Pad(0.0,0.0,1.0,0.33)
    pad1.SetBottomMargin(0.015)
    pad1.SetTopMargin(0.05)
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.3)
    pad1.Draw()
    pad2.Draw()
    pad1.SetTicks(1, 1)
    pad2.SetTicks(1, 1)

    pad1.cd()
    hist_eff_tnh.Draw("pe") 
    asym_eff_tnh.Draw("same pe") 
    asym_eff_smhh.Draw("same pe")
    asym_eff_grav400.Draw("same pe") 
    asym_eff_grav600.Draw("same pe") 
    asym_eff_scal400.Draw("same pe") 
    asym_eff_scal600.Draw("same pe") 

    pad2.cd()
    rat_smhh = hist_eff_smhh.Clone("smhh_over_tnh")
    rat_smhh.Divide(hist_eff_tnh)
    rat_smhh.GetYaxis().SetRangeUser(0.20, 1.80)
    rat_smhh.GetYaxis().SetNdivisions(505)
    rat_smhh.GetXaxis().SetLabelSize(0.11)
    rat_smhh.GetYaxis().SetLabelSize(0.1)
    rat_smhh.GetXaxis().SetLabelOffset(0.009)
    rat_smhh.GetYaxis().SetLabelOffset(0.007)
    rat_smhh.GetYaxis().SetTitle("#varepsilon_{sig} / #varepsilon_{ttbar}")
    rat_smhh.GetYaxis().CenterTitle();
    rat_smhh.GetXaxis().SetTitle(x_label_dict(column))
    rat_smhh.SetTitle('')
    rat_smhh.GetYaxis().SetTitleSize(0.12)
    rat_smhh.GetXaxis().SetTitleSize(0.12)
    rat_smhh.GetYaxis().SetTitleOffset(0.35)
    rat_smhh.GetXaxis().SetTitleOffset(0.95)

    rat_grav400 = hist_eff_grav400.Clone("grav400_over_tnh")
    rat_grav400.Divide(hist_eff_tnh)
    rat_grav600 = hist_eff_grav600.Clone("grav600_over_tnh")
    rat_grav600.Divide(hist_eff_tnh)
 
    rat_scal400 = hist_eff_scal400.Clone("scal400_over_tnh")
    rat_scal400.Divide(hist_eff_tnh)
    rat_scal600 = hist_eff_scal600.Clone("scal600_over_tnh")
    rat_scal600.Divide(hist_eff_tnh)
 
    rat_smhh.Draw("pe");
    rat_grav400.Draw("same pe");
    rat_grav600.Draw("same pe");
    rat_scal400.Draw("same pe");
    rat_scal600.Draw("same pe");


    pad2.cd()
    line1 = ROOT.TLine(xlim[0], 1.0, xlim[1], 1.0)
    line1.SetLineColor(1)
    line1.SetLineWidth(1)
    line1.SetLineStyle(2)
    line1.Draw()

    pad1.cd()
    line1.Draw()
    if trig == "b1j0" and not doL1TurnOn:
        legend1 = ROOT.TLegend(0.15, 0.13, 0.40, 0.53)
    else:
        legend1 = ROOT.TLegend(0.58, 0.13, 0.83, 0.53)
    legend1.AddEntry(asym_eff_tnh,  'ttbar(lep)', 'pe')
    legend1.AddEntry(asym_eff_smhh, 'SMHH(NR)', 'pe')
    legend1.AddEntry(asym_eff_grav400, 'RS G_{kk} 400GeV', 'pe')
    legend1.AddEntry(asym_eff_grav600, 'RS G_{kk} 600GeV', 'pe')
    legend1.AddEntry(asym_eff_scal400, 'Scalar 400GeV', 'pe')
    legend1.AddEntry(asym_eff_scal600, 'Scalar 600GeV', 'pe')
    legend1.SetBorderSize(0)
    legend1.SetFillStyle(0)
    legend1.Draw() 

    lat_label1 = HLTrigName(year, trig, nth[n])
    if doL1TurnOn:
       lat_label1 = L1TrigName(year, trig, nth[n])
    ATLASLabel(0.15, 0.855, 0.058, internal, ('#sqrt{s} = 13 TeV, '+year+', '+str(label_lumi)+' fb^{-1}'), lat_label1, 0.77, 0.065)
    DateLatex(2)

    canvas.Draw()
    canvas.SaveAs((output_dir+'sigTrig'+yr_short+'_'+trig+'_'+column+label+'.C'))
    canvas.SaveAs((output_dir+'sigTrig'+yr_short+'_'+trig+'_'+column+label+'.pdf'))

    ### Fill systematic on HLTJet size cut ###
    syst = rat_smhh.Clone(nth[n]+"_diff_ttbarVersusSignal")
    syst.GetYaxis().SetRangeUser(0.0, 2.0)
    syst.GetYaxis().SetTitleSize(0.040)
    syst.GetYaxis().SetLabelSize(0.040)
    syst.GetXaxis().SetTitleSize(0.040)
    syst.GetXaxis().SetLabelSize(0.040)
    syst.GetYaxis().SetTitleOffset(1.05)
    syst.GetXaxis().SetTitleOffset(1.05)
    for i in range(len(rebins)-1):
        syst_ttbarVersusSignal = calcSignalSystError( \
            rat_grav400.GetBinContent(i+1), rat_grav400.GetBinContent(i+1), rat_scal400.GetBinContent(i+1), rat_scal600.GetBinContent(i+1), rat_smhh.GetBinContent(i+1),
            rat_grav400.GetBinError(i+1),   rat_grav400.GetBinError(i+1),   rat_scal400.GetBinError(i+1),   rat_scal600.GetBinError(i+1),   rat_smhh.GetBinError(i+1))
        syst.SetBinContent(i+1, syst_ttbarVersusSignal)
        syst.SetBinError(i+1, 0.)

    fout = ROOT.TFile(foutName,'update')
    fout.cd()
    tree = fout.Get(trigName)
    tree.cd()
    tree.Add(syst)
    fout.Write()
    fout.Close() 

