#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from numpy.lib.recfunctions import append_fields
from trigUtils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")

parser.add_option("--dat", dest="dat_file", default="",
                  help="Input data filename")
parser.add_option("--tth", dest="tth_file", default="",
                  help="Input tth filename")
parser.add_option("--tnh", dest="tnh_file", default="",
                  help="Input tnh filename")
parser.add_option("--vjj", dest="vjj_file", default="",
                  help="Input vjets filename")
parser.add_option("--dib", dest="dib_file", default="",
                  help="Input diboson filename")

parser.add_option("--year", dest="year", default="2016",
                  help="Year (default: 2016)")
parser.add_option("--ttbarSelection", action="store_true",
                  dest="ttbarSelection", default=False, 
                  help="Enable to apply ttbar selection")
parser.add_option("--label", dest="label", default="",
                  help="Year (default: 2017)")

(options, args) = parser.parse_args()

import root_numpy
import ROOT

# configure local options
ttbarSelection = options.ttbarSelection
label = options.label
if ttbarSelection:
    label += "_TOP"
else:
    label += "_NOM"

year_in = options.year
year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2016")
    year='2016'
    yr_short='16'

# configure input files
input_dir = options.input_dir
output_dir = options.output_dir
if input_dir:
    if input_dir[-1] != '/': input_dir+='/'
if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

dat_file = options.dat_file
if dat_file:
    if dat_file[0] == '/':
        input_dir = ''
    dat_file = input_dir+dat_file
    if dat_file[-4:] != 'root':
        print("Warning! Data file not a root file")

tth_file = options.tth_file
if tth_file:
    if tth_file[0] != '/':
        tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! Non tth file not a root file")

tnh_file = options.tnh_file
if tnh_file:
    if tnh_file[0] != '/':
        tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non tnh file not a root file")

vjj_file = options.vjj_file
if vjj_file:
    if vjj_file[0] != '/':
        vjj_file = input_dir+vjj_file
    if vjj_file[-4:] != 'root':
        print("Warning! Non vjets file not a root file")

dib_file = options.dib_file
if dib_file:
    if dib_file[0] != '/':
        dib_file = input_dir+dib_file
    if dib_file[-4:] != 'root':
        print("Warning! Non diboson file not a root file")

print("Start to product ttbar purity distribution in ", year)
print("Enable to apply ttbar selection ?                  -> ", ttbarSelection)
print("Initialization done!")

# Set up columns
columns    = ['pass_mutrig', 'pass_eltrig', 'run_number', 'njets',
              'HT_Off', 'pT_j1', 'pT_j2', 'pT_j3', 'pT_j4',
              'eta_j1', 'eta_j2', 'eta_j3', 'eta_j4', 'phi_j1', 'phi_j2', 'phi_j3', 'phi_j4']
columns_mc = ['pass_mutrig', 'pass_eltrig', 'run_number', 'njets', 'mc_sf',
              'HT_Off', 'pT_j1', 'pT_j2', 'pT_j3', 'pT_j4',
              'eta_j1', 'eta_j2', 'eta_j3', 'eta_j4', 'phi_j1', 'phi_j2', 'phi_j3', 'phi_j4']
if ttbarSelection:
    columns    += ['ntag', 'nmuons', 'nels', 'mtw', 'met']
    columns_mc += ['ntag', 'nmuons', 'nels', 'mtw', 'met']

# Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
arrs = {}
print("Start preparing data file")
arrs['dat'] = make_arrs(dat_file, columns,    yr_short, "b2j2", ttbarSelection, False, False, ttbarPurity=True)
print("Start preparing ttbar-hadronic file")
arrs['tth'] = make_arrs(tth_file, columns_mc, yr_short, "b2j2", ttbarSelection, False, False, ttbarPurity=True)
print("Start preparing ttbar-leptonic file")
arrs['tnh'] = make_arrs(tnh_file, columns_mc, yr_short, "b2j2", ttbarSelection, False, False, ttbarPurity=True)
print("Start preparing boson+jets file")
arrs['vjj'] = make_arrs(vjj_file, columns_mc, yr_short, "b2j2", ttbarSelection, False, False, ttbarPurity=True)
print("Start preparing diboson file")
arrs['dib'] = make_arrs(dib_file, columns_mc, yr_short, "b2j2", ttbarSelection, False, False, ttbarPurity=True)
print("Filling done!")

# Setup output file
from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)
from array import array

label_lumi = mc_lumi(year)

columns_to_plot = ['pT_j1', 'pT_j2', 'pT_j3', 'pT_j4', 'HT_Off', 'njets']
nth = ['1st', '2nd', '3rd', '4th', 'HT']

############################# START MAKING TTBAR PURITY PLOTS #############################
count = 0
for column in columns_to_plot:

    n_binsBase = 40
    if column.find('HT') != -1:
       n = 4
    elif column.find('njets') != -1:
       n = 1
       n_binsBase = 10
    elif column.find('nth') != -1:
       n = count
       n_binsBase = 10
       count = count +1
    else:
       n = int(column[-1])-1
    passNth = 'pass_'+nth[n]
    xlimBase = xlims(column, arrs['dat'])
   
    # data reference 
    hist_datBase = Hist(n_binsBase, xlimBase[0], xlimBase[1], title='Data', legendstyle='PE')
    hist_datBase.fill_array(arrs['dat'][arrs['dat'][passNth+'Base']][column])
    hist_datBase.SetBinContent(n_binsBase, hist_datBase.GetBinContent(n_binsBase)+hist_datBase.GetBinContent(n_binsBase+1))
    hist_datBase.SetMarkerStyle(20)
    hist_datBase.SetMarkerSize(0.6)
    hist_datBase.SetMarkerColor(1)
    hist_datBase.SetLineColor(1)
 
    # ttbar had reference 
    hist_tthBase = Hist(n_binsBase, xlimBase[0], xlimBase[1], title='ttbar(had)', legendstyle='F')
    hist_tthBase.fill_array(arrs['tth'][arrs['tth'][passNth+'Base']][column], weights=arrs['tth'][arrs['tth'][passNth+'Base']]['mc_sf'])
    hist_tthBase.SetBinContent(n_binsBase, hist_tthBase.GetBinContent(n_binsBase)+hist_tthBase.GetBinContent(n_binsBase+1))
    hist_tthBase.SetLineColor(ROOT.kGray)
    hist_tthBase.SetFillColor(ROOT.kGray)
    hist_tthBase.SetFillStyle('solid')
 
    # ttbar lep reference 
    hist_tnhBase = Hist(n_binsBase, xlimBase[0], xlimBase[1], title='ttbar(lep)', legendstyle='F')
    hist_tnhBase.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Base']][column], weights=arrs['tnh'][arrs['tnh'][passNth+'Base']]['mc_sf'])
    hist_tnhBase.SetBinContent(n_binsBase, hist_tnhBase.GetBinContent(n_binsBase)+hist_tnhBase.GetBinContent(n_binsBase+1))
    hist_tnhBase.SetLineColor(1)
    hist_tnhBase.SetFillColor(0)
 
    # vjj reference 
    hist_vjjBase = Hist(n_binsBase, xlimBase[0], xlimBase[1], title='V+jets', legendstyle='F')
    hist_vjjBase.fill_array(arrs['vjj'][arrs['vjj'][passNth+'Base']][column], weights=arrs['vjj'][arrs['vjj'][passNth+'Base']]['mc_sf'])
    hist_vjjBase.SetBinContent(n_binsBase, hist_vjjBase.GetBinContent(n_binsBase)+hist_vjjBase.GetBinContent(n_binsBase+1))
    hist_vjjBase.SetLineColor(94)
    hist_vjjBase.SetFillColor(94)
    hist_vjjBase.SetFillStyle('solid')
 
    # diboson reference 
    hist_dibBase = Hist(n_binsBase, xlimBase[0], xlimBase[1], title='Diboson', legendstyle='F')
    hist_dibBase.fill_array(arrs['dib'][arrs['dib'][passNth+'Base']][column], weights=arrs['dib'][arrs['dib'][passNth+'Base']]['mc_sf'])
    hist_dibBase.SetBinContent(n_binsBase, hist_dibBase.GetBinContent(n_binsBase)+hist_dibBase.GetBinContent(n_binsBase+1))
    hist_dibBase.SetLineColor(5)
    hist_dibBase.SetFillColor(5)
    hist_dibBase.SetFillStyle('solid')

    # stack MC samples
    hist_mcBase = HistStack(hists=[hist_dibBase, hist_vjjBase, hist_tthBase, hist_tnhBase])

    # Canvas for Nth jet pT distribution
    canvas = Canvas()
    canvas.cd()
    pad1 = Pad(0.0,0.33,1.0,1.0)
    pad2 = Pad(0.0,0.0,1.0,0.33)
    pad1.SetBottomMargin(0.015)
    pad1.SetTopMargin(0.05)
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.3)
    pad1.Draw()
    pad2.Draw()
    pad1.SetTicks(1, 1)
    pad2.SetTicks(1, 1)

    pad1.cd()
    hist_mcBase.SetMaximum(hist_datBase.GetMaximum()*1.7)
    hist_mcBase.Draw("hist")
    hist_datBase.Draw("same pe")
    hist_mcBase.GetXaxis().SetLabelSize(0)
    hist_mcBase.GetYaxis().SetLabelSize(0.045)
    hist_mcBase.GetYaxis().SetLabelOffset(0.005)

    pad2.cd()
    ROOT.gStyle.SetOptStat(0)
    rat_dat = hist_mcBase.GetStack().Last().Clone("dist_mc_vs_data")
    rat_dat.Divide(hist_datBase)
    rat_dat.GetYaxis().SetRangeUser(0.60, 1.40)
    rat_dat.GetYaxis().SetNdivisions(505)
    rat_dat.GetXaxis().SetLabelSize(0.11)
    rat_dat.GetYaxis().SetLabelSize(0.1)
    rat_dat.GetXaxis().SetLabelOffset(0.009)
    rat_dat.GetYaxis().SetLabelOffset(0.007)
    rat_dat.GetYaxis().SetTitle("MC / Data")
    rat_dat.GetYaxis().CenterTitle();
    rat_dat.GetXaxis().SetTitle(x_label_dict(column))
    rat_dat.SetTitle('')
    rat_dat.GetYaxis().SetTitleSize(0.12)
    rat_dat.GetXaxis().SetTitleSize(0.12)
    rat_dat.GetYaxis().SetTitleOffset(0.35)
    rat_dat.GetXaxis().SetTitleOffset(0.95)

    rat_dat.SetMarkerStyle(20)
    rat_dat.SetMarkerSize(0.6)
    rat_dat.SetMarkerColor(1)
    rat_dat.SetLineColor(1)
    rat_dat.Draw();

    line = ROOT.TLine(hist_datBase.GetXaxis().GetXmin(), 1, hist_datBase.GetXaxis().GetXmax(), 1)
    line.SetLineColor(1)
    line.SetLineWidth(1)
    line.SetLineStyle(2)
    line.Draw()

    pad1.cd()
    legend = ROOT.TLegend(0.56, 0.55, 0.85, 0.89)
    legend.AddEntry(hist_datBase, 'Data', 'pe')
    legend.AddEntry(hist_tnhBase, 'ttbar(lep)', 'F')
    legend.AddEntry(hist_tthBase, 'ttbar(had)', 'F')
    legend.AddEntry(hist_vjjBase, 'V+jets', 'F')
    legend.AddEntry(hist_dibBase, 'WW/ZZ/WZ', 'F')
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.Draw() 

    lat_label1 = 'Pure ttbar-leptonic events'
    ATLASLabel(0.15, 0.85, 0.058, 'Internal', ('#sqrt{s} = 13 TeV, '+year+', '+str(label_lumi)+' fb^{-1}'), lat_label1)
    DateLatex(2)

    canvas.Draw()
    
    canvas.SaveAs((output_dir+'kdis'+yr_short+'_'+'baseline'+'_'+column+label+'.C'))
    canvas.SaveAs((output_dir+'kdis'+yr_short+'_'+'baseline'+'_'+column+label+'.pdf'))
