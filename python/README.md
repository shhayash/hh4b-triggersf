# Python tools

The tools using root_numpy and Python 3 to validate and make trigger SFs are at `python/`. 
The environment setup for lxplus is python3 LCG 96.

Add the following to your `.bashrc` or `.zshrc`:
``` console
alias lcg96bpy3='source /cvmfs/sft.cern.ch/lcg/views/LCG_96bpython3/x86_64-centos7-gcc8-opt/setup.sh'
```
You can then load the LCG release with `lcg96bpy3`.

### Validating the emulation method
If you want to check if there is any difference between the emulation method and the TrigDecisionTool method, 
you can do by `python/valiTrigEmu.py`. The help text is:
``` console
$ python python/valiTrigEmu.py --help
Usage: valiTrigEmu.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  --mc=MC_FILE          Input MC filename
  --trig=TRIG           Trigger, where start to validate trigger efficiency
                        (default: b2j1)
  --year=YEAR           Year (default: 2017)
  --doL1TurnOn          Enable to show L1 turn on, (default: HLT turn on)
  --applyJetTrig        Apply the requirement that any jet trigger fires
  --ttbarSelection      Enable to apply ttbar selection
  --label=LABEL         Year (default: 2017)
```

You can run it on MC with a command like:
``` console
$ ### L1 turn on curve
$ python python/valiTrigEmu.py --input_dir ${input_dir} --mc tnh2017.root --trig "b2j2" --year 2017 -o ${output_dir} --doL1TurnOn --applyJetTrig --ttbarSelection
$ ### HLT turn on curve
$ python python/valiTrigEmu.py --input_dir ${input_dir} --mc tnh2017.root --trig "b2j2" --year 2017 -o ${output_dir} --applyJetTrig --ttbarSelection
```

### Validating a bias of any jet trigger requirement
If you want to validate if there is a bias from the any jet trigger requirement, you can do that by `python/valiTrigBias.py`. 
The help text is:
``` console
$ python valiTrigBias.py --help
Usage: valiTrigBias.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  --data=DATA_FILE      Input Data filename
  --trig=TRIG           Trigger, where start to validate trigger efficiency
                        (default: b2j1)
  --year=YEAR           Year (default: 2017)
  --doL1TurnOn          Enable to show L1 turn on, (default: HLT turn on)
  --ttbarSelection      Enable to apply ttbar selection
  --label=LABEL         Year (default: 2017)
```

You can run it on data with a command like:
``` console
# L1 turn on curve
python python/valiTrigBias.py --input_dir ${input_dir} --data ${data} --trig $trig --year 2017 -o ${output_dir} --doL1TurnOn --ttbarSelection
# HLT turn on curve
python python/valiTrigBias.py --input_dir ${input_dir} --data ${data} --trig $trig --year 2017 -o ${output_dir} --ttbarSelection
```

### Validating any difference between HH4b signal samples and ttbar-leptonic


### Checking ttbar-leptonic purity 
If you want to check ttbar-leptonic purity on pT distribution plots, you can do that by `python/makeTrigSF.py` with an option `--disableSF`.
e.g. a command lik:
``` console
$ python python/makeTrigSF.py -i $input_dir -o $output_dir --dat $dat --tnh $tnh --tth $tth --vjj $vjj --dib $dib --trig "b2j2" --year "2017" --disableSF --ttbarSelection
```

### Applying the trigger SFs



