#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from numpy.lib.recfunctions import append_fields
from trigUtils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")

parser.add_option("--dat", dest="dat_file", default="",
                  help="Input data filename")
parser.add_option("--tth", dest="tth_file", default="",
                  help="Input tth filename")
parser.add_option("--tnh", dest="tnh_file", default="",
                  help="Input tnh filename")
parser.add_option("--vjj", dest="vjj_file", default="",
                  help="Input vjets filename")
parser.add_option("--dib", dest="dib_file", default="",
                  help="Input diboson filename")

parser.add_option("--trig", dest="trig", default="b2j1",
                  help="Trigger, where start to take a product of the SFs (default: b2j1)")
parser.add_option("--year", dest="year", default="2016",
                  help="Year (default: 2016)")
parser.add_option("--doL1TurnOn", action="store_true",
                  dest="doL1TurnOn", default=False,
                  help="Enable to show L1 turn on, (default: HLT turn on)")
parser.add_option("--doL1andHLTTurnOn", action="store_true",
                  dest="doL1andHLTTurnOn", default=False,
                  help="Enable to show L1 and HLT combination turn on, (default: HLT turn on)")
parser.add_option("--ttbarSelection", action="store_true",
                  dest="ttbarSelection", default=False, 
                  help="Enable to apply ttbar selection")
parser.add_option("--fullSyst", action="store_true",
                  dest="fullSyst", default=False,
                  help="Store full systematic, i.e. including ttbar modelling uncertainties")
parser.add_option("--label", dest="label", default="",
                  help="Year (default: 2017)")

(options, args) = parser.parse_args()

import root_numpy
import ROOT

# configure local options
trig = options.trig
doL1TurnOn = options.doL1TurnOn
doL1andHLTTurnOn = options.doL1andHLTTurnOn
ttbarSelection = options.ttbarSelection
fullSyst = options.fullSyst
label = options.label
if doL1andHLTTurnOn:
    label += "_L1andHLT"
elif doL1TurnOn:
    label += "_L1"
else:
    label += "_HLT"
if ttbarSelection:
    label += "_TOP"
else:
    label += "_NOM"
if fullSyst:
    label += "_fullSyst"

print("Start to product trigger SFs from jet-level trigger efficiency")
print("Enable to apply ttbar selection ?                  -> ", ttbarSelection)
print("Store full systematic uncertainties ?              -> ", fullSyst)
if doL1andHLTTurnOn:
    print("Setup to L1 and HLT combination turn on curve")
elif doL1TurnOn:
    print("Setup to L1 turn on curve")
else:
    print("Setup to HLT turn on curve")

year_in = options.year
year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2016")
    year='2016'
    yr_short='16'

# configure input files
input_dir = options.input_dir
output_dir = options.output_dir
if input_dir:
    if input_dir[-1] != '/': input_dir+='/'
if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

dat_file = options.dat_file
if dat_file:
    if dat_file[0] == '/':
        input_dir = ''
    dat_file = input_dir+dat_file
    if dat_file[-4:] != 'root':
        print("Warning! Data file not a root file")

tth_file = options.tth_file
if tth_file:
    if tth_file[0] != '/':
        tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! Non tth file not a root file")

tnh_file = options.tnh_file
if tnh_file:
    if tnh_file[0] != '/':
        tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non tnh file not a root file")

vjj_file = options.vjj_file
if vjj_file:
    if vjj_file[0] != '/':
        vjj_file = input_dir+vjj_file
    if vjj_file[-4:] != 'root':
        print("Warning! Non vjets file not a root file")

dib_file = options.dib_file
if dib_file:
    if dib_file[0] != '/':
        dib_file = input_dir+dib_file
    if dib_file[-4:] != 'root':
        print("Warning! Non diboson file not a root file")

print("Initialization done!")
print("Define trigger : ", trig, " in ", year)

# Set up columns
columns    = ['pass_mutrig', 'pass_eltrig', 'run_number']
columns_mc = ['pass_mutrig', 'pass_eltrig', 'run_number', 'mc_sf']
if ttbarSelection:
    columns    += ['ntag', 'nmuons', 'nels', 'mtw', 'met']
    columns_mc += ['ntag', 'nmuons', 'nels', 'mtw', 'met']

# Set up each single jet trigger of Nth jets
nthTrig = nthTrigs(trig, year)
prefixJetName = prefixJetNames(doL1TurnOn) 
for i in range(len(nthTrig)):
    if doL1TurnOn:
        if nthTrig[i].find('HT') != -1: # -> Go to HT cut
            columns    += ['HT_Off', prefixJetName+'isL1Emulated_'+nthTrig[i], prefixJetName+'hasL1Jet_HT']
            columns_mc += ['HT_Off', prefixJetName+'isL1Emulated_'+nthTrig[i], prefixJetName+'hasL1Jet_HT']
        else: # -> Go to Et cut
            columns    += [prefixJetName+'pT_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasL1Jet_j'+str(i+1)]
            columns_mc += [prefixJetName+'pT_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasL1Jet_j'+str(i+1)]
    else:
        if nthTrig[i].find('HT') != -1: # -> Go to HT cut
            columns    += ['HT_Off', prefixJetName+'isL1Emulated_'+nthTrig[i],  prefixJetName+'hasL1Jet_'+nthTrig[i],
                                     prefixJetName+'isHLTEmulated_'+nthTrig[i], prefixJetName+'hasHLTJet_'+nthTrig[i]]
            columns_mc += ['HT_Off', prefixJetName+'isL1Emulated_'+nthTrig[i],  prefixJetName+'hasL1Jet_'+nthTrig[i],
                                     prefixJetName+'isHLTEmulated_'+nthTrig[i], prefixJetName+'hasHLTJet_'+nthTrig[i]]
        else: # -> Go to Et cut
            columns    += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1),  prefixJetName+'hasL1Jet_'+nthTrig[i]+'_j'+str(i+1),
                           prefixJetName+'isHLTEmulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasHLTJet_'+nthTrig[i]+'_j'+str(i+1)]
            columns_mc += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1),  prefixJetName+'hasL1Jet_'+nthTrig[i]+'_j'+str(i+1),
                           prefixJetName+'isHLTEmulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasHLTJet_'+nthTrig[i]+'_j'+str(i+1)]

# Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
arrs = {}
print("Start preparing data file")
arrs['dat'] = make_arrs(dat_file, columns,    yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Start preparing ttbar-hadronic file")
arrs['tth'] = make_arrs(tth_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Start preparing ttbar-leptonic file")
arrs['tnh'] = make_arrs(tnh_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Start preparing boson+jets file")
arrs['vjj'] = make_arrs(vjj_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Start preparing diboson file")
arrs['dib'] = make_arrs(dib_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Filling done!")

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)
from array import array

label_lumi = mc_lumi(year)
columns_to_plot = []
for i in range(len(nthTrig)):
    if nthTrig[i].find('HT') != -1: 
        columns_to_plot += ['HT_Off']
    else:
        if doL1TurnOn:
            columns_to_plot += [prefixJetName+'pT_j'+str(i+1)]
        else:
            columns_to_plot += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1)]

nth = nths(trig)
suffixSyst = ""
if fullSyst:
    suffixSyst = "Full"

 # Setup output file
foutName = "2020-AUG08-13TeV-HH4bJetLevelTriggerScaleFactor-"+year+".root"
fout = ROOT.TFile(foutName,'update')
if doL1andHLTTurnOn:
    trigName = "SF_"+multibTrigName(year, trig)
elif doL1TurnOn:
    trigName = "L1SF_"+multibTrigName(year, trig)
else:
    trigName = "HLTSF_"+multibTrigName(year, trig)
trigName = trigName.replace('.', '-')

have_tree = fout.GetListOfKeys().Contains(trigName)
if (have_tree):
    tree = fout.Get(trigName)
    for nt in nth:
        have_SF = tree.GetListOfKeys().Contains(nt+"JetSF")
        if (have_SF):
            tree.FindKey(nt+"JetSF").Delete()
        have_SFUP = tree.GetListOfKeys().Contains(nt+"JetSF_UP"+suffixSyst)
        if (have_SFUP):
            tree.FindKey(nt+"JetSF_UP"+suffixSyst).Delete()
        have_SFDOWN = tree.GetListOfKeys().Contains(nt+"JetSF_DOWN"+suffixSyst)
        if (have_SFDOWN):
            tree.FindKey(nt+"JetSF_DOWN"+suffixSyst).Delete()
else:
   tree = fout.mkdir(trigName, (year+" "+trig+" trigger"))
fout.Write()
fout.Close()


############################# START MAKING TRIGGER SCALE FACTORS #############################
for column in columns_to_plot:

    if column.find('HT') != -1:
       n = 2
    else:
       n = int(column[-1])-1
    passNth = 'pass_'+nth[n]
    xlim = xlimsTrigSF(year, trig, nth[n])
    n_bins = xlim[2]
 
    ### 1. Trigger Efficiency ###
    # data reference
    hist_datRef = Hist(n_bins, xlim[0], xlim[1], title='Data', legendstyle='PE')
    hist_datRef.fill_array(arrs['dat'][arrs['dat'][passNth+'Ref']][column])
    hist_datRef.SetBinContent(n_bins, hist_datRef.GetBinContent(n_bins)+hist_datRef.GetBinContent(n_bins+1))
    # data trigger
    hist_datTri = Hist(n_bins, xlim[0], xlim[1], title='Data', legendstyle='PE')
    hist_datTri.fill_array(arrs['dat'][arrs['dat'][passNth+'Tri']][column])
    hist_datTri.SetBinContent(n_bins, hist_datTri.GetBinContent(n_bins)+hist_datTri.GetBinContent(n_bins+1))
    
    # ttbar had reference
    hist_tthRef = Hist(n_bins, xlim[0], xlim[1], title='ttbar(had)', legendstyle='PE')
    hist_tthRef.fill_array(arrs['tth'][arrs['tth'][passNth+'Ref']][column], weights=arrs['tth'][arrs['tth'][passNth+'Ref']]['mc_sf'])
    hist_tthRef.SetBinContent(n_bins, hist_tthRef.GetBinContent(n_bins)+hist_tthRef.GetBinContent(n_bins+1))
    # ttbar had trigger
    hist_tthTri = Hist(n_bins, xlim[0], xlim[1], title='ttbar(had)', legendstyle='PE')
    hist_tthTri.fill_array(arrs['tth'][arrs['tth'][passNth+'Tri']][column], weights=arrs['tth'][arrs['tth'][passNth+'Tri']]['mc_sf'])
    hist_tthTri.SetBinContent(n_bins, hist_tthTri.GetBinContent(n_bins)+hist_tthTri.GetBinContent(n_bins+1))

    # ttbar lep reference
    hist_tnhRef = Hist(n_bins, xlim[0], xlim[1], title='ttbar(lep)', legendstyle='PE')
    hist_tnhRef.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Ref']][column], weights=arrs['tnh'][arrs['tnh'][passNth+'Ref']]['mc_sf'])
    hist_tnhRef.SetBinContent(n_bins, hist_tnhRef.GetBinContent(n_bins)+hist_tnhRef.GetBinContent(n_bins+1))
    # ttbar lep trigger
    hist_tnhTri = Hist(n_bins, xlim[0], xlim[1], title='ttbar(lep)', legendstyle='PE')
    hist_tnhTri.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Tri']][column], weights=arrs['tnh'][arrs['tnh'][passNth+'Tri']]['mc_sf'])
    hist_tnhTri.SetBinContent(n_bins, hist_tnhTri.GetBinContent(n_bins)+hist_tnhTri.GetBinContent(n_bins+1))
    
    # vjj reference
    hist_vjjRef = Hist(n_bins, xlim[0], xlim[1], title='V+jets', legendstyle='PE')
    hist_vjjRef.fill_array(arrs['vjj'][arrs['vjj'][passNth+'Ref']][column], weights=arrs['vjj'][arrs['vjj'][passNth+'Ref']]['mc_sf'])
    hist_vjjRef.SetBinContent(n_bins, hist_vjjRef.GetBinContent(n_bins)+hist_vjjRef.GetBinContent(n_bins+1))
    # vjj trigger
    hist_vjjTri = Hist(n_bins, xlim[0], xlim[1], title='V+jets', legendstyle='PE')
    hist_vjjTri.fill_array(arrs['vjj'][arrs['vjj'][passNth+'Tri']][column], weights=arrs['vjj'][arrs['vjj'][passNth+'Tri']]['mc_sf'])
    hist_vjjTri.SetBinContent(n_bins, hist_vjjTri.GetBinContent(n_bins)+hist_vjjTri.GetBinContent(n_bins+1))
    
    # diboson reference
    hist_dibRef = Hist(n_bins, xlim[0], xlim[1], title='Diboson', legendstyle='PE')
    hist_dibRef.fill_array(arrs['dib'][arrs['dib'][passNth+'Ref']][column], weights=arrs['dib'][arrs['dib'][passNth+'Ref']]['mc_sf'])
    hist_dibRef.SetBinContent(n_bins, hist_dibRef.GetBinContent(n_bins)+hist_dibRef.GetBinContent(n_bins+1))
    # diboson trigger
    hist_dibTri = Hist(n_bins, xlim[0], xlim[1], title='Diboson', legendstyle='PE')
    hist_dibTri.fill_array(arrs['dib'][arrs['dib'][passNth+'Tri']][column], weights=arrs['dib'][arrs['dib'][passNth+'Tri']]['mc_sf'])
    hist_dibTri.SetBinContent(n_bins, hist_dibTri.GetBinContent(n_bins)+hist_dibTri.GetBinContent(n_bins+1))
    
    # stack MC samples
    hist_mcRef = HistStack(hists=[hist_dibRef, hist_vjjRef, hist_tthRef, hist_tnhRef])
    hist_mcTri = HistStack(hists=[hist_dibTri, hist_vjjTri, hist_tthTri, hist_tnhTri])
    
    # ttbar had reference
    err_tthRef = Hist(n_bins, xlim[0], xlim[1], title='ttbar(had)', legendstyle='PE')
    err_tthRef.fill_array(arrs['tth'][arrs['tth'][passNth+'Ref']][column])
    err_tthRef.SetBinContent(n_bins, err_tthRef.GetBinContent(n_bins)+err_tthRef.GetBinContent(n_bins+1))
    # ttbar had trigger
    err_tthTri = Hist(n_bins, xlim[0], xlim[1], title='ttbar(had)', legendstyle='PE')
    err_tthTri.fill_array(arrs['tth'][arrs['tth'][passNth+'Tri']][column])
    err_tthTri.SetBinContent(n_bins, err_tthTri.GetBinContent(n_bins)+err_tthTri.GetBinContent(n_bins+1))

    # ttbar lep reference
    err_tnhRef = Hist(n_bins, xlim[0], xlim[1], title='ttbar(lep)', legendstyle='PE')
    err_tnhRef.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Ref']][column])
    err_tnhRef.SetBinContent(n_bins, err_tnhRef.GetBinContent(n_bins)+err_tnhRef.GetBinContent(n_bins+1))
    # ttbar lep trigger
    err_tnhTri = Hist(n_bins, xlim[0], xlim[1], title='ttbar(lep)', legendstyle='PE')
    err_tnhTri.fill_array(arrs['tnh'][arrs['tnh'][passNth+'Tri']][column])
    err_tnhTri.SetBinContent(n_bins, err_tnhTri.GetBinContent(n_bins)+err_tnhTri.GetBinContent(n_bins+1))
    
    # vjj reference
    err_vjjRef = Hist(n_bins, xlim[0], xlim[1], title='V+jets', legendstyle='PE')
    err_vjjRef.fill_array(arrs['vjj'][arrs['vjj'][passNth+'Ref']][column])
    err_vjjRef.SetBinContent(n_bins, err_vjjRef.GetBinContent(n_bins)+err_vjjRef.GetBinContent(n_bins+1))
    # vjj trigger
    err_vjjTri = Hist(n_bins, xlim[0], xlim[1], title='V+jets', legendstyle='PE')
    err_vjjTri.fill_array(arrs['vjj'][arrs['vjj'][passNth+'Tri']][column])
    err_vjjTri.SetBinContent(n_bins, err_vjjTri.GetBinContent(n_bins)+err_vjjTri.GetBinContent(n_bins+1))
    
    # diboson reference
    err_dibRef = Hist(n_bins, xlim[0], xlim[1], title='Diboson', legendstyle='PE')
    err_dibRef.fill_array(arrs['dib'][arrs['dib'][passNth+'Ref']][column])
    err_dibRef.SetBinContent(n_bins, err_dibRef.GetBinContent(n_bins)+err_dibRef.GetBinContent(n_bins+1))
    # diboson trigger
    err_dibTri = Hist(n_bins, xlim[0], xlim[1], title='Diboson', legendstyle='PE')
    err_dibTri.fill_array(arrs['dib'][arrs['dib'][passNth+'Tri']][column])
    err_dibTri.SetBinContent(n_bins, err_dibTri.GetBinContent(n_bins)+err_dibTri.GetBinContent(n_bins+1))
 
    err_mcRef = HistStack(hists=[err_dibRef, err_vjjRef, err_tthRef, err_tnhRef])
    err_mcTri = HistStack(hists=[err_dibTri, err_vjjTri, err_tthTri, err_tnhTri])
 
    # rebin
    bin_width = (xlim[1]-xlim[0])/n_bins
    rebin_base = rebinTrigSF_HLT(year, trig, nth[n])
    if doL1TurnOn:
        rebin_base = rebinTrigSF_L1(year, trig, nth[n])
    rebins = []
    for i in range(len(rebin_base)+1):
        if i == 0:
            rebins.append(xlim[0])
        else:
            rebins.append(rebins[i-1]+bin_width*rebin_base[i-1])
    
    hist_mcTri_rebin = hist_mcTri.GetStack().Last().Rebin(len(rebins)-1, 'eff_mcTri_rebin', array('d', rebins))
    hist_mcRef_rebin = hist_mcRef.GetStack().Last().Rebin(len(rebins)-1, 'eff_mcRef_rebin', array('d', rebins))
    hist_datTri_rebin = hist_datTri.Rebin(len(rebins)-1, 'eff_datTri_rebin', array('d', rebins))
    hist_datRef_rebin = hist_datRef.Rebin(len(rebins)-1, 'eff_datRef_rebin', array('d', rebins))
    err_mcTri_rebin = err_mcTri.GetStack().Last().Rebin(len(rebins)-1, 'err_mcTri_rebin', array('d', rebins))
    err_mcRef_rebin = err_mcRef.GetStack().Last().Rebin(len(rebins)-1, 'err_mcRef_rebin', array('d', rebins))
    
    hist_eff_mc = hist_mcTri_rebin.Clone("eff_mc")
    hist_eff_mc.Divide(hist_mcRef_rebin)
    hist_eff_mc.SetMarkerStyle(24)
    hist_eff_mc.SetMarkerSize(0.7)
    hist_eff_mc.SetMarkerColor(ROOT.kRed)
    hist_eff_mc.SetLineColor(ROOT.kRed)
    hist_eff_mc.GetYaxis().SetRangeUser(0.00, 1.45)
    hist_eff_mc.GetXaxis().SetLabelSize(0.040)
    hist_eff_mc.GetYaxis().SetLabelSize(0.040)
    hist_eff_mc.GetXaxis().SetLabelOffset(0.009)
    hist_eff_mc.GetYaxis().SetLabelOffset(0.007)
    if doL1andHLTTurnOn:
        hist_eff_mc.GetYaxis().SetTitle("Trigger Efficiency #varepsilon")
    elif doL1TurnOn:
        hist_eff_mc.GetYaxis().SetTitle("Trigger Efficiency #varepsilon_{L1}")
    else:
        hist_eff_mc.GetYaxis().SetTitle("Trigger Efficiency #varepsilon_{HLT}")
    hist_eff_mc.GetXaxis().SetTitle(x_label_dict(column))
    hist_eff_mc.SetTitle('')
    hist_eff_mc.GetYaxis().SetTitleSize(0.042)
    hist_eff_mc.GetXaxis().SetTitleSize(0.042)
    hist_eff_mc.GetYaxis().SetTitleOffset(1.05)
    hist_eff_mc.GetXaxis().SetTitleOffset(1.05)
    
    hist_eff_dat = hist_datTri_rebin.Clone("eff_dat")
    hist_eff_dat.Divide(hist_datRef_rebin)
    hist_eff_dat.SetMarkerStyle(20)
    hist_eff_dat.SetMarkerSize(0.7)
    hist_eff_dat.SetMarkerColor(1)
    hist_eff_dat.SetLineColor(1)    
    
    # Canvas for jet-level trigger efficiency
    ROOT.gStyle.SetOptStat(0) 
    canvas2 = Canvas()
    canvas2.cd()
    pad2_1 = Pad(0.0,0.0,1.0,1.0)
    pad2_1.Draw()
    pad2_1.cd()
    pad2_1.SetTicks(1, 1)
    
    # Set statistic and systematic error
    gx_eff = []
    gy_eff = []
    gex_eff = []
    gey_up_eff = []
    gey_dw_eff = []
    for i in range(len(rebins)-1):
        yval = hist_eff_dat.GetBinContent(i+1)
        gx_eff.append((rebins[i+1]+rebins[i])/2)
        gy_eff.append(yval)
        gex_eff.append((rebins[i+1]-rebins[i])/2)

        # MC statistic error
        mc_stat_err = calcEffStatError(err_mcTri_rebin.GetBinContent(i+1), err_mcRef_rebin.GetBinContent(i+1), hist_eff_mc.GetBinContent(i+1))

        # Data statistic and systematic error
        stat_err = calcEffStatError(hist_datTri_rebin.GetBinContent(i+1), hist_datRef_rebin.GetBinContent(i+1), hist_eff_dat.GetBinContent(i+1))
        syst_err = getSystErrors(i+1, year, trig, nth[n], doL1TurnOn, doL1andHLTTurnOn, fullSyst, False)*hist_eff_dat.GetBinContent(i+1)
        gey_dw_eff.append(np.sqrt(stat_err**2+syst_err**2))
        if (np.sqrt(stat_err**2+syst_err**2) + hist_eff_dat.GetBinContent(i+1) > 1.0): # trigger efficiency should be < 1.0
            gey_up_eff.append(1.0 - hist_eff_dat.GetBinContent(i+1))
        else:
            gey_up_eff.append(np.sqrt(stat_err**2+syst_err**2))
        hist_eff_dat.SetBinError(i+1, stat_err)
        hist_eff_mc.SetBinError(i+1, mc_stat_err)
    
    graph_mcErr2_1 = TEAsymGraph(len(rebins)-1, gx_eff, gy_eff, gex_eff, gex_eff, gey_dw_eff, gey_up_eff)
    graph_mcErr2_1.SetFillColor(ROOT.kSpring-8)  # green
    graph_mcErr2_1.SetFillStyle(1001)
    
    pad2_1.cd()
    hist_eff_mc.Draw("pe") 
    graph_mcErr2_1.Draw("same 2") 
    hist_eff_mc.Draw("same pe")
    hist_eff_dat.Draw("same pe")

    pad2_1.RedrawAxis()
    
    line2 = ROOT.TLine(hist_datRef_rebin.GetXaxis().GetXmin(), 1.0, hist_datRef_rebin.GetXaxis().GetXmax(), 1.0)
    line2.SetLineColor(1)
    line2.SetLineWidth(1)
    line2.SetLineStyle(2)
    line2.Draw()
    
    legend2 = ROOT.TLegend(0.55, 0.72, 0.85, 0.85)
    legend2.AddEntry(hist_eff_dat, 'Data', 'pe')
    legend2.AddEntry(hist_eff_mc, 'Simulation', 'pe')
    legend2.AddEntry(graph_mcErr2_1, 'Stat+Syst Error', 'F')
    legend2.SetBorderSize(0)
    legend2.SetFillStyle(0)
    legend2.Draw() 
    
    lat_label2 = HLTrigName(year, trig, nth[n])
    if doL1TurnOn:
        lat_label2 = L1TrigName(year, trig, nth[n])
    ATLASLabel(0.15, 0.83, 0.041, 'Internal', ('#sqrt{s} = 13 TeV, '+year+', '+str(label_lumi)+' fb^{-1}'), lat_label2, 0.80, 0.048)
    DateLatex(1)
 
    canvas2.Draw()
    canvas2.SaveAs((output_dir+'tref'+yr_short+'_'+trig+'_'+column+label+'.C'))
    canvas2.SaveAs((output_dir+'tref'+yr_short+'_'+trig+'_'+column+label+'.pdf'))
    
    
    ### 2. Trigger Scale Factor ###
    hist_sf = hist_eff_dat.Clone("scalefactor")
    hist_sf.Divide(hist_eff_mc)
    hist_sf.SetMarkerStyle(20)
    hist_sf.SetMarkerSize(1.0)
    hist_sf.SetMarkerColor(1)
    hist_sf.SetLineColor(1)
    hist_sf.GetYaxis().SetRangeUser(0.00, 2.0)
    #hist_sf.SetMaximum(hist_sf.GetMaximum()*1.7)
    hist_sf.GetXaxis().SetLabelSize(0.040)
    hist_sf.GetYaxis().SetLabelSize(0.040)
    hist_sf.GetXaxis().SetLabelOffset(0.009)
    hist_sf.GetYaxis().SetLabelOffset(0.007)
    hist_sf.GetYaxis().SetTitle("#varepsilon_{data} / #varepsilon_{MC}")
    hist_sf.GetXaxis().SetTitle(x_label_dict(column))
    hist_sf.SetTitle('')
    hist_sf.GetYaxis().SetTitleSize(0.042)
    hist_sf.GetXaxis().SetTitleSize(0.042)
    hist_sf.GetYaxis().SetTitleOffset(1.05)
    hist_sf.GetXaxis().SetTitleOffset(1.05)
    
    trigSF = hist_sf.Clone(nth[n]+"JetSF")
    suffixSyst = ""
    if fullSyst:
        suffixSyst = "Full"
    trigSF_up = hist_sf.Clone(nth[n]+"JetSF_UP"+suffixSyst)
    trigSF_down = hist_sf.Clone(nth[n]+"JetSF_DOWN"+suffixSyst)
    
    # set error
    gx_sf = []
    gy_sf = []
    gex_sf = []
    gey_up_sf = []
    gey_dw_sf = []
    for i in range(len(rebins)-1):
        yval = hist_sf.GetBinContent(i+1)
        gx_sf.append((rebins[i+1]+rebins[i])/2)
        gy_sf.append(yval)
        gex_sf.append((rebins[i+1]-rebins[i])/2)

        # MC statistic error
        mc_stat_err = calcEffStatError(err_mcTri_rebin.GetBinContent(i+1), err_mcRef_rebin.GetBinContent(i+1), hist_eff_mc.GetBinContent(i+1))

        # Data statistic error and systematic error
        stat_err = calcEffStatError(hist_datTri_rebin.GetBinContent(i+1), hist_datRef_rebin.GetBinContent(i+1), hist_eff_dat.GetBinContent(i+1))
        syst_err = getSystErrors(i+1, year, trig, nth[n], doL1TurnOn, doL1andHLTTurnOn, fullSyst, True)*hist_eff_dat.GetBinContent(i+1)
        gey_dw_sf.append(convSFError(stat_err, syst_err, hist_eff_mc.GetBinContent(i+1), hist_eff_dat.GetBinContent(i+1)))

        if (np.sqrt(stat_err**2+syst_err**2) + hist_eff_dat.GetBinContent(i+1) > 1.0): # trigger efficiency should be < 1.0
            if ((1-hist_eff_dat.GetBinContent(i+1))**2-stat_err*2) > 0.:
                syst_err = np.sqrt((1-hist_eff_dat.GetBinContent(i+1))**2-stat_err*2)
            else:
                syst_err = 0
        gey_up_sf.append(convSFError(stat_err, syst_err, hist_eff_mc.GetBinContent(i+1), hist_eff_dat.GetBinContent(i+1)))
    
        # For Output file
        if hist_eff_mc.GetBinContent(i+1)==0:
            hist_sf.SetBinError(i+1, (0*stat_err))
        else:
            hist_sf.SetBinError(i+1, (1/hist_eff_mc.GetBinContent(i+1)*stat_err))
            trigSF.SetBinError(i+1, gey_dw_sf[i]) # stat and syst
            trigSF_up.SetBinContent(i+1, (trigSF.GetBinContent(i+1)+gey_up_sf[i])/trigSF.GetBinContent(i+1))   # fill a ratio of up/nominal.
            trigSF_up.SetBinError(i+1, 0)
            trigSF_down.SetBinContent(i+1, (trigSF.GetBinContent(i+1)-gey_dw_sf[i])/trigSF.GetBinContent(i+1)) # fill a ratio of down/nominal.
            trigSF_down.SetBinError(i+1, 0)
    
    graph_mcErr3_1 = TEAsymGraph(len(rebins)-1, gx_sf, gy_sf, gex_sf, gex_sf, gey_dw_sf, gey_up_sf)
    graph_mcErr3_1.SetFillColor(ROOT.kSpring-8)  # green
    graph_mcErr3_1.SetFillStyle(1001)
    
    canvas3 = Canvas()
    canvas3.cd()
    pad3_1 = Pad(0.0,0.0,1.0,1.0)
    pad3_1.Draw()
    pad3_1.cd()
    pad3_1.SetTicks(1, 1)
    
    hist_sf.Draw("pe") 
    graph_mcErr3_1.Draw("same 2")
    hist_sf.Draw("same pe") 
    
    pad3_1.RedrawAxis()
    
    line3 = ROOT.TLine(hist_datRef_rebin.GetXaxis().GetXmin(), 1.0, hist_datRef_rebin.GetXaxis().GetXmax(), 1.0)
    line3.SetLineColor(1)
    line3.SetLineWidth(1)
    line3.SetLineStyle(2)
    line3.Draw()
    
    legend3 = ROOT.TLegend(0.55, 0.72, 0.85, 0.82)
    legend3.AddEntry(hist_sf, 'Scale Factor (Stat.)', 'pe')
    legend3.AddEntry(graph_mcErr3_1, 'Stat+Syst Error', 'F')
    legend3.SetBorderSize(0)
    legend3.SetFillStyle(0)
    legend3.Draw() 
    
    ATLASLabel(0.15, 0.83, 0.041, 'Internal', ('#sqrt{s} = 13 TeV, '+year+', '+str(label_lumi)+' fb^{-1}'), lat_label2, 0.8, 0.048)
    DateLatex(1)

    canvas3.Draw()
    canvas3.SaveAs((output_dir+'tfsf'+yr_short+'_'+trig+'_'+column+label+'.C'))
    canvas3.SaveAs((output_dir+'tfsf'+yr_short+'_'+trig+'_'+column+label+'.pdf'))
    
    ### Fill trigger SFs ###
    fout = ROOT.TFile(foutName,'update')
    fout.cd()
    tree = fout.Get(trigName)
    tree.cd()
    tree.Add(trigSF)
    tree.Add(trigSF_up)
    tree.Add(trigSF_down)
    fout.Write()
    fout.Close()
    
