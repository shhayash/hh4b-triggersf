#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from numpy.lib.recfunctions import append_fields
from trigUtils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("--nominal", dest="nominal_file", default="",
                  help="Input nominal ttbar filename (i.e. 410470.PhPy8EG)")
parser.add_option("--systGen", dest="systGen_file", default="",
                  help="Input systematic generator ttbar filename (i.e. Generator: 410464.aMcAtNloPy8EvtGen)")
parser.add_option("--systShw", dest="systShw_file", default="",
                  help="Input systematic showering ttbar filename (i.e. Showering: 410557.PowhegHerwig7EvtGen)")
parser.add_option("--trig", dest="trig", default="b2j1",
                  help="Trigger, where start to validate trigger efficiency (default: b2j1)")
parser.add_option("--year", dest="year", default="2017",
                  help="Year (default: 2017)")
parser.add_option("--doL1TurnOn", action="store_true",
                  dest="doL1TurnOn", default=False,
                  help="Enable to show L1 turn on, (default: HLT turn on)")
parser.add_option("--doL1andHLTTurnOn", action="store_true",
                  dest="doL1andHLTTurnOn", default=False,
                  help="Enable to show L1 and HLT turn on, (default: HLT turn on)")
parser.add_option("--ttbarSelection", action="store_true",
                  dest="ttbarSelection", default=False,
                  help="Enable to apply ttbar selection")
parser.add_option("--label", dest="label", default="",
                  help="Year (default: 2017)")
(options, args) = parser.parse_args()

import root_numpy
import ROOT
 

# configure local options
trig = options.trig
doL1TurnOn = options.doL1TurnOn
doL1andHLTTurnOn = options.doL1andHLTTurnOn
ttbarSelection = options.ttbarSelection
label = options.label
if doL1andHLTTurnOn:
    label += "_L1andHLT"
elif doL1TurnOn:
    label += "_L1"
else:
    label += "_HLT"
if ttbarSelection:
    label += "_TOP"
else:
    label += "_NOM"

print("Start to compare difference of ttbar modelling on trigger efficiency")
print("Enable to apply ttbar selection ?                  -> ", ttbarSelection)
if doL1andHLTTurnOn:
    print("Setup to L1 and HLT combination turn on curve")
elif doL1TurnOn:
    print("Setup to L1 turn on curve")
else:
    print("Setup to HLT turn on curve")

year_in = options.year
year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2017")
    year='2017'
    yr_short='17'

# configure input files
input_dir = options.input_dir
output_dir = options.output_dir
if input_dir:
    if input_dir[-1] != '/': input_dir+='/'
if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

nominal_file = options.nominal_file
if nominal_file:
    if nominal_file[0] == '/':
        input_dir = ''
    nominal_file = input_dir+nominal_file
    if nominal_file[-4:] != 'root':
        print("Warning! Nominal ttbar file not a root file")

systGen_file = options.systGen_file
if systGen_file:
    if systGen_file[0] == '/':
        input_dir = ''
    systGen_file = input_dir+systGen_file
    if systGen_file[-4:] != 'root':
        print("Warning! Systematic generator ttbar file not a root file")

systShw_file = options.systShw_file
if systShw_file:
    if systShw_file[0] == '/':
        input_dir = ''
    systShw_file = input_dir+systShw_file
    if systShw_file[-4:] != 'root':
        print("Warning! Systematic showering ttbar file not a root file")

print("Initialization done!")
print("Define trigger : ", trig, " in ", year)

# Set up columns
columns_mc = ['pass_mutrig', 'pass_eltrig', 'run_number', 'mc_sf']
if ttbarSelection:
    columns_mc += ['ntag', 'nmuons', 'nels', 'mtw', 'met']

# Set up each single jet trigger of Nth jets
nthTrig = nthTrigs(trig, year)
prefixJetName = prefixJetNames(doL1TurnOn) 
for i in range(len(nthTrig)):
    if doL1TurnOn:
        if nthTrig[i].find('HT') != -1: # -> Go to HT cut
            columns_mc += ['HT_Off', 
                           prefixJetName+'isL1Emulated_'+nthTrig[i], prefixJetName+'hasL1Jet_HT']
        else: # -> Go to Et cut
            columns_mc += [prefixJetName+'pT_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasL1Jet_j'+str(i+1)]
    else:
        if nthTrig[i].find('HT') != -1: # -> Go to HT cut
            columns_mc += ['HT_Off', 
                           prefixJetName+'isL1Emulated_'+nthTrig[i],  prefixJetName+'hasL1Jet_'+nthTrig[i],
                           prefixJetName+'isHLTEmulated_'+nthTrig[i], prefixJetName+'hasHLTJet_'+nthTrig[i]]
        else: # -> Go to Et cut
            columns_mc += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1), 
                           prefixJetName+'isL1Emulated_'+nthTrig[i]+'_j'+str(i+1),  prefixJetName+'hasL1Jet_'+nthTrig[i]+'_j'+str(i+1),
                           prefixJetName+'isHLTEmulated_'+nthTrig[i]+'_j'+str(i+1), prefixJetName+'hasHLTJet_'+nthTrig[i]+'_j'+str(i+1)]


# Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
arrs = {}
print("Start preparing nominal ttbar file")
arrs['nominal'] = make_arrs(nominal_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Start preparing systematic generator ttbar file")
arrs['systGen'] = make_arrs(systGen_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Start preparing systematic showering ttbar file")
arrs['systShw'] = make_arrs(systShw_file, columns_mc, yr_short, trig, ttbarSelection, doL1TurnOn, doL1andHLTTurnOn)
print("Filling done!")

print(">>> Go to produce trigger efficiency plots!")

label_lumi = mc_lumi(year)
internal = "Simulation Internal" 

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)
from array import array

columns_to_plot = []
for i in range(len(nthTrig)):
    if nthTrig[i].find('HT') != -1:
        columns_to_plot += ['HT_Off']
    else:
        if doL1TurnOn:
            columns_to_plot += [prefixJetName+'pT_j'+str(i+1)]
        else: 
            columns_to_plot += [prefixJetName+'pT_'+nthTrig[i]+'_j'+str(i+1)]

nth = nths(trig)
# setup output file
foutName = "2020-AUG08-13TeV-HH4bSystError-"+year+".root"
fout = ROOT.TFile(foutName,'update')
if doL1andHLTTurnOn: 
    trigName = "SystError_"+multibTrigName(year, trig)
elif doL1TurnOn:
    trigName = "L1SystError_"+multibTrigName(year, trig)
else:
    trigName = "HLTSystError_"+multibTrigName(year, trig)
trigName = trigName.replace('.', '-')
systNames = ["_diff_ttbarGenerator", "_diff_ttbarShower"]

have_tree = fout.GetListOfKeys().Contains(trigName)
if (have_tree):
    tree = fout.Get(trigName)
    for systName in systNames:
        for nt in nth:
            have_syst = tree.GetListOfKeys().Contains(nt+systName)
            if (have_syst):
                tree.FindKey(nt+systName).Delete()
else:
    tree = fout.mkdir(trigName, (year+" "+trig+" trigger"))
fout.Write()
fout.Close() 


############################# START MAKING TRIGGER EFFICIENCY PLOTS TO CHECK MODELLING DIFFERENCE #############################
for column in columns_to_plot:

    if column.find('HT') != -1:
       n = 2
    elif column.find('njets') != -1:
       n = 1
    else:
       n = int(column[-1])-1
    passNth = 'pass_'+nth[n]

    ### 1. Trigger Efficiency ###
    xlim = xlimsTrigSF(year, trig, nth[n])
    n_bins = xlim[2]
    
    # Nominal reference  1
    hist_nominalRef = Hist(n_bins, xlim[0], xlim[1], title='Nominal Reference', legendstyle='F')
    hist_nominalRef.fill_array(arrs['nominal'][arrs['nominal'][passNth+'Ref']][column], weights=arrs['nominal'][arrs['nominal'][passNth+'Ref']]['mc_sf'])
    hist_nominalRef.SetBinContent(n_bins, hist_nominalRef.GetBinContent(n_bins)+hist_nominalRef.GetBinContent(n_bins+1))

    err_nominalRef = Hist(n_bins, xlim[0], xlim[1], title='Nominal Reference (No weight)', legendstyle='F')
    err_nominalRef.fill_array(arrs['nominal'][arrs['nominal'][passNth+'Ref']][column])
    err_nominalRef.SetBinContent(n_bins, err_nominalRef.GetBinContent(n_bins)+err_nominalRef.GetBinContent(n_bins+1))

    # Nominal trigger (Emulated)
    hist_nominalTri = Hist(n_bins, xlim[0], xlim[1], title='Nominal Emulate', legendstyle='PE')
    hist_nominalTri.fill_array(arrs['nominal'][arrs['nominal'][passNth+'Tri']][column], weights=arrs['nominal'][arrs['nominal'][passNth+'Tri']]['mc_sf'])
    hist_nominalTri.SetBinContent(n_bins, hist_nominalTri.GetBinContent(n_bins)+hist_nominalTri.GetBinContent(n_bins+1))

    err_nominalTri = Hist(n_bins, xlim[0], xlim[1], title='Nominal Emulate (No weight)', legendstyle='PE')
    err_nominalTri.fill_array(arrs['nominal'][arrs['nominal'][passNth+'Tri']][column])
    err_nominalTri.SetBinContent(n_bins, err_nominalTri.GetBinContent(n_bins)+err_nominalTri.GetBinContent(n_bins+1))

    # Systematic Generator reference  1
    hist_systGenRef = Hist(n_bins, xlim[0], xlim[1], title='Systematic Generator Reference', legendstyle='F')
    hist_systGenRef.fill_array(arrs['systGen'][arrs['systGen'][passNth+'Ref']][column], weights=arrs['systGen'][arrs['systGen'][passNth+'Ref']]['mc_sf'])
    hist_systGenRef.SetBinContent(n_bins, hist_systGenRef.GetBinContent(n_bins)+hist_systGenRef.GetBinContent(n_bins+1))

    err_systGenRef = Hist(n_bins, xlim[0], xlim[1], title='Systematic Generator Reference (No weight)', legendstyle='F')
    err_systGenRef.fill_array(arrs['systGen'][arrs['systGen'][passNth+'Ref']][column])
    err_systGenRef.SetBinContent(n_bins, err_systGenRef.GetBinContent(n_bins)+err_systGenRef.GetBinContent(n_bins+1))

    # Systematic Generator trigger (Emulated)
    hist_systGenTri = Hist(n_bins, xlim[0], xlim[1], title='Systematic Generator Emulate', legendstyle='PE')
    hist_systGenTri.fill_array(arrs['systGen'][arrs['systGen'][passNth+'Tri']][column], weights=arrs['systGen'][arrs['systGen'][passNth+'Tri']]['mc_sf'])
    hist_systGenTri.SetBinContent(n_bins, hist_systGenTri.GetBinContent(n_bins)+hist_systGenTri.GetBinContent(n_bins+1))

    err_systGenTri = Hist(n_bins, xlim[0], xlim[1], title='Systematic Generator Emulate (No weight)', legendstyle='PE')
    err_systGenTri.fill_array(arrs['systGen'][arrs['systGen'][passNth+'Tri']][column])
    err_systGenTri.SetBinContent(n_bins, err_systGenTri.GetBinContent(n_bins)+err_systGenTri.GetBinContent(n_bins+1))

    # Systematic Showering reference  1
    hist_systShwRef = Hist(n_bins, xlim[0], xlim[1], title='Systematic Showering Reference', legendstyle='F')
    hist_systShwRef.fill_array(arrs['systShw'][arrs['systShw'][passNth+'Ref']][column], weights=arrs['systShw'][arrs['systShw'][passNth+'Ref']]['mc_sf'])
    hist_systShwRef.SetBinContent(n_bins, hist_systShwRef.GetBinContent(n_bins)+hist_systShwRef.GetBinContent(n_bins+1))

    err_systShwRef = Hist(n_bins, xlim[0], xlim[1], title='Systematic Showering Reference (No weight)', legendstyle='F')
    err_systShwRef.fill_array(arrs['systShw'][arrs['systShw'][passNth+'Ref']][column])
    err_systShwRef.SetBinContent(n_bins, err_systShwRef.GetBinContent(n_bins)+err_systShwRef.GetBinContent(n_bins+1))

    # Systematic Showering trigger (Emulated)
    hist_systShwTri = Hist(n_bins, xlim[0], xlim[1], title='Systematic Showering Emulate', legendstyle='PE')
    hist_systShwTri.fill_array(arrs['systShw'][arrs['systShw'][passNth+'Tri']][column], weights=arrs['systShw'][arrs['systShw'][passNth+'Tri']]['mc_sf'])
    hist_systShwTri.SetBinContent(n_bins, hist_systShwTri.GetBinContent(n_bins)+hist_systShwTri.GetBinContent(n_bins+1))

    err_systShwTri = Hist(n_bins, xlim[0], xlim[1], title='Systematic Showering Emulate (No weight)', legendstyle='PE')
    err_systShwTri.fill_array(arrs['systShw'][arrs['systShw'][passNth+'Tri']][column])
    err_systShwTri.SetBinContent(n_bins, err_systShwTri.GetBinContent(n_bins)+err_systShwTri.GetBinContent(n_bins+1))


    # Rebin
    bin_width = (xlim[1]-xlim[0])/n_bins
    rebin_base = rebinTrigSF_HLT(year, trig, nth[n])
    if doL1TurnOn:
        rebin_base = rebinTrigSF_L1(year, trig, nth[n])
    rebins = []
    for i in range(len(rebin_base)+1):
        if i == 0:
            rebins.append(xlim[0])
        else:
            rebins.append(rebins[i-1]+bin_width*rebin_base[i-1])

    hist_nominalRef_rebin = hist_nominalRef.Rebin(len(rebins)-1, 'nominalRef_rebin', array('d', rebins))
    hist_nominalTri_rebin = hist_nominalTri.Rebin(len(rebins)-1, 'nominalTri_rebin', array('d', rebins))
    err_nominalRef_rebin = err_nominalRef.Rebin(len(rebins)-1, 'err_nominalRef_rebin', array('d', rebins))
    err_nominalTri_rebin = err_nominalTri.Rebin(len(rebins)-1, 'err_nominalTri_rebin', array('d', rebins))

    hist_systGenRef_rebin = hist_systGenRef.Rebin(len(rebins)-1, 'systGenRef_rebin', array('d', rebins))
    hist_systGenTri_rebin = hist_systGenTri.Rebin(len(rebins)-1, 'systGenTri_rebin', array('d', rebins))
    err_systGenRef_rebin = err_systGenRef.Rebin(len(rebins)-1, 'err_systGenRef_rebin', array('d', rebins))
    err_systGenTri_rebin = err_systGenTri.Rebin(len(rebins)-1, 'err_systGenTri_rebin', array('d', rebins))

    hist_systShwRef_rebin = hist_systShwRef.Rebin(len(rebins)-1, 'systShwRef_rebin', array('d', rebins))
    hist_systShwTri_rebin = hist_systShwTri.Rebin(len(rebins)-1, 'systShwTri_rebin', array('d', rebins))
    err_systShwRef_rebin = err_systShwRef.Rebin(len(rebins)-1, 'err_systShwRef_rebin', array('d', rebins))
    err_systShwTri_rebin = err_systShwTri.Rebin(len(rebins)-1, 'err_systShwTri_rebin', array('d', rebins))


    hist_eff_nominal = hist_nominalTri_rebin.Clone("eff_nominal")
    hist_eff_nominal.Divide(hist_nominalRef_rebin)
    hist_eff_nominal.SetMarkerStyle(24)
    hist_eff_nominal.SetMarkerSize(0.7)
    hist_eff_nominal.SetMarkerColor(ROOT.kBlack)
    hist_eff_nominal.SetLineColor(ROOT.kBlack)
    hist_eff_nominal.GetYaxis().SetRangeUser(0.00, 1.45)
    hist_eff_nominal.GetXaxis().SetLabelSize(0.0)
    hist_eff_nominal.GetYaxis().SetLabelSize(0.045)
    hist_eff_nominal.GetYaxis().SetLabelOffset(0.005)
    if doL1andHLTTurnOn:
        hist_eff_nominal.GetYaxis().SetTitle("Trigger Efficiency #varepsilon")
    elif doL1TurnOn:
        hist_eff_nominal.GetYaxis().SetTitle("Trigger Efficiency #varepsilon_{L1}")
    else:
        hist_eff_nominal.GetYaxis().SetTitle("Trigger Efficiency #varepsilon_{HLT}")
    hist_eff_nominal.SetTitle('')
    hist_eff_nominal.GetYaxis().SetTitleSize(0.045)
    hist_eff_nominal.GetYaxis().SetTitleOffset(0.95)

    hist_eff_systGen = hist_systGenTri_rebin.Clone("eff_systGen")
    hist_eff_systGen.Divide(hist_systGenRef_rebin)
    hist_eff_systGen.SetMarkerStyle(26)
    hist_eff_systGen.SetMarkerSize(0.7)
    hist_eff_systGen.SetMarkerColor(ROOT.kRed)
    hist_eff_systGen.SetLineColor(ROOT.kRed)

    hist_eff_systShw = hist_systShwTri_rebin.Clone("eff_systShw")
    hist_eff_systShw.Divide(hist_systShwRef_rebin)
    hist_eff_systShw.SetMarkerStyle(26)
    hist_eff_systShw.SetMarkerSize(0.7)
    hist_eff_systShw.SetMarkerColor(ROOT.kBlue)
    hist_eff_systShw.SetLineColor(ROOT.kBlue)

    asym_eff_nominal = ROOT.TGraphAsymmErrors(hist_eff_nominal) 
    asym_eff_systGen = ROOT.TGraphAsymmErrors(hist_eff_systGen) 
    asym_eff_systShw = ROOT.TGraphAsymmErrors(hist_eff_systShw) 
    # Set error 
    for i in range(len(rebins)-1):
        hist_eff_nominal.SetBinError(i+1, \
            calcEffStatError(err_nominalTri_rebin.GetBinContent(i+1), err_nominalRef_rebin.GetBinContent(i+1), hist_eff_nominal.GetBinContent(i+1)))
        hist_eff_systGen.SetBinError(i+1, \
            calcEffStatError(err_systGenTri_rebin.GetBinContent(i+1), err_systGenRef_rebin.GetBinContent(i+1), hist_eff_systGen.GetBinContent(i+1)))
        hist_eff_systShw.SetBinError(i+1, \
            calcEffStatError(err_systShwTri_rebin.GetBinContent(i+1), err_systShwRef_rebin.GetBinContent(i+1), hist_eff_systShw.GetBinContent(i+1)))

        asym_eff_nominal.SetPointEYhigh(i, \
            calcEffStatError(err_nominalTri_rebin.GetBinContent(i+1), err_nominalRef_rebin.GetBinContent(i+1), hist_eff_nominal.GetBinContent(i+1), True))
        asym_eff_nominal.SetPointEYlow(i, \
            calcEffStatError(err_nominalTri_rebin.GetBinContent(i+1), err_nominalRef_rebin.GetBinContent(i+1), hist_eff_nominal.GetBinContent(i+1), False))
        asym_eff_systGen.SetPointEYhigh(i, \
            calcEffStatError(err_systGenTri_rebin.GetBinContent(i+1), err_systGenRef_rebin.GetBinContent(i+1), hist_eff_systGen.GetBinContent(i+1), True))
        asym_eff_systGen.SetPointEYlow(i, \
            calcEffStatError(err_systGenTri_rebin.GetBinContent(i+1), err_systGenRef_rebin.GetBinContent(i+1), hist_eff_systGen.GetBinContent(i+1), False))
        asym_eff_systShw.SetPointEYhigh(i, \
            calcEffStatError(err_systShwTri_rebin.GetBinContent(i+1), err_systShwRef_rebin.GetBinContent(i+1), hist_eff_systShw.GetBinContent(i+1), True))
        asym_eff_systShw.SetPointEYlow(i, \
            calcEffStatError(err_systShwTri_rebin.GetBinContent(i+1), err_systShwRef_rebin.GetBinContent(i+1), hist_eff_systShw.GetBinContent(i+1), False))

    # Canvas for jet-level trigger efficiency
    ROOT.gStyle.SetOptStat(0)
    canvas = Canvas()
    canvas.cd()
    pad1 = Pad(0.0,0.33,1.0,1.0)
    pad2 = Pad(0.0,0.0,1.0,0.33)
    pad1.SetBottomMargin(0.015)
    pad1.SetTopMargin(0.05)
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.3)
    pad1.Draw()
    pad2.Draw()
    pad1.SetTicks(1, 1)
    pad2.SetTicks(1, 1)

    pad1.cd()
    hist_eff_nominal.Draw("pe") 
    asym_eff_nominal.Draw("same pe") 
    asym_eff_systGen.Draw("same pe") 
    asym_eff_systShw.Draw("same pe") 

    pad2.cd()
    rat_systGen = hist_eff_systGen.Clone("systGen_over_nominal")
    rat_systGen.Divide(hist_eff_nominal)
    rat_systGen.GetYaxis().SetRangeUser(0.60, 1.40)
    rat_systGen.GetYaxis().SetNdivisions(505)
    rat_systGen.GetXaxis().SetLabelSize(0.11)
    rat_systGen.GetYaxis().SetLabelSize(0.1)
    rat_systGen.GetXaxis().SetLabelOffset(0.009)
    rat_systGen.GetYaxis().SetLabelOffset(0.007)
    rat_systGen.GetYaxis().SetTitle("#varepsilon_{syst} / #varepsilon_{nominal}")
    rat_systGen.GetYaxis().CenterTitle();
    rat_systGen.GetXaxis().SetTitle(x_label_dict(column))
    rat_systGen.SetTitle('')
    rat_systGen.GetYaxis().SetTitleSize(0.12)
    rat_systGen.GetXaxis().SetTitleSize(0.12)
    rat_systGen.GetYaxis().SetTitleOffset(0.35)
    rat_systGen.GetXaxis().SetTitleOffset(0.95)

    rat_systGen.SetMarkerStyle(20)
    rat_systGen.SetMarkerSize(0.6)
    rat_systGen.SetMarkerColor(ROOT.kRed)
    rat_systGen.SetLineColor(ROOT.kRed)
    rat_systGen.Draw("pe");

    rat_systShw = hist_eff_systShw.Clone("systShw_over_nominal")
    rat_systShw.Divide(hist_eff_nominal)
    rat_systShw.SetMarkerStyle(20)
    rat_systShw.SetMarkerSize(0.6)
    rat_systShw.SetMarkerColor(ROOT.kBlue)
    rat_systShw.SetLineColor(ROOT.kBlue)
    rat_systShw.Draw("same pe");

    pad2.cd()
    line1 = ROOT.TLine(xlim[0], 1.0, xlim[1], 1.0)
    line1.SetLineColor(1)
    line1.SetLineWidth(1)
    line1.SetLineStyle(2)
    line1.Draw()

    pad1.cd()
    line1.Draw()
    if trig == "b1j0" and not doL1TurnOn:
        legend1 = ROOT.TLegend(0.15, 0.20, 0.43, 0.43)
    else:
        legend1 = ROOT.TLegend(0.55, 0.20, 0.83, 0.43)
    legend1.AddEntry(asym_eff_nominal, 'Nominal (PhPy8)', 'pe')
    legend1.AddEntry(asym_eff_systGen, 'Generator (aMcAtNloPy8)', 'pe')
    legend1.AddEntry(asym_eff_systShw, 'Showering (PhHerwig7)', 'pe')
    legend1.SetBorderSize(0)
    legend1.SetFillStyle(0)
    legend1.Draw() 

    lat_label1 = HLTrigName(year, trig, nth[n])
    if doL1TurnOn:
        lat_label1 = L1TrigName(year, trig, nth[n]) 
    ATLASLabel(0.15, 0.855, 0.058, internal, ('#sqrt{s} = 13 TeV, '+year+', '+str(label_lumi)+' fb^{-1}'), lat_label1, 0.77, 0.065)

    canvas.Draw()
    canvas.SaveAs((output_dir+'ttbarModel'+yr_short+'_'+trig+'_'+column+label+'.C'))
    canvas.SaveAs((output_dir+'ttbarModel'+yr_short+'_'+trig+'_'+column+label+'.pdf'))


    for systName in systNames:
       if systName.find("Gene") != -1:
           syst = rat_systGen.Clone(nth[n]+systName)
       if systName.find("Shower") != -1:
           syst = rat_systShw.Clone(nth[n]+systName)
       for i in range(syst.GetNbinsX()):
           syst.SetBinError(i+1, 0.)
       syst.GetYaxis()
       syst.GetYaxis().SetTitleSize(0.040)
       syst.GetYaxis().SetLabelSize(0.040)
       syst.GetXaxis().SetTitleSize(0.040)
       syst.GetXaxis().SetLabelSize(0.040)
       syst.GetYaxis().SetTitleOffset(1.05)
       syst.GetXaxis().SetTitleOffset(1.05)

       fout = ROOT.TFile(foutName,'update')
       fout.cd()
       tree = fout.Get(trigName)
       tree.cd()
       tree.Add(syst)
       fout.Write()
       fout.Close() 
